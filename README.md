#Bow Simulation Tool
Bow Simulation Tool is a program for analysing the static and dynamic performance of bow designs. See http://stfnp.bitbucket.org/ for more information about the program.

The source code is organised into three main parts:
- FEM: A basic set of classes and functions for assembling and solving mechanical finite element systems
- Bow: Uses the FEM part to provide a simulation model for bows
- GUI: A graphical interface to the bow model so that users can work with it

#Dependencies
- Nana 1.0.1 (graphical user interface, http://www.nanapro.org/)
- Eigen 3.2.1 (linear algebra, http://eigen.tuxfamily.org/)
- Jsoncons 0.93 (parsing json files, https://sourceforge.net/projects/jsoncons/)
- Tclap 1.2.1 (parsing command line arguments, http://tclap.sourceforge.net/)

#License
Bow Simulation Tool is released under the GNU General Public License v3.0.