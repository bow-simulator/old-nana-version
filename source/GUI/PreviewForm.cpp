// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "PreviewForm.h"

PreviewForm::PreviewForm(nana::window wd, const nana::string& filename, const BowParameters& parameters, const BowSettings& settings)
	: nana::form(wd, nana::rectangle(0, 0, 600, 500)),
	  limb(parameters, settings.elements),
	  plot_shape(*this),
	  plot_w(*this),
	  plot_h(*this),
	  tabs(*this),
	  plc(*this)
{
	init_plots(parameters);

	caption(L"Preview for " + filename + L" - Bow Simulation Tool");

	//Display plots
	tabs.push_back(L"Centerline");
	tabs.push_back(L"Width");
	tabs.push_back(L"Height");
	
	tabs.relate(0, plot_shape);
	tabs.relate(1, plot_w);
	tabs.relate(2, plot_h);
	tabs.activate(0);

	plc.div("<vertical <tabs weight=25><plots>>");
	plc.field("tabs") << tabs;
	plc.field("plots").fasten(plot_shape);
	plc.field("plots").fasten(plot_w);
	plc.field("plots").fasten(plot_h);
	plc.collocate();
}

///\todo Why init function?
void PreviewForm::init_plots(const BowParameters& parameters)
{
	//Init plot_shape: Plot the centerline
	plot_shape.title(L"Limb centerline");
	plot_shape.getXAxis().label(L"x-position " + units::length.label());
	plot_shape.getYAxis().label(L"y-positiony " + units::length.label());
	plot_shape.getXAxis().scale(units::length.from_base());
	plot_shape.getYAxis().scale(units::length.from_base());
	plot_shape.grid(true);
	plot_shape.addPlotData(PlotData(parameters.x,
									parameters.y,
									LineFormat(LineStyle::NONE),
									MarkerFormat(MarkerStyle::SQUARE, 0xB7102F)));	//Plot centerline control points

	plot_shape.addPlotData(PlotData(limb.x(),
									limb.y(),
									LineFormat(LineStyle::SOLID, 0x00008B),
									MarkerFormat(MarkerStyle::PLUS)));	//Plot approximated centerline
	plot_shape.fit125(true, true, true, true);
	//plot_shape.getXAxis().max(plot_shape.getYAxis().max());

	//Init plot_w
	plot_w.title(L"Limb width");
	plot_w.getXAxis().label(L"Centerline arc length " + units::length.label());
	plot_w.getYAxis().label(L"Width " + units::length.label());
	plot_w.getXAxis().scale(units::length.from_base());
	plot_w.getYAxis().scale(units::length.from_base());
	plot_w.grid(true);
	plot_w.addPlotData(PlotData(limb.arcLength(),
								limb.width(),
								LineFormat(LineStyle::SOLID, 0x00008B),
								MarkerFormat(MarkerStyle::PLUS)));
	plot_w.fit125(false, false, true, true);
	plot_w.getYAxis().max(1.5*plot_w.getYAxis().max());

	//Init plot_h
	plot_h.title(L"Limb height");
	plot_h.getXAxis().label(L"Centerline arc length " + units::length.label());
	plot_h.getYAxis().label(L"Height " + units::length.label());
	plot_h.getXAxis().scale(units::length.from_base());
	plot_h.getYAxis().scale(units::length.from_base());
	plot_h.grid(true);
	plot_h.addPlotData(PlotData(limb.arcLength(),
								limb.height(),
								LineFormat(LineStyle::SOLID, 0x00008B),
								MarkerFormat(MarkerStyle::PLUS)));
	plot_h.fit125(false, false, true, true);
	plot_h.getYAxis().max(1.5*plot_h.getYAxis().max());
}