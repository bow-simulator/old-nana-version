// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef TABLE_H
#define TABLE_H

#include <nana/gui.hpp>
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/textbox.hpp>
#include <nana/gui/widgets/panel.hpp>

#include <iostream>
#include <vector>
#include <memory>
#include <cstdio>

/**
Makeshift Table widget used for limb data input, maybe this can be replaced if nana will provide a 'real' table.
*/
class Table: public nana::panel<true>
{
public:
	///\todo Solve sprintf problem without turning the warning off (Preprocessor symbol _CRT_SECURE_NO_DEPRECATE)
	Table(nana::window wd, std::size_t rows, std::size_t cols);

	void setColumnLabel(std::size_t col, nana::string text);
	double getCellEntry(std::size_t row, std::size_t col) const;
	void setCellEntry(std::size_t row, std::size_t col, double val);
	void setCellEntry(std::size_t row, std::size_t col, nana::string val);
	std::vector<double> getColumn(std::size_t col);
	void setColumn(std::size_t col, const std::vector<double>& vec);
	void setColumn(std::size_t col, const std::vector<nana::string>& vec);

	void clear();
	bool edited() const;
	void edited_reset();

	std::size_t getRows() const;
	std::size_t getCols() const;

private:
	std::size_t rows;
	std::size_t cols;

	nana::place plc;
	std::vector<std::unique_ptr<nana::label>> labels;
	std::vector< std::vector<std::unique_ptr<nana::textbox>> > cells;
};

#endif // TABLE_H