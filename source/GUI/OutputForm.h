// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef OUTPUT_FORM_H
#define OUTPUT_FORM_H

#include "../Bow/Units.h"
#include "../Bow/StaticData.h"
#include "../Bow/DynamicData.h"
#include "../Bow/LimbGeometry.h"
#include "Plot/Plot.h"
#include "GroupBox.h"

#include <nana/gui.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/textbox.hpp>
#include <nana/gui/widgets/panel.hpp>
#include <nana/gui/widgets/tabbar.hpp>
#include <nana/gui/place.hpp>

class StaticValues: public nana::panel<false>
{
public:
	StaticValues(nana::window wd, const StaticData& static_data);

private:
	nana::place plc;
	GroupBox box;
	nana::label lb_draw_force;
	nana::label lb_string_length;
	nana::label lb_energy_braced;
	nana::label lb_energy_drawn;
	nana::label lb_drawing_work;
	nana::label lb_storage_ratio;

	nana::textbox tb_val_draw_force;
	nana::textbox tb_val_string_length;
	nana::textbox tb_val_energy_braced;
	nana::textbox tb_val_energy_drawn;
	nana::textbox tb_val_drawing_work;
	nana::textbox tb_val_storage_ratio;
};

class TabpageStatic: public nana::panel<false>
{
public:
	TabpageStatic(nana::window wd, const StaticData& static_data, const LimbGeometry& limb);

private:
	Plot plot_draw_states;
	Plot plot_draw_curve;
	Plot plot_draw_work;
	Plot plot_string_force;
	Plot plot_stress;
	StaticValues specific_values;
	nana::tabbar<std::wstring> tabs;
	nana::place plc;
};

class DynamicValues: public nana::panel<false>
{
public:
	DynamicValues(nana::window wd, const StaticData& static_data, const DynamicData& dynamic_data);

private:
	nana::place plc;
	GroupBox box;
	nana::label lb_final_velocity;
	nana::label lb_final_energy;
	nana::label lb_efficiency;

	nana::textbox tb_val_final_velocity;
	nana::textbox tb_val_final_energy;
	nana::textbox tb_val_efficiency;
};

class TabpageDynamic: public nana::panel<false>
{
public:
	TabpageDynamic(nana::window wd, const StaticData& static_data, const DynamicData& dynamic_data, const LimbGeometry& limb);
	
private:
	Plot plot_position;
	Plot plot_velocity;
	Plot plot_acceleration;
	Plot plot_string_force;
	Plot plot_support_force;
	Plot plot_energy;
	DynamicValues specific_values;
	nana::tabbar<std::wstring> tabs;
	nana::place plc;
};

class OutputForm: public nana::form
{
public:
	OutputForm(nana::window wd, const nana::string& filename, const StaticData& static_data, const DynamicData& dynamic_data, const LimbGeometry& limb);

private:
	std::unique_ptr<TabpageStatic> tabpage_static;
	std::unique_ptr<TabpageDynamic> tabpage_dynamic;
	nana::tabbar<std::wstring> tabs;
	nana::place plc;
};

#endif // OUTPUT_FORM