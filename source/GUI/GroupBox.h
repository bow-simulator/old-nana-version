// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef GROUP_BOX_H
#define GROUP_BOX_H

#include <nana/gui.hpp>
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/panel.hpp>
#include <cstdio>
#include <iostream>

class GroupBox: public nana::panel<true>
{
public:
	GroupBox(nana::window wd, const nana::string& text)
		: panel(wd),
		  content(*this),
		  plc_inner(content),
		  plc_outer(*this),
		  dw(*this),
		  gap(40)
	{
		caption(text);

		char div_text[50];
		sprintf(div_text, "<vertical content margin=%u>", gap);
		plc_outer.div(div_text);
		plc_outer.field("content") << content;
		plc_outer.collocate();

		dw.draw([&](nana::paint::graphics& graph)
		{
			nana::size size_caption = graph.text_extent_size(caption());
			graph.string(nana::point((graph.width() - size_caption.width)/2, (gap-size_caption.height)/2), caption(), nana::color(0, 0, 0));

			nana::color line_color(128, 128, 128);
			graph.line(nana::point(gap/2, gap/2), nana::point((graph.width() - size_caption.width - size_caption.height)/2, gap/2), line_color);
			graph.line(nana::point((graph.width() + size_caption.width + size_caption.height)/2, gap/2), nana::point(graph.width()-gap/2, gap/2), line_color);
			graph.line(nana::point(graph.width()-gap/2, gap/2), nana::point(graph.width()-gap/2, graph.height()-gap/2), line_color);
			graph.line(nana::point(graph.width()-gap/2, graph.height()-gap/2), nana::point(gap/2, graph.height()-gap/2), line_color);
			graph.line(nana::point(gap/2, graph.height()-gap/2), nana::point(gap/2, gap/2), line_color);
		});

		dw.update();
	}

	nana::place& plc()
	{
		return plc_inner;
	}

	nana::panel<false>& pnl()
	{
		return content;
	}

private:
	nana::panel<false> content;
	nana::place plc_inner;
	nana::place plc_outer;
	nana::drawing dw;
	unsigned int gap;
};

#endif // GROUP_BOX_H