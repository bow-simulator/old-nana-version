// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef PANEL_CENTERLINE_H
#define PANEL_CENTERLINE_H

#include "CurveEditor.h"
#include "../Plot/Plot.h"

#include <nana/gui.hpp>
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/panel.hpp>

class PanelCenterline: public nana::panel<false>
{
public:
	PanelCenterline(nana::window wd);
	void load(const jsoncons::wjson& obj);
	jsoncons::wjson save() const;

private:
	std::vector<jsoncons::wjson> segment_types;
	CurveEditor editor;
	Plot plot;
	nana::place plc;

	std::vector<double> x_values;
	std::vector<double> y_values;
	std::vector<double> x_marking;
	std::vector<double> y_marking;

	void update();
	void initSegmentTypes();
	std::unique_ptr<Curve> curveFromData(const jsoncons::wjson& data);
};

#endif // PANEL_CENTERLINE_H