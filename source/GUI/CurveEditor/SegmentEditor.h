// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef SEGMENT_EDITOR_H
#define SEGMENT_EDITOR_H

#include "../../Bow/Units.h"
#include "../../Bow/Geometry/ComposedCurve.h"

#include <nana/gui.hpp>
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/panel.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/combox.hpp>
#include <nana/gui/widgets/tabbar.hpp>
#include <nana/gui/widgets/textbox.hpp>
#include <nana/gui/widgets/checkbox.hpp>

#include <iostream>
#include <string>
#include <memory>

/*
To do:
- Ask user to confirm type change of a segment before losing data
*/

#include <jsoncons/json.hpp>

class SegmentEditor;

class TabGeneral: public nana::panel<false>
{
public:
	TabGeneral(SegmentEditor& editor, const std::vector<jsoncons::wjson>& segment_types);
	void load(const jsoncons::wjson& obj);
	jsoncons::wjson save() const;

private:
	nana::label lb_type;
	nana::combox cb_type;
	nana::checkbox cb_angle;
	nana::textbox tb_angle;
	nana::place plc;
};

class TabSpecific: public nana::panel<false>
{
public:
	TabSpecific(SegmentEditor& editor);
	void load(const jsoncons::wjson& obj);
	jsoncons::wjson save() const;

private:
	std::vector<std::unique_ptr<nana::label>> labels;
	std::vector<std::unique_ptr<nana::textbox>> tboxes;
	nana::place plc;
};

class SegmentEditor: public nana::form
{
public:
	SegmentEditor(nana::window wd, const std::vector<jsoncons::wjson>& segment_types);	//Loads from segment_types[0]
	SegmentEditor(nana::window wd, const std::vector<jsoncons::wjson>& segment_types, jsoncons::wjson& data);	//Loads from data
	SegmentEditor(nana::window wd, const std::vector<jsoncons::wjson>& segment_types, std::size_t tab_index);	//Loads nothing

	void type(std::size_t index);
	bool operator()(jsoncons::wjson& data);	// Saves to data

private:
	const std::vector<jsoncons::wjson>& segment_types;
	bool success;
	bool loading;
	nana::tabbar<std::wstring> tabbar;
	TabGeneral tab1;
	TabSpecific tab2;

	nana::button bt_ok;
	nana::button bt_cancel;
	nana::place plc;

	void load(const jsoncons::wjson& obj);
	jsoncons::wjson save() const;
};

#endif // SEGMENT_EDITOR_H