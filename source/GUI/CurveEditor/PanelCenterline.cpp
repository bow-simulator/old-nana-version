// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "PanelCenterline.h"

PanelCenterline::PanelCenterline(nana::window wd)
	: panel(wd),
	  editor(*this, segment_types, std::bind(&PanelCenterline::update, this)),
	  plot(*this),
	  plc(*this)
{
	initSegmentTypes();

	plot.title(L"Limb centerline");
	plot.getXAxis().label(L"x-position " + units::length.label());
	plot.getYAxis().label(L"y-position " + units::length.label());
	plot.getXAxis().scale(units::length.from_base());
	plot.getYAxis().scale(units::length.from_base());
	plot.grid(true);
	plot.addPlotData(PlotData(x_values, y_values, LineFormat(LineStyle::SOLID, 0x00008B)));
	plot.addPlotData(PlotData(x_marking, y_marking, LineFormat(LineStyle::NONE, 0x00008B), MarkerFormat(MarkerStyle::SQUARE, 0xB7102F)));
	plot.fit125();

	plc.div("<editor weight=260><plot>");
	plc.field("editor") << editor;
	plc.field("plot") << plot;
	plc.collocate();
}

void PanelCenterline::load(const jsoncons::wjson& obj)
{
	for(std::size_t i = 0; i < obj.size(); ++i)
	{
		editor.addData(obj[i]);
	}
}

jsoncons::wjson PanelCenterline::save() const
{
	jsoncons::wjson obj(jsoncons::wjson::an_array);
	for(std::size_t i = 0; i < editor.segments(); ++i)
	{
		obj.add(editor.getData(i));
	}
	
	return obj;
}

void PanelCenterline::update()
{
	x_values.clear();
	y_values.clear();
		
	x_marking.clear();
	y_marking.clear();

	ComposedCurve curve;
	std::vector<std::unique_ptr<Curve>> seg;

	for(std::size_t i = 0; i < editor.segments(); ++i)
	{
		const jsoncons::wjson& data = editor.getData(i);
		seg.push_back(curveFromData(data));

		if(data.has_member(L"angle"))
		{
			curve.addSegment(*seg.back(), units::angle.to_base(data[L"angle"].as<double>()));
		}
		else
		{
			curve.addSegment(*seg.back());
		}
	}

	if(seg.size() > 0)
	{
		for(double p = 0.0; p <= 1.0; p += 0.001)
		{
			Eigen::Vector2d R = curve.value(p);
			x_values.push_back(R(0));
			y_values.push_back(R(1));
		}

		curve.getNodes(x_marking, y_marking);
	}

	plot.fit125();
	plot.update();
}

void PanelCenterline::initSegmentTypes()
{
	using namespace jsoncons;
	
	//Line
	wjson line_seg_length(wjson::an_array);
	line_seg_length.add(L"length " + units::length.label());
	line_seg_length.add(units::length.from_base(0.5));

	wjson line_seg(wjson::an_array);
	line_seg.add(line_seg_length);

	wjson line;
	line[L"segment"] = line_seg;
	line[L"type"] = L"Line";
	line[L"angle"] = 0;
	
	//Arc (radius, angle)
	wjson arc1_seg_radius(wjson::an_array);
	arc1_seg_radius.add(L"radius " + units::length.label());
	arc1_seg_radius.add(units::length.from_base(0.5));

	wjson arc1_seg_length(wjson::an_array);
	arc1_seg_length.add(L"angle " + units::angle.label());
	arc1_seg_length.add(units::angle.from_base(M_PI_4));

	wjson arc1_seg(wjson::an_array);
	arc1_seg.add(arc1_seg_radius);
	arc1_seg.add(arc1_seg_length);

	wjson arc1;
	arc1[L"segment"] = arc1_seg;
	arc1[L"type"] = L"Arc (r, phi)";
	arc1[L"angle"] = 0;


	//Arc (radius, length)
	wjson arc2_seg_radius(wjson::an_array);
	arc2_seg_radius.add(L"radius " + units::length.label());
	arc2_seg_radius.add(units::length.from_base(0.5));

	wjson arc2_seg_length(wjson::an_array);
	arc2_seg_length.add(L"length " + units::length.label());
	arc2_seg_length.add(units::length.from_base(0.392699));

	wjson arc2_seg(wjson::an_array);
	arc2_seg.add(arc2_seg_radius);
	arc2_seg.add(arc2_seg_length);

	wjson arc2;
	arc2[L"segment"] = arc2_seg;
	arc2[L"type"] = L"Arc (r, L)";
	arc2[L"angle"] = 0;

	segment_types.push_back(line);
	segment_types.push_back(arc1);
	segment_types.push_back(arc2);
}

std::unique_ptr<Curve> PanelCenterline::curveFromData(const jsoncons::wjson& data)
{
	if(data[L"type"] == L"Line")
	{
		double length = units::length.to_base(data[L"segment"][0][1].as<double>());
		return std::unique_ptr<Curve>(new LineSegment(length));
	}
	else if(data[L"type"] == L"Arc (r, phi)")
	{
		double radius = units::length.to_base(data[L"segment"][0][1].as<double>());
		double angle = units::angle.to_base(data[L"segment"][1][1].as<double>());
		return std::unique_ptr<Curve>(new ArcSegment(radius, angle));
	}
	else if(data[L"type"] == L"Arc (r, L)")
	{
		double radius = units::length.to_base(data[L"segment"][0][1].as<double>());
		double length = units::length.to_base(data[L"segment"][1][1].as<double>());
		return std::unique_ptr<Curve>(new ArcSegment(radius, length/radius));
	}
	else
	{
		return std::unique_ptr<Curve>();
	}
}