// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "CurveEditor.h"

nana::listbox::oresolver& operator<<(nana::listbox::oresolver& ores, const jsoncons::wjson& data)
{
	if(data.has_member(L"angle"))
	{
		return ores << data[L"type"].as<std::wstring>() << data[L"angle"].as<std::wstring>();
	}
	else
	{
		return ores << data[L"type"].as<std::wstring>() << L"none";
	}
}

CurveEditor::CurveEditor(nana::window wd, const std::vector<jsoncons::wjson>& segment_types, std::function<void()> edit_callback)
	: panel(wd),
	  segment_types(segment_types),
	  edit_callback(edit_callback),
	  lsbox(*this),
	  bt_new(*this, L"New"),
	  bt_clear(*this, L"Clear"),
	  plc(*this)
{
	bt_new.events().click(std::bind(&CurveEditor::OnNew, this));
	
	bt_clear.enabled(false);
	bt_clear.events().click(std::bind(&CurveEditor::OnClear, this));
	
	lsbox.freeze_sort(true);
	lsbox.append_header(L"Type");
	lsbox.append_header(L"Alignment " + units::angle.label());

	lsbox.events().dbl_click(std::bind(&CurveEditor::OnEdit, this));
	lsbox.events().selected([&](const ::nana::arg_listbox& arg)
	{
		if(arg.selected) //When an item is selected
		{
			lsbox.auto_draw(false);
			auto pos = arg.item.pos();
			auto selections = lsbox.selected();

			//Unselect other selected items.
			for (auto & s : selections)
			{
				if (s != pos)
					lsbox.at(s.cat).at(s.item).select(false);
			}
			lsbox.auto_draw(true);
		}
	});
	
	plc.div("<vertical <<buttons> weight=25><listbox>>");
	plc.field("buttons") << bt_new << bt_clear;
	plc.field("listbox") << lsbox;
	plc.collocate();
}

void CurveEditor::edited(std::function<void()> callback)
{
	edit_callback = callback;
}

void CurveEditor::addData(const jsoncons::wjson& obj)
{
	lsbox.at(0).append(obj, true);
	edit_callback();
	bt_clear.enabled(true);
}

const jsoncons::wjson& CurveEditor::getData(std::size_t i) const
{
	return lsbox.at(0).at(i).value<jsoncons::wjson>();
}

std::size_t CurveEditor::segments() const
{
	return lsbox.at(0).size();
}

void CurveEditor::OnEdit()
{
	auto selection = lsbox.selected();
		
	if(!selection.empty())
	{
		auto s = selection.at(0);
		jsoncons::wjson& data = lsbox.at(s.cat).at(s.item).value<jsoncons::wjson>();

		SegmentEditor editor(*this, segment_types, data);

		if(editor(data))
		{
			lsbox.at(s.cat).at(s.item).resolve_from(data);
			edit_callback();
		}
	}
}

void CurveEditor::OnNew()
{
	SegmentEditor editor(*this, segment_types);

	jsoncons::wjson data = segment_types[0];
	if(editor(data))
	{
		lsbox.at(0).append(data, true);
		edit_callback();
		bt_clear.enabled(true);
	}
}

void CurveEditor::OnClear()
{
	lsbox.clear();
	bt_clear.enabled(false);

	edit_callback();
}