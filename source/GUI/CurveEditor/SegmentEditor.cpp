// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "SegmentEditor.h"

TabGeneral::TabGeneral(SegmentEditor& editor, const std::vector<jsoncons::wjson>& segment_types)
	: panel(editor),
	  lb_type(*this, L"Type"),
	  cb_type(*this),
	  cb_angle(*this, L"Alignment angle " + units::angle.label()),
	  tb_angle(*this),
	  plc(*this)
{
	for(std::size_t i = 0; i < segment_types.size(); ++i)
	{
		cb_type.push_back(segment_types[i][L"type"].as<std::wstring>());
	}

	cb_type.events().selected([&](const nana::arg_combox& arg)
	{
		editor.type(arg.widget.option());
	});
	
	tb_angle.multi_lines(false);

	tb_angle.enabled(false);
	cb_angle.events().click([&]()
	{
		tb_angle.enabled(cb_angle.checked());
	});
	
	plc.div("<vertical <tbl grid=[2,2] gap=10 weight=66><> margin=10>");
	plc.field("tbl") << lb_type << cb_type
					 << cb_angle << tb_angle;
	plc.collocate();
}

void TabGeneral::load(const jsoncons::wjson& obj)
{
	for(std::size_t i = 0; i < cb_type.the_number_of_options(); ++i)
	{
		if(cb_type.text(i) == obj[L"type"].as<std::wstring>())
			cb_type.option(i);
	}	
	
	if(obj.has_member(L"angle"))
	{
		cb_angle.check(true);
		tb_angle.enabled(true);
		tb_angle.caption(obj[L"angle"].as<std::wstring>());
	}
}

jsoncons::wjson TabGeneral::save() const
{
	jsoncons::wjson obj;

	obj[L"type"] = cb_type.text(cb_type.option());

	if(cb_angle.checked())
	{
		obj[L"angle"] = tb_angle.to_double();
	}
	else if(obj.has_member(L"angle"))
	{
		obj.remove_member(L"angle");
	}

	return obj;
}

TabSpecific::TabSpecific(SegmentEditor& editor)
	: panel(editor),
	  plc(*this)
{

}

void TabSpecific::load(const jsoncons::wjson& obj)
{
	plc.div("<vertical <tbl grid=[2,6] gap=10 weight=210><> margin=10>");

	labels.clear();
	tboxes.clear();

	for(std::size_t i = 0; i < obj.size(); ++i)
	{
		std::unique_ptr<nana::label> lb(new nana::label(*this, obj[i][0].as<std::wstring>()));
		std::unique_ptr<nana::textbox> tb(new nana::textbox(*this, obj[i][1].as<std::wstring>()));
		tb->multi_lines(false);

		plc.field("tbl") << *lb << *tb;

		labels.push_back(std::move(lb));
		tboxes.push_back(std::move(tb));
	}

	plc.collocate();
}

jsoncons::wjson TabSpecific::save() const
{
	jsoncons::wjson obj(jsoncons::wjson::an_array);

	for(std::size_t i = 0; i < labels.size(); ++i)
	{
		jsoncons::wjson entry(jsoncons::wjson::an_array);
		entry.add(labels[i]->caption());
		entry.add(tboxes[i]->to_double());
		obj.add(entry);
	}

	return obj;
}

SegmentEditor::SegmentEditor(nana::window wd, const std::vector<jsoncons::wjson>& segment_types)
	: SegmentEditor(wd, segment_types, 0)
{
	load(segment_types[0]);
}

SegmentEditor::SegmentEditor(nana::window wd, const std::vector<jsoncons::wjson>& segment_types, jsoncons::wjson& data)
	: SegmentEditor(wd, segment_types, 1)
{
	load(data);
}

SegmentEditor::SegmentEditor(nana::window wd, const std::vector<jsoncons::wjson>& segment_types, std::size_t tab_index)
	: form(wd, nana::API::make_center(wd, 350, 250), nana::appear::decorate<nana::appear::sizable>()),
	  segment_types(segment_types),
	  success(false),
	  loading(false),
	  tabbar(*this),
	  tab1(*this, segment_types),
	  tab2(*this),
	  bt_ok(*this, L"OK"),
	  bt_cancel(*this, L"Cancel"),
	  plc(*this)
{
	caption(L"Edit segment");

	tabbar.push_back(L"General");
	tabbar.push_back(L"Parameters");
	tabbar.relate(0, tab1);
	tabbar.relate(1, tab2);
	tabbar.activate(tab_index);

	bt_ok.events().click([&]()
	{
		success = true;
		close();
	});

	bt_cancel.events().click([&]()
	{
		close();
	});

	plc.div("<vertical <tabbar weight=25><tabs><buttons weight=50 margin=10 gap=10>>");
	plc.field("tabbar") << tabbar;
	plc.field("tabs").fasten(tab1);
	plc.field("tabs").fasten(tab2);
	plc.field("buttons") << bt_ok << bt_cancel;
	plc.collocate();
}

void SegmentEditor::load(const jsoncons::wjson& obj)
{
	loading = true;
	tab1.load(obj);
	tab2.load(obj[L"segment"]);
	loading = false;
}

jsoncons::wjson SegmentEditor::save() const
{
	jsoncons::wjson obj;
	obj = tab1.save();
	obj[L"segment"] = tab2.save();

	return obj;
}

void SegmentEditor::type(std::size_t index)
{
	if(!loading)
	{
		tab2.load(segment_types[index][L"segment"]);
	}

}

bool SegmentEditor::operator()(jsoncons::wjson& data)
{
	events().unload([&]()
	{
		if(success)
			data = save();
	});

	nana::API::modal_window(*this);
	return success;
}