// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef CURVE_EDITOR_H
#define CURVE_EDITOR_H

#include "SegmentEditor.h"

#include <nana/gui.hpp>
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/panel.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/listbox.hpp>

#include <jsoncons/json.hpp>

#include <functional>

class CurveEditor: public nana::panel<true>
{
public:
	CurveEditor(nana::window wd, const std::vector<jsoncons::wjson>& segment_types, std::function<void()> edit_callback);
	void edited(std::function<void()> callback);

	void addData(const jsoncons::wjson& obj);
	const jsoncons::wjson& getData(std::size_t i) const;
	std::size_t segments() const;

private:
	const std::vector<jsoncons::wjson>& segment_types;
	std::function<void()> edit_callback;

	nana::listbox lsbox;
	nana::button bt_new;
	nana::button bt_clear;
	nana::place plc;
	
	void OnEdit();
	void OnNew();
	void OnClear();
};

#endif // CURVE_EDITOR_H