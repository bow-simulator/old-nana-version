﻿// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "PanelParameters.h"

PanelParameters::PanelParameters(nana::window wd)
  : panel(wd),
	plc(*this),
	box_centerline(*this, L"Limb centerline"),
	box_sections(*this, L"Cross sections"),
	box_material(*this, L"Limb material"),
	box_string(*this, L"String"),
	box_masses(*this, L"Masses"),

	tbl_centerline(box_centerline.pnl(), 18, 2),
	tbl_sections(box_sections.pnl(), 18, 3),

	lb_material_E(box_material.pnl(), L"E " + units::e_modulus.label()),
	lb_material_rho(box_material.pnl(), L"\u03C1 " + units::vol_density.label()),
	tb_material_E(box_material.pnl()),
	tb_material_rho(box_material.pnl()),

	lb_string_rhoA(box_string.pnl(), L"\u03C1A " + units::lin_density.label()),
	lb_string_EA(box_string.pnl(), L"EA " + units::ltd_stiffness.label()),
	lb_string_diameter(box_string.pnl(), L"Diameter " + units::length.label()),
	lb_string_length(box_string.pnl(), L"Brace height " + units::length.label()),
	lb_string_draw(box_string.pnl(), L"Draw length " + units::length.label()),
	tb_string_rhoA(box_string.pnl()),
	tb_string_EA(box_string.pnl()),
	tb_string_diameter(box_string.pnl()),
	tb_string_brace(box_string.pnl()),
	tb_string_draw(box_string.pnl()),
						
	lb_mass_arrow(box_masses.pnl(), L"Arrow " + units::mass.label()),
	lb_mass_tip(box_masses.pnl(), L"Limb tips " + units::mass.label()),
	lb_mass_serving(box_masses.pnl(), L"String center " + units::mass.label()),
	tb_mass_arrow(box_masses.pnl()),
	tb_mass_tip(box_masses.pnl()),
	tb_mass_serving(box_masses.pnl())
{
	//Setup of the textboxes
	tb_string_rhoA.multi_lines(false);
	tb_string_EA.multi_lines(false);
	tb_string_diameter.multi_lines(false);
	tb_string_brace.multi_lines(false);
	tb_string_draw.multi_lines(false);
	tb_mass_arrow.multi_lines(false);
	tb_mass_tip.multi_lines(false);
	tb_mass_serving.multi_lines(false);

	tbl_centerline.setColumnLabel(0, L"x " + units::length.label());
	tbl_centerline.setColumnLabel(1, L"y " + units::length.label());

	tbl_sections.setColumnLabel(0, L"position " + units::percent.label());
	tbl_sections.setColumnLabel(1, L"width " + units::length.label());
	tbl_sections.setColumnLabel(2, L"height " + units::length.label());

	box_centerline.plc().div("<tbl_centerline>");
	box_centerline.plc().field("tbl_centerline") << tbl_centerline;
	box_centerline.plc().collocate();

	box_sections.plc().div("<tbl_sections>");
	box_sections.plc().field("tbl_sections") << tbl_sections;
	box_sections.plc().collocate();

	box_material.plc().div("<grid=[2,2] material gap=2>");
	box_material.plc().field("material") << lb_material_E << tb_material_E
										 << lb_material_rho << tb_material_rho;

	box_string.plc().div("<grid=[2,5] string gap=2>");
	box_string.plc().field("string") << lb_string_rhoA << tb_string_rhoA
									 << lb_string_EA << tb_string_EA
									 << lb_string_diameter << tb_string_diameter
									 << lb_string_length << tb_string_brace
									 << lb_string_draw << tb_string_draw;
	box_string.plc().collocate();

	box_masses.plc().div("<grid=[2,3] masses gap=2>");
	box_masses.plc().field("masses") << lb_mass_arrow << tb_mass_arrow
									 << lb_mass_tip << tb_mass_tip
									 << lb_mass_serving << tb_mass_serving;
	box_masses.plc().collocate();
	
	plc.div("<vertical <horizontal <<box_centerline weight=40%><box_sections> weight=63%><vertical <box_material weight=130><box_string weight=208><box_masses weight=156><>>>>");
	plc.field("box_centerline") << box_centerline;
	plc.field("box_sections") << box_sections;
	plc.field("box_material") << box_material;
	plc.field("box_string") << box_string;
	plc.field("box_masses") << box_masses;
	plc.collocate();
}

void PanelParameters::loadFromJson(const jsoncons::wjson& obj)
{
	tbl_centerline.clear();
	tbl_sections.clear();

	jsoncons::wjson centerline = obj[L"parameters"][L"centerline"];
	tbl_centerline.setColumn(0, centerline[L"x"].as<std::vector<std::wstring>>());
	tbl_centerline.setColumn(1, centerline[L"y"].as<std::vector<std::wstring>>());
	
	jsoncons::wjson sections = obj[L"parameters"][L"sections"];
	tbl_sections.setColumn(0, sections[L"pos"].as<std::vector<std::wstring>>());
	tbl_sections.setColumn(1, sections[L"width"].as<std::vector<std::wstring>>());
	tbl_sections.setColumn(2, sections[L"height"].as<std::vector<std::wstring>>());

	jsoncons::wjson material = obj[L"parameters"][L"material"];
	tb_material_rho.caption(material[L"rho"].as<std::wstring>());
	tb_material_E.caption(material[L"E"].as<std::wstring>());

	jsoncons::wjson string = obj[L"parameters"][L"string"];
	tb_string_rhoA.caption(string[L"rhoA"].as<std::wstring>());
	tb_string_EA.caption(string[L"EA"].as<std::wstring>());
	tb_string_diameter.caption(string[L"diameter"].as<std::wstring>());
	tb_string_brace.caption(string[L"brace_height"].as<std::wstring>());
	tb_string_draw.caption(string[L"draw_length"].as<std::wstring>());
	
	jsoncons::wjson masses = obj[L"parameters"][L"masses"];
	tb_mass_arrow.caption(masses[L"arrow"].as<std::wstring>());
	tb_mass_tip.caption(masses[L"limb_tip"].as<std::wstring>());
	tb_mass_serving.caption(masses[L"serving"].as<std::wstring>());

	edited_reset();
}

void PanelParameters::saveToJson(jsoncons::wjson& obj)
{
	jsoncons::wjson centerline;
	std::vector<double> vec_x = tbl_centerline.getColumn(0);
	std::vector<double> vec_y = tbl_centerline.getColumn(1);
	jsoncons::wjson x(vec_x.begin(), vec_x.end());
	jsoncons::wjson y(vec_y.begin(), vec_y.end());
	centerline[L"x"] = x;
	centerline[L"y"] = y;

	jsoncons::wjson sections;
	std::vector<double> vec_pos = tbl_sections.getColumn(0);
	std::vector<double> vec_width = tbl_sections.getColumn(1);
	std::vector<double> vec_height = tbl_sections.getColumn(2);
	jsoncons::wjson pos(vec_pos.begin(), vec_pos.end());
	jsoncons::wjson width(vec_width.begin(), vec_width.end());
	jsoncons::wjson height(vec_height.begin(), vec_height.end());
	sections[L"pos"] = pos;
	sections[L"width"] = width;
	sections[L"height"] = height;

	jsoncons::wjson material;
	material[L"rho"] = tb_material_rho.to_double();
	material[L"E"] = tb_material_E.to_double();

	jsoncons::wjson string;
	string[L"rhoA"] = tb_string_rhoA.to_double();
	string[L"EA"] = tb_string_EA.to_double();
	string[L"diameter"] = tb_string_diameter.to_double();
	string[L"brace_height"] = tb_string_brace.to_double();
	string[L"draw_length"] = tb_string_draw.to_double();

	jsoncons::wjson masses;
	masses[L"arrow"] = tb_mass_arrow.to_double();
	masses[L"limb_tip"] = tb_mass_tip.to_double();
	masses[L"serving"] = tb_mass_serving.to_double();
	
	jsoncons::wjson parameters;
	parameters[L"material"] = material;
	parameters[L"string"] = string;
	parameters[L"masses"] = masses;
	parameters[L"centerline"] = centerline;
	parameters[L"sections"] = sections;

	obj[L"parameters"] = parameters;

	edited_reset();
}

bool PanelParameters::edited() const
{
	return tbl_centerline.edited() || tbl_sections.edited() || tb_material_rho.edited() || tb_material_E.edited() || tb_string_rhoA.edited()
		|| tb_string_EA.edited() || tb_string_diameter.edited() || tb_string_brace.edited() || tb_string_draw.edited()
		|| tb_mass_arrow.edited() || tb_mass_tip.edited() || tb_mass_serving.edited();
}

void PanelParameters::edited_reset()
{
	tbl_centerline.edited_reset();
	tbl_sections.edited_reset();
	tb_material_rho.edited_reset();
	tb_material_E.edited_reset();
	tb_string_rhoA.edited_reset();
	tb_string_EA.edited_reset();
	tb_string_diameter.edited_reset();
	tb_string_brace.edited_reset();
	tb_string_draw.edited_reset();
	tb_mass_arrow.edited_reset();
	tb_mass_tip.edited_reset();
	tb_mass_serving.edited_reset();
}