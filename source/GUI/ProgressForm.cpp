// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "ProgressForm.h"

ProgressForm::ProgressForm(nana::window wd, bool dynamic)
	: nana::form(wd, nana::API::make_center(wd, 300, 160), appear::decorate<>()),
	  lb1(*this, L"Statics"),
	  lb2(*this, L"Dynamics"),
	  pbar1(*this),
	  pbar2(*this),
	  btn_cancel(*this, L"Cancel"),
	  plc(*this),
	  canceled(false),
	  closed(false)
{
	caption(L"Simulation progress");
	lb1.text_align(nana::align::left, nana::align_v::bottom);
	lb2.text_align(nana::align::left, nana::align_v::bottom);

	plc.div("<vertical <vertical a margin=[0,10]><cancel weight=31% margin=[10,100]>>");
	plc.field("a") << lb1 << pbar1 << lb2 << pbar2;
	plc.field("cancel") << btn_cancel;
	plc.collocate();

	if(!dynamic)
	{
		lb2.hide();
		pbar2.hide();
	}
	
	btn_cancel.events().click([&]()
	{
		canceled = true;
	});

	events().unload([&](const nana::arg_unload& arg)
	{
		arg.cancel = !closed;
		canceled = true;
	});
}

bool ProgressForm::progress1(unsigned int p)
{
	pbar1.value(p);
	return !canceled;
}

bool ProgressForm::progress2(unsigned int p)
{
	pbar2.value(p);
	return !canceled;
}

void ProgressForm::close()
{
	closed = true;
	nana::form::close();
}