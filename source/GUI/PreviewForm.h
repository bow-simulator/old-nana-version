// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef PREVIEW_FORM_H
#define PREVIEW_FORM_H

#include "../Bow/Bow.h"
#include "../Bow/LimbGeometry.h"
#include "../Bow/Units.h"
#include "Plot/Plot.h"

#include <nana/gui.hpp>
#include <nana/gui/widgets/tabbar.hpp>
#include <nana/gui/place.hpp>

#include <vector>

#include <Eigen/Core>

class PreviewForm: public nana::form
{
public:
	PreviewForm(nana::window wd, const nana::string& filename, const BowParameters& parameters, const BowSettings& settings);

private:
	LimbGeometry limb;
	Plot plot_shape;
	Plot plot_w;
	Plot plot_h;

	nana::tabbar<std::wstring> tabs;
	nana::place plc;

	void init_plots(const BowParameters& parameters);
};

#endif // PREVIEW_FORM