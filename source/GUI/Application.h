// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef APPLICATION_H
#define APPLICATION_H

#include "../Bow/Bow.h"
#include "../Bow/LimbGeometry.h"
#include "../Bow/StaticData.h"
#include "../Bow/DynamicData.h"

#include "PanelParameters.h"
#include "PanelSettings.h"
#include "PanelComments.h"
#include "PreviewForm.h"
#include "ProgressForm.h"
#include "OutputForm.h"

#include <jsoncons/json.hpp>

#include <nana/gui.hpp>
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/menu.hpp>
#include <nana/gui/widgets/menubar.hpp>
#include <nana/gui/widgets/tabbar.hpp>
#include <nana/gui/filebox.hpp>
#include <nana/filesystem/fs_utility.hpp>
#include <nana/gui/msgbox.hpp>
#include <nana/threads/pool.hpp>

#include <functional>
#include <iostream>
#include <memory>

/**
Top level class which is instantiated in the main method of the program.\n
It handles the setup of the GUI and responds to menu commands like loading/saving files, running the simulation or exiting the program.
\todo Ask user if he wants to override file when an already existing file is chosen in save_as
*/
class Application: public nana::form
{
public:
	static const std::string version;
	static int cmdSimulationRun(const std::wstring& path_in, const std::wstring& path_out_s, const std::wstring& path_out_d);

	Application(const nana::string& path = L"");
	bool load();
	bool load(const nana::string& path);
	bool save(const nana::string& path);

private:
	nana::string filepath;
	nana::string filename;

	nana::menubar menubar;
	nana::tabbar<std::wstring> tabbar;
	PanelParameters pnl_parameters;
	PanelSettings pnl_settings;
	PanelComments pnl_comments;
	nana::place plc;

	void loadFromJson(const jsoncons::wjson& obj);
	void saveToJson(jsoncons::wjson& obj);

	bool askToSave();
	bool edited();
	void edited_reset();
	void error(std::exception& e);

	void fileNew();
	void fileOpen();
	void fileSave();
	void fileSaveAs();
	void fileExit();
	void viewPreview();
	void simulationRun(bool dynamic);
	void helpAbout();
};

class CmdProgress
{
public:
	CmdProgress(unsigned int step): step(step), progress(0)
	{
		display();
	}

	void set(unsigned int prg)
	{
		if(prg - progress >= step)
		{
			progress += step;
			display();
		}
	}

private:
	unsigned int step;
	unsigned int progress;

	void display()
	{
		std::cout << "\r" << progress << "% completed";
	}

};


#endif // APPLICATION_H