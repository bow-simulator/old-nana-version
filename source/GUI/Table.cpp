// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "Table.h"

Table::Table(nana::window wd, std::size_t rows, std::size_t cols): panel(wd), rows(rows), cols(cols), plc(*this)
{
	std::stringstream stream;
	stream << "<table grid=[" << cols << "," << rows + 1 << "] gap=2>";
	plc.div(stream.str().data());

	//Column labels
	for(std::size_t j = 0; j < cols; ++j)
	{
		std::unique_ptr<nana::label> lb(new nana::label(*this));
		lb->text_align(nana::align::center);
		plc.field("table") << *lb;
		labels.push_back(std::move(lb));
	}

	//Textboxes
	for(std::size_t i = 0; i < rows; ++i)
	{
		cells.push_back(std::vector<std::unique_ptr<nana::textbox>>());

		for(std::size_t j = 0; j < cols; j++)
		{
			std::unique_ptr<nana::textbox> box(new nana::textbox(*this));
			box->multi_lines(false);
			plc.field("table") << *box;
			cells[i].push_back(std::move(box));
		}
	}

	plc.collocate();
}

void Table::setColumnLabel(std::size_t col, nana::string text)
{
	labels[col]->caption(text);
}

double Table::getCellEntry(std::size_t row, std::size_t col) const
{
	return std::stod(cells[row][col]->caption());
}

void Table::setCellEntry(std::size_t row, std::size_t col, double val)
{
	cells[row][col]->from(val);
}

void Table::setCellEntry(std::size_t row, std::size_t col, nana::string val)
{
	cells[row][col]->caption(val);
}

std::vector<double> Table::getColumn(std::size_t col)
{
	std::vector<double> vec;
	for(std::size_t i = 0; i < getRows(); ++i)
	{
		try
		{
			vec.push_back(getCellEntry(i, col));
		}
		catch(std::exception& e)
		{
			break;
		}
	}

	return vec;
}

void Table::setColumn(std::size_t col, const std::vector<double>& vec)
{
	for(std::size_t i = 0; i < vec.size(); ++i)
	{
		setCellEntry(i, col, vec[i]);
	}
}

void Table::setColumn(std::size_t col, const std::vector<nana::string>& vec)
{
	for(std::size_t i = 0; i < vec.size(); ++i)
	{
		setCellEntry(i, col, vec[i]);
	}
}

void Table::clear()
{
	for(std::size_t i = 0; i < getRows(); ++i)
	{
		for(std::size_t j = 0; j < getCols(); ++j)
		{
			cells[i][j]->caption(L"");
		}
	}
}

bool Table::edited() const
{
	bool ed = false;

	for(std::size_t i = 0; i < getRows(); ++i)
	{
		for(std::size_t j = 0; j < getCols(); ++j)
		{
			ed = ed || cells[i][j]->edited();
		}
	}

	return ed;
}

void Table::edited_reset()
{
	for(std::size_t i = 0; i < getRows(); ++i)
	{
		for(std::size_t j = 0; j < getCols(); ++j)
		{
			cells[i][j]->edited_reset();
		}
	}
}

std::size_t Table::getRows() const
{
	return rows;
}

std::size_t Table::getCols() const
{
	return cols;
}