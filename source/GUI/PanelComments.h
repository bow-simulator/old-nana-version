// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef PANEL_COMMENTS_H
#define PANEL_COMMENTS_H

#include "GroupBox.h"
#include "../Bow/Units.h"

#include <nana/gui.hpp>
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/textbox.hpp>

#include <jsoncons/json.hpp>

#include <stdexcept>

class PanelComments: public nana::panel<false>
{
public:
	PanelComments(nana::window wd);

	void loadFromJson(const jsoncons::wjson& obj);
	void saveToJson(jsoncons::wjson& obj);

	bool edited();
	void edited_reset();

private:
	nana::textbox tb_comments;
	nana::place plc;
};

#endif // PANEL_COMMENTS_H