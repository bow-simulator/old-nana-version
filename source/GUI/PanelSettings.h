// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef PANEL_SETTINGS_H
#define PANEL_SETTINGS_H

#include "GroupBox.h"
#include "../Bow/Bow.h"
#include "../Bow/Units.h"

#include <nana/gui.hpp>
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/textbox.hpp>
#include <nana/gui/widgets/checkbox.hpp>

#include <jsoncons/json.hpp>

#include <stdexcept>

/**
This panel groups together all GUI elements for the input of the bow parameters
*/
class PanelSettings: public nana::panel<false>
{
public:
	PanelSettings(nana::window wd);
	void loadFromJson(const jsoncons::wjson& obj);
	void saveToJson(jsoncons::wjson& obj);

	bool edited();
	void edited_reset();

private:
	GroupBox box_settings;
	nana::label lb_elements;
	nana::label lb_timestep_factor;
	nana::textbox tb_elements;
	nana::textbox tb_timestep_factor;
	nana::checkbox cb_contact;
	nana::place plc;
};

#endif // PANEL_SETTINGS_H