// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef PROGRESS_FORM_H
#define PROGRESS_FORM_H

#include <nana/gui.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/progress.hpp>
#include <nana/gui/place.hpp>

class ProgressForm: public nana::form
{
public:
	ProgressForm(nana::window wd, bool dynamic);
	bool progress1(unsigned int p);
	bool progress2(unsigned int p);
	void close();

private:
	nana::label lb1;
	nana::label lb2;
	nana::progress pbar1;
	nana::progress pbar2;
	nana::button btn_cancel;
	nana::place plc;

	bool canceled;
	bool closed;
};

#endif // PROGRESS_FORM_H