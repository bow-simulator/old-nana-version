// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef AXIS_H
#define AXIS_H

#include "NumberFormat.h"

#include <nana/gui.hpp>
#include "Drawer.h"

#include <vector>
#include <sstream>
#include <iostream>

struct NumberLabel
{
	int pos;
	nana::string text;

	NumberLabel(int pos, nana::string text): pos(pos), text(text)
	{

	}

	NumberLabel()
	{

	}
};


class Axis
{
public:
	Axis(double min_val, double max_val, bool horizontal);

	void label(const std::wstring& lb);
	const std::wstring& label() const;

	void min(double min);
	double min() const;
	void max(double max);
	double max() const;

	void round();
	void setView(double val0, double val1);
	void shiftView(double delta);

	void scale(double scl);
	double scale() const;
	void grid(bool gd);

	void minTickSize(unsigned int pxl);
	unsigned int minTickSize() const;

	NumberFormat& numberFormat();
	
	void Axis::draw(Drawer& graph, const nana::rectangle& r);
	int coordToPixel(double value, int length) const;
	double pixelToCoord(int pixel, int length) const;

private:
	std::wstring label_;
	double min_data;
	double max_data;
	double min_view;
	double max_view;
	double scale_;
	bool horizontal;
	//bool update_limits;
	bool grid_;

	NumberFormat num_format;
	std::vector<NumberLabel> num_labels;
		
	void updateLabels(int length, int n);
};

#endif // AXIS_H


//Not working template-version

/*
#include "NumberFormat.h"

#include <nana/gui.hpp>

#include <vector>
#include <sstream>
#include <iostream>

struct NumberLabel
{
	int pos;
	nana::string text;

	NumberLabel(int pos, nana::string text): pos(pos), text(text)
	{

	}

	NumberLabel()
	{

	}

};

template<bool vertical>
class Axis
{
public:
	Axis(double min, double max);

	void range(double min, double max);
	void min(double min);
	double min() const;
	void max(double max);
	double max() const;
	void round();
	void scale(double scl);
	double scale() const;
	void grid(bool gd);

	void minTickSize(unsigned int pxl);
	unsigned int minTickSize() const;

	NumberFormat& numberFormat();
	
	void Axis::draw(nana::paint::graphics& graph, const nana::rectangle& r);
	int coordToPixel(double value, int length) const;

private:
	double lower;
	double upper;
	double scale_;
	//bool update_limits;
	unsigned int min_tick_size;
	bool grid_;

	NumberFormat num_format;
	std::vector<NumberLabel> num_labels;
		
	void updateLabels(int length, int label_width);

};




template<bool vertical>
Axis<vertical>::Axis(double min, double max): num_format(), scale_(1.0), min_tick_size(50), grid_(false)//, update_limits(false)
{
	range(min, max);
}


template<bool vertical>
void Axis<vertical>::range(double min, double max)
{
	lower = min;
	upper = max;
	num_format.update(scale_*lower, scale_*upper);
}


template<bool vertical>
void Axis<vertical>::min(double min)
{
	lower = min;
	num_format.update(lower, upper);
}


template<bool vertical>
double Axis<vertical>::min() const
{
	return lower;
}


template<bool vertical>
void Axis<vertical>::max(double max)
{
	upper = max;
	num_format.update(lower, upper);
}


template<bool vertical>
double Axis<vertical>::max() const
{
	return upper;
}


template<bool vertical>
void Axis<vertical>::round()
{
	//update_limits = true;
}


template<bool vertical>
void Axis<vertical>::scale(double scl)
{
	scale_ = scl;
}


template<bool vertical>
double Axis<vertical>::scale() const
{
	return scale_;
}


template<bool vertical>
void Axis<vertical>::grid(bool gd)
{
	grid_ = gd;
}


template<bool vertical>
void Axis<vertical>::minTickSize(unsigned int pxl)
{
	min_tick_size = pxl;
}


template<bool vertical>
unsigned int Axis<vertical>::minTickSize() const
{
	return min_tick_size;
}


template<bool vertical>
NumberFormat& Axis<vertical>::numberFormat()
{
	return num_format;
}


template<bool vertical>
void Axis<vertical>::updateLabels(int length, int label_width)
{

	double min = scale_*lower;
	double max = scale_*upper;

	double epsilon = (max - min)*label_width/length;
	double delta = std::numeric_limits<double>::max();
	
	for(int i: {1, 2, 5})
	{
		int ni = std::ceil(std::log10(epsilon/i));
		double delta_i = i*std::pow(10, ni);

		if(delta_i < delta)
		{
			delta = delta_i;
		}

	}
	
	//Loop through label values
	num_labels.clear();
	double value = std::ceil(min/delta)*delta;
	while(value <= max)
	{
		//std::cout << "value: " << value << ", ";
		num_labels.push_back(NumberLabel(coordToPixel(value/scale_, length), num_format.numberToString(value)));
		value += delta;
	}
	//std::cout << "\n";

}


template<bool vertical>
void Axis<vertical>::draw(nana::paint::graphics& graph, const nana::rectangle& r)
{

	for (size_t i = 0; i < num_labels.size(); ++i)
	{

		if(vertical)
		{

			updateLabels(r.height, min_tick_size);

			int y_pos = r.y + r.height - 1 - num_labels[i].pos;
			nana::size size_y_label = graph.text_extent_size(num_labels[i].text);

			//Number tick and label
			graph.line(r.x + 1, y_pos, r.x + 8, y_pos, 0x000000);
			graph.string(r.x - size_y_label.width - size_y_label.height/2, y_pos - size_y_label.height/2 + 1, 0x000000, num_labels[i].text);

			//Grid line
			if(grid_ && y_pos > r.y && y_pos < r.y + static_cast<int>(r.height) - 1)
			{
				graph.line(r.x + 9, y_pos, r.x + r.width - 2, y_pos, 0xC2C2C2);
			}

		}
		else
		{

			updateLabels(r.width, min_tick_size);

			int x_pos = r.x + num_labels[i].pos;
			nana::size size_x_label = graph.text_extent_size(num_labels[i].text);

			//Number tick and label
			graph.line(x_pos, r.y + r.height - 2, x_pos, r.y + r.height - 9, 0x000000);

			graph.string(x_pos - size_x_label.width/2, r.y + r.height,
						 0x000000, num_labels[i].text);

			//Grid line
			if (grid_ && x_pos > r.x && x_pos < r.x + static_cast<int>(r.width) - 1)
			{
				graph.line(x_pos, r.y + 1, x_pos, r.y + r.height - 10, 0xC2C2C2);
			}

		}

	}

}



template<bool vertical>
int Axis<vertical>::coordToPixel(double value, int length) const
{
	return static_cast<int>((length-1)*(value-lower)/(upper-lower));
}
*/