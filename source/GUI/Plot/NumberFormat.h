// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef NUMBER_FORMAT_H
#define NUMBER_FORMAT_H

#include <nana/gui.hpp>

#include "Drawer.h"

#include <iostream>
#include <cmath>
#include <algorithm>

class NumberFormat
{
public:
	NumberFormat(unsigned pre_decimal = 4, unsigned post_decimal = 3)
		: pre_decimal(pre_decimal), post_decimal(post_decimal), max_width(0), max_height(0)
	{
		//stream.setf(std::ios::scientific, std::ios::floatfield);
		stream.precision(post_decimal);
	}

	nana::size maxSize(const Drawer& graph) const
	{
		if(max_width == 0)
		{
			unsigned int width_standard = graph.textWidth(numberToString(-pow(10.0, pre_decimal)-pow(10.0, -(int)post_decimal)));
			unsigned int width_scientific = graph.textWidth(numberToString(-pow(10.0, pre_decimal)));

			max_width = std::max(width_standard, width_scientific);
			max_height = graph.textHeight();
		}

		return nana::size(max_width, max_height);
	}

	void update(double min, double max)
	{
		if(    abs(static_cast<int>(min)) > pow(10, pre_decimal)-1  //Absolute vaule of the pre-decimal part larger than largest integer with pre_decimal places...
			|| abs(static_cast<int>(max)) > pow(10, pre_decimal)-1
			|| (abs(min) < pow(10, -static_cast<int>(post_decimal)) && min != 0.0)
			|| (abs(max) < pow(10, -static_cast<int>(post_decimal)) && max != 0.0))                   //Number smaller than precision representable with fixed
		{
			//Change to scientific
			stream.setf(std::ios::scientific, std::ios::floatfield);
		}
		else
		{
			//Default
			stream.setf(std::ios::fixed, std::ios::floatfield);
		}
	}

	//Taken from "Der C++ Programmierer", page 394
	nana::string numberToString(double value) const
	{
		stream.str(nana::string());
		stream << value;

		nana::string result = stream.str();

		//Uncomment to erase trailing zeros, stringstream won't do this when the precision is specified >:(
		result.erase(result.find_last_not_of('0') + 1, nana::string::npos);
		if(result[result.size()-1] == '.')
		{
			result.erase(result.end()-1);
		}

		return result;
	}

private:
	mutable unsigned int max_width;
	mutable unsigned int max_height;
	unsigned pre_decimal;
	unsigned post_decimal;
	mutable std::wstringstream stream;
};

#endif // NUMBER_FORMAT_H

//sprintf-version:
/*

#ifndef NUMBER_FORMAT_H
#define NUMBER_FORMAT_H

#include <nana/gui.hpp>

#include <iostream>
#include <cmath>
#include <cstdio>
#include <algorithm>

class NumberFormat
{
public:
	NumberFormat(unsigned pre_decimal = 6, unsigned post_decimal = 4): pre_decimal(pre_decimal), post_decimal(post_decimal), format(L"%f")
	{

	}

	nana::size size(nana::paint::graphics graph) const
	{
		//Absch�tzung
		int width = std::max((pre_decimal + post_decimal)*graph.text_extent_size(L"0").width + graph.text_extent_size(L"-.").width,
			                 (post_decimal + 4)*graph.text_extent_size(L"0").width + graph.text_extent_size(L"-.e-").width);

		return nana::size(width, graph.text_extent_size(L"0").height);
	}


	void update(double lower, double upper)
	{
		
		if(    std::abs(static_cast<int>(lower)) > std::pow(10, pre_decimal)-1  //Betrag des vor-komma-Teils gr��er als gr��ter integer mit pre_decimal stellen...
			|| std::abs(static_cast<int>(upper)) > std::pow(10, pre_decimal)-1
			|| (std::abs(lower) < std::pow(10, -static_cast<int>(post_decimal)) && lower != 0.0)
			|| (std::abs(upper) < std::pow(10, -static_cast<int>(post_decimal)) && upper != 0.0))            //Zahl kleiner als mit fixed darstellbare Genauigkeit
		{
			//Wechsel zur scientific-Darstellung
		}
		else
		{
			//Defaultdarstellung
		}

		max_length = std::max(pre_decimal + post_decimal + 2, post_decimal + 8);

	}


	nana::string numberToString(double value)
	{	
		wchar_t result[20];
		swprintf(result, 20, format, value);
		return nana::string(result);
	}

private:
	unsigned pre_decimal;
	unsigned post_decimal;
	unsigned max_length;
	wchar_t* format;

};


#endif // NUMBER_FORMAT_H
*/