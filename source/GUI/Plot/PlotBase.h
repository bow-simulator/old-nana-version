// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef PLOT_BASE_H
#define PLOT_BASE_H

#include <nana/gui.hpp>

#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>
#include <string>
#include <sstream>

#include "PlotData.h"
#include "Axis.h"
#include "Drawer.h"

struct point
{
	double x;
	double y;
};

class PlotBase
{
public:
	PlotBase();

	void addPlotData(PlotData d);
	std::vector<PlotData>& getData();
	Axis& getXAxis();
	Axis& getYAxis();
	void fit(bool fit_x_min = true, bool fit_y_min = true, bool fit_x_max = true, bool fit_y_max = true);
	void fit125(bool fit_x_min = true, bool fit_y_min = true, bool fit_x_max = true, bool fit_y_max = true);
	void grid(bool gd);

	void title(std::wstring tl);
	const std::wstring& title() const;

	void zoom(const nana::point& pos, double factor);
	void zoom(const nana::point& start, const nana::point& end);
	void zoomBox(const nana::point& start, const nana::point& end);
	void zoomBox();
	void pan(const nana::point& start, const nana::point& end);

	void draw(Drawer& graph);
	
private:
	std::wstring title_;
	std::vector<PlotData> data;
	Axis x_axis;
	Axis y_axis;

	int x_margin;
	int y_margin;

	nana::rectangle widget_area;
	nana::rectangle plot_area;
	nana::rectangle zoom_box;
	bool zoom_box_active;

	point pixelToCoordinates(const nana::point& pos);
};

#endif // PLOT_BASE_H