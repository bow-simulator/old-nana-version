// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "Axis.h"

Axis::Axis(double min_val, double max_val, bool horizontal)
	: num_format(),
	  scale_(1.0),
	  horizontal(horizontal),
	  grid_(false)//, update_limits(false)
{
	min(min_val);
	max(max_val);
}

void Axis::label(const std::wstring& lb)
{
	label_ = lb;
}

const std::wstring& Axis::label() const
{
	return label_;
}

void Axis::min(double min)
{
	min_view = min;
	min_data = min;
	num_format.update(scale_*min_view, scale_*max_view);
}

double Axis::min() const
{
	return min_view;
}

void Axis::max(double max)
{
	max_view = max;
	max_data = max;
	num_format.update(scale_*min_view, scale_*max_view);
}

double Axis::max() const
{
	return max_view;
}

void Axis::round()
{
	//update_limits = true;
}

void Axis::setView(double val0, double val1)
{
	//Sort values
	if(val0 > val1)
	{
		std::swap(val0, val1);
	}

	//Change only displayed start and end values min_ and max_, not the borders. Also do not exceed border limits
	min_view = std::max(val0, min_data);
	max_view = std::min(val1, max_data);

}

void Axis::shiftView(double delta)
{
	if(min_view + delta >= min_data && max_view + delta <= max_data)
	{
		min_view += delta;
		max_view += delta;
	}
}

void Axis::scale(double scl)
{
	scale_ = scl;
}

double Axis::scale() const
{
	return scale_;
}

void Axis::grid(bool gd)
{
	grid_ = gd;
}

NumberFormat& Axis::numberFormat()
{
	return num_format;
}

void Axis::updateLabels(int length, int n)
{
	double min = scale_*min_view;
	double max = scale_*max_view;

	double epsilon = (max - min)/n;
	double delta = std::numeric_limits<double>::max();
	
	for(int i: {1, 2, 5})
	{
		int ni = std::ceil(std::log10(epsilon/i));
		double delta_i = i*std::pow(10, ni);

		if(delta_i < delta)
		{
			delta = delta_i;
		}
	}

	/*
	if(update_limits)
	{
		min = std::floor(min/delta)*delta;
		max = std::ceil(max/delta)*delta;

		std::cout << "min: " << min << ", max: " << max << "\n";

		lower = min/scale_;
		upper = max/scale_;
		update_limits = false;
	}
	*/
	
	//Loop through label values
	num_labels.clear();
	double value = std::ceil(min/delta)*delta;
	while(value <= max)
	{
		//std::cout << "value: " << value << ", ";
		num_labels.push_back(NumberLabel(coordToPixel(value/scale_, length), num_format.numberToString(value)));
		value += delta;
	}

	//std::cout << "\n";
}

void Axis::draw(Drawer& graph, const nana::rectangle& r)
{
	const double tick_spacing = 1.5;

	if(horizontal)
	{
		updateLabels(r.width, tick_spacing*r.width/num_format.maxSize(graph).width);

		for (std::size_t i = 0; i < num_labels.size(); ++i)
		{
			int x_pos = r.x + num_labels[i].pos;
			nana::size size_x_label(graph.textWidth(num_labels[i].text), graph.textHeight());

			//Major tick and label
			graph.line(x_pos, r.y + r.height - 2, x_pos, r.y + r.height - 9, 0x000000);
			graph.text(x_pos - size_x_label.width/2, r.y + r.height + size_x_label.height/2, num_labels[i].text, 0x000000, false);

			//Grid line
			if (grid_ && x_pos > r.x && x_pos < r.x + static_cast<int>(r.width) - 1)
			{
				graph.line(x_pos, r.y + 1, x_pos, r.y + r.height - 10, 0xC2C2C2);
			}
		}
	}
	else
	{
		updateLabels(r.height, tick_spacing*r.height/num_format.maxSize(graph).width);

		for (std::size_t i = 0; i < num_labels.size(); ++i)
		{
			int y_pos = r.y + r.height - 1 - num_labels[i].pos;
			nana::size size_y_label(graph.textWidth(num_labels[i].text), graph.textHeight());

			//Number tick and label
			graph.line(r.x + 1, y_pos, r.x + 8, y_pos, 0x000000);
			graph.text(r.x - size_y_label.width - size_y_label.height/2, y_pos - size_y_label.height/2 + 1, num_labels[i].text, 0x000000, false);

			//Grid line
			if(grid_ && y_pos > r.y && y_pos < r.y + static_cast<int>(r.height) - 1)
			{
				graph.line(r.x + 9, y_pos, r.x + r.width - 2, y_pos, 0xC2C2C2);
			}
		}
	}
}

int Axis::coordToPixel(double value, int length) const
{
	return static_cast<int>((length-1)*(value-min_view)/(max_view-min_view));
}

double Axis::pixelToCoord(int pixel, int length) const
{
	return min_view + pixel*(max_view-min_view)/length;
}