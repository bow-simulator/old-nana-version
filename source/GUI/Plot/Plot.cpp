// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "Plot.h"

Plot::Plot(nana::window wd): panel(wd), dw(*this)
{
	mobj.append(L"Copy data", std::bind(&Plot::menu_copy, this, std::placeholders::_1));
	events().click(menu_popuper(mobj));

	events().mouse_wheel([&](const nana::arg_wheel& arg)
	{
		if(arg.upwards)
		{
			zoom(arg.pos, 0.75);
		}
		else
		{
			zoom(arg.pos, 1.25);
		}

		dw.update();
	});

	events().mouse_down([&](const nana::arg_mouse& arg)
	{
		if(arg.left_button)
		{
			down_left = arg.pos;
			zoomBox();
		}
		else if(arg.mid_button)
		{
			down_middle = arg.pos;
		}
	});

	events().mouse_up([&](const nana::arg_mouse& arg)
	{
		if(arg.left_button)
		{
			zoom(down_left, arg.pos);
			dw.update();
		}
	});

	events().mouse_move([&](const nana::arg_mouse& arg)
	{
		if(arg.left_button)
		{
			zoomBox(down_left, arg.pos);
			dw.update();
		}
		else if(arg.mid_button)
		{
			pan(down_middle, arg.pos);
			down_middle = arg.pos;
			dw.update();
		}
	});

	dw.draw([&](nana::paint::graphics& grp)
	{
		draw(GdipDrawer(grp));
	});

	dw.update();
}

void Plot::update()
{
	dw.update();
}

/*
void Plot::saveAsFile(const char* path, nana::size s)
{
	nana::paint::graphics graph(s);
	draw(graph);
	graph.save_as_file(path);
}
*/

/*
void Plot::saveAsFile(const char* path)
{
	nana::paint::graphics graph(this->size());
	draw(graph);
	graph.save_as_file(path);
}
*/

void Plot::plot(const std::vector<double>& x, const std::vector<double>& y, std::wstring title, std::wstring x_label, std::wstring y_label)
{
	nana::form fm(nana::API::make_center(1000, 600));

	Plot pl(fm);
	pl.title(title);
	pl.getXAxis().label(x_label);
	pl.getYAxis().label(y_label);
	pl.grid(true);
	pl.addPlotData(PlotData(x, y, LineFormat(LineStyle::SOLID, 0x00008B)));
	pl.fit125();
	
	nana::place plc(fm);
	plc.div("<plot>");
	plc.field("plot") << pl;
	plc.collocate();

	fm.show();
	nana::exec();
}

void Plot::menu_copy(nana::menu::item_proxy& ip)
{
	nana::string text;

	for (std::size_t i = 0; i < getData().size(); ++i)
	{
		for (std::size_t j = 0; j < getData()[i].x().size(); ++j)
		{
			text += std::to_wstring(getXAxis().scale()*getData()[i].x()[j]) + L"\t" + std::to_wstring(getYAxis().scale()*getData()[i].y()[j]) + L"\n";
		}

		text += L"\n";
	}

	nana::system::dataexch de;
	de.set(text);
}