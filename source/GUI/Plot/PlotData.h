// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef PLOT_DATA_H
#define PLOT_DATA_H

#include <nana/gui.hpp>

#include "Axis.h"
#include "Drawer.h"

#include <vector>
#include <array>

enum class LineStyle
{
	NONE,
	SOLID
};

struct LineFormat
{
	static std::array<unsigned int, 7> default_colors;
	LineStyle style;
	unsigned int color;

	LineFormat(LineStyle style = LineStyle::SOLID, unsigned int color = 0x00008B): style(style), color(color)
	{

	}

	LineFormat(unsigned int color_index): style(LineStyle::SOLID), color(default_colors[color_index % default_colors.size()])
	{

	}

	void draw(Drawer& graph, nana::point p0, nana::point p1) const
	{
		if(style == LineStyle::SOLID)
		{
			graph.line(p0.x, p0.y, p1.x, p1.y, color);
		}
	}
};

enum class MarkerStyle
{
	NONE,
	PLUS,
	ASTERISK,
	CROSS,
	SQUARE,
	DIAMOND
};

class MarkerFormat
{
public:
	MarkerStyle style;
	unsigned int color;

	MarkerFormat(MarkerStyle style = MarkerStyle::NONE, unsigned int color = 0x00008B): style(style), color(color)
	{

	}

	void draw(Drawer& graph, const nana::point& p) const
	{
		switch(style)
		{
		case MarkerStyle::NONE:
			//Do nothing
		break;

		case MarkerStyle::PLUS:
			graph.line(p.x-4, p.y, p.x+4, p.y, color);
			graph.line(p.x, p.y-4, p.x, p.y+4, color);
		break;

		case MarkerStyle::ASTERISK:
			graph.line(p.x-4, p.y, p.x+4, p.y, color);
			graph.line(p.x, p.y-4, p.x, p.y+4, color);
			graph.line(p.x-3, p.y-3, p.x+3, p.y+3, color);
			graph.line(p.x-3, p.y+3, p.x+3, p.y-3, color);
		break;

		case MarkerStyle::CROSS:
			graph.line(p.x-4, p.y-4, p.x+4, p.y+4, color);
			graph.line(p.x-4, p.y+4, p.x+4, p.y-4, color);
		break;

		case MarkerStyle::SQUARE:
			graph.rectangle(p.x-4, p.y-4, 9, 9, color, false);
		break;

		case MarkerStyle::DIAMOND:
			graph.line(p.x, p.y-4, p.x-4, p.y, color);
			graph.line(p.x-4, p.y, p.x, p.y+4, color);
			graph.line(p.x, p.y+4, p.x+4, p.y, color);
			graph.line(p.x+4, p.y, p.x, p.y-4, color);
		break;
		};
	}
};

class PlotData
{
public:
	PlotData(const std::vector<double>& x_values,
			 const std::vector<double>& y_values,
			 LineFormat line_fmt = LineFormat(),
			 MarkerFormat marker_fmt = MarkerFormat());

	const std::vector<double>& x() const;
	const std::vector<double>& y() const;
	
	double x_min() const;
	double x_max() const;
	double y_min() const;
	double y_max() const;
	
	LineFormat& lineFormat();
	MarkerFormat& markerFormat();

	void visible(bool vb);
	bool visible() const;

	void draw(Drawer& graph, const nana::rectangle& r, const Axis& x_axis, const Axis& y_axis) const;

private:
	const std::vector<double>& x_val;
	const std::vector<double>& y_val;

	bool visible_;
	LineFormat line_fmt;
	MarkerFormat marker_fmt;
};

#endif // PLOT_DATA_H