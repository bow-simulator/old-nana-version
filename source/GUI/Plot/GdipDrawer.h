// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef GDIP_DRAWER_H
#define GDIP_DRAWER_H

#include "Drawer.h"

#include <nana/gui.hpp>

#include <windows.h>
#include <objidl.h>
#include <gdiplus.h>
#pragma comment (lib,"Gdiplus.lib")

//No, thanks...
#undef min
#undef max

class GdipDrawer : public Drawer
{
public:
	GdipDrawer(nana::paint::graphics& graph);

	virtual unsigned int width() const override;
	virtual unsigned int height() const override;
	virtual unsigned int textWidth(const std::wstring& string) const override;
	virtual unsigned int textHeight() const override;

	virtual void line(int x1, int y1, int x2, int y2, unsigned int color) override;
	virtual void rectangle(int x, int y, unsigned int width, unsigned int height, unsigned int color, bool filled) override;
	virtual void text(int x, int y, const std::wstring& string, unsigned int color, bool vertical) override;

	virtual void clip(int x, int y, unsigned int width, unsigned int height) override;
	virtual void reset_clip() override;

private:
	Gdiplus::Graphics gdip;
	Gdiplus::Font font;

	unsigned int width_;
	unsigned int height_;
	Gdiplus::Rect clip_plane;

	Gdiplus::Color rgb(unsigned int color);
};

class GdipInit
{
public:
	GdipInit()
	{
		Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, nullptr);
	}

	~GdipInit()
	{
		Gdiplus::GdiplusShutdown(gdiplusToken);
	}

private:
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
};

#endif // GDIP_DRAWER_H