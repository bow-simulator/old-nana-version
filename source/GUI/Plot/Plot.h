// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef PLOT_H
#define PLOT_H

#include <nana/gui.hpp>
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/panel.hpp>
#include <nana/gui/widgets/menu.hpp>
#include <nana/system/dataexch.hpp>

#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>
#include <string>
#include <sstream>

#include "PlotBase.h"
#include "GdipDrawer.h"

//Bugs, ideas for improvements:
//-----------------------------
//- Different font sizes for number labels and caption
//- Draw markers OVER the lines
//- Possibility for a "pretty fit", where start and end of the intervals are 1-2-5 values
//- Minor ticks for Axis
//- Mark origin with black lines, easier when there is proper clipping
//- Legend
//- Cross hair
//- Rename graphics to drawer where it occurs
//- Erase trailing zeros/ same number of post decimal places for all numbers of one axis
//- Option for number labels on start and end of axis
//- Strange effects when zooming too close
//- Number format width is very conservative, switch to scientific doesn't work
//- Option for equal axis scaling
//- Fitting clips off too much
//- Update automatically when fit is called

class Plot: public nana::panel<true>, public PlotBase
{
public:
	//Plot(nana::window wd, const nana::rectangle& r = nana::rectangle(), bool visible = true);
	Plot(nana::window wd);
	void update();
	//void saveAsFile(const char* path, nana::size s);
	//void saveAsFile(const char* path);

	static void plot(const std::vector<double>& x, const std::vector<double>& y, std::wstring title, std::wstring x_label, std::wstring y_label);

private:
	nana::menu mobj;
	nana::drawing dw;

	nana::point down_left;
	nana::point down_middle;

	void menu_copy(nana::menu::item_proxy& ip);
};

#endif // PLOT_H