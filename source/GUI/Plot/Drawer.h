// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef DRAWER_H
#define DRAWER_H

#include <string>

class Drawer
{
public:
	//Measurement
	virtual unsigned int width() const = 0;
	virtual unsigned int height() const = 0;
	virtual unsigned int textWidth(const std::wstring& string) const = 0;
	virtual unsigned int textHeight() const = 0;

	//Drawing
	virtual void line(int x1, int y1, int x2, int y2, unsigned int color) = 0;
	virtual void rectangle(int x, int y, unsigned int width, unsigned int height, unsigned int color, bool filled) = 0;
	virtual void text(int x, int y, const std::wstring& string, unsigned int color, bool vertical) = 0;

	//Clipping
	virtual void clip(int x, int y, unsigned int width, unsigned int height) = 0;
	virtual void reset_clip() = 0;
};

#endif // DRAWER_H