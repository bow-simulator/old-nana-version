// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "PlotData.h"

std::array<unsigned int, 7> LineFormat::default_colors = {0x0000FF, 0x008000, 0xFF0000, 0x00BFBF, 0xBF00BF, 0xBFBF00, 0x404040};

PlotData::PlotData(const std::vector<double>& x_values, const std::vector<double>& y_values, LineFormat line_fmt, MarkerFormat marker_fmt)
		: x_val(x_values),
		  y_val(y_values),
		  visible_(true),
		  line_fmt(line_fmt),
		  marker_fmt(marker_fmt)
{

}

const std::vector<double>& PlotData::x() const
{
	return x_val;
}

const std::vector<double>& PlotData::y() const
{
	return y_val;
}

double PlotData::x_min() const
{
	return *std::min_element(x_val.begin(), x_val.end());
}

double PlotData::x_max() const
{
	return *std::max_element(x_val.begin(), x_val.end());
}

double PlotData::y_min() const
{
	return *std::min_element(y_val.begin(), y_val.end());
}

double PlotData::y_max() const
{
	return *std::max_element(y_val.begin(), y_val.end());
}

LineFormat& PlotData::lineFormat()
{
	return line_fmt;
}

MarkerFormat& PlotData::markerFormat()
{
	return marker_fmt;
}

void PlotData::visible(bool vb)
{
	visible_ = vb;
}

bool PlotData::visible() const
{
	return visible_;
}

void PlotData::draw(Drawer& graph, const nana::rectangle& r, const Axis& x_axis, const Axis& y_axis) const
{
	if(visible() && x_val.size() > 0)
	{
		nana::point beg;
		nana::point end;

		beg = nana::point(x_axis.coordToPixel(x_val[0], r.width) + r.x,
			             -y_axis.coordToPixel(y_val[0], r.height) + r.y + r.height - 1);

		marker_fmt.draw(graph, beg);

		for(std::size_t i = 1; i < x_val.size(); ++i)
		{
			end = nana::point(x_axis.coordToPixel(x_val[i], r.width) + r.x,
				             -y_axis.coordToPixel(y_val[i], r.height) + r.y + r.height - 1);

			marker_fmt.draw(graph, beg);
			line_fmt.draw(graph, beg, end);

			beg = end;
		}

		if(r.is_hit(end.x, end.y))
		{
			marker_fmt.draw(graph, end);
		}
	}
}