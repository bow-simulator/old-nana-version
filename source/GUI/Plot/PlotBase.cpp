// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "PlotBase.h"

PlotBase::PlotBase(): x_axis(0.0, 1.0, true), y_axis(0.0, 1.0, false), x_margin(5), y_margin(5), zoom_box_active(false)
{

}

void PlotBase::title(std::wstring tl)
{
	title_ = tl;
}

const std::wstring& PlotBase::title() const
{
	return title_;
}

void PlotBase::addPlotData(PlotData d)
{
	data.push_back(d);
}

std::vector<PlotData>& PlotBase::getData()
{
	return data;
}

Axis& PlotBase::getXAxis()
{
	return x_axis;
}

Axis& PlotBase::getYAxis()
{
	return y_axis;
}

void PlotBase::fit(bool fit_x_min, bool fit_y_min, bool fit_x_max, bool fit_y_max)
{
	double x_min = std::numeric_limits<double>::max();
	double x_max = std::numeric_limits<double>::lowest();
	double y_min = std::numeric_limits<double>::max();
	double y_max = std::numeric_limits<double>::lowest();

	for(std::size_t i = 0; i < data.size(); ++i)
	{
		for(std::size_t j = 0; j < data[i].x().size(); ++j)
		{
			x_min = std::min(x_min, data[i].x()[j]);
			x_max = std::max(x_max, data[i].x()[j]);
			y_min = std::min(y_min, data[i].y()[j]);
			y_max = std::max(y_max, data[i].y()[j]);
		}
	}

	if(x_min == x_max && fit_x_min && fit_x_max)
	{
		x_min -= 1.0;
		x_max += 1.0;
	}

	if(y_min == y_max && fit_y_min && fit_y_max)
	{
		y_min -= 1.0;
		y_max += 1.0;
	}

	if(fit_x_min)
	{
		x_axis.min(x_min);
	}

	if (fit_y_min)
	{
		y_axis.min(y_min);
	}

	if(fit_x_max)
	{
		x_axis.max(x_max);
	}

	if(fit_y_max)
	{
		y_axis.max(y_max);
	}

	/*
	if (y_axis.min() == y_axis.max())
	{
		y_axis.min(y_axis.min() - 1.0);
		y_axis.max(y_axis.max() + 1.0);
	}

	if (x_axis.min() == x_axis.max())
	{
		x_axis.min(y_axis.min() - 1.0);
		x_axis.max(y_axis.max() + 1.0);
	}
	*/
}

void PlotBase::fit125(bool fit_x_min , bool fit_y_min, bool fit_x_max, bool fit_y_max)
{
	fit(fit_x_min, fit_y_min, fit_x_max, fit_y_max);
	x_axis.round();
	y_axis.round();
}

void PlotBase::draw(Drawer& graph)
{
	widget_area = nana::rectangle(0, 0, graph.width(), graph.height());

	//Draw Background area
	graph.rectangle(0, 0, graph.width(), graph.height(), 0xFFFFFF, true);
	graph.rectangle(0, 0, graph.width(), graph.height(), 0x000000, false);
	
	//New plot dimensions
	int plot_left = 0;
	int plot_right = graph.width()-1;
	int plot_up = 0;
	int plot_down = graph.height()-1;

	//Draw title
	nana::size size_title(graph.textWidth(title_), graph.textHeight());
	graph.text((graph.width()-size_title.width)/2, size_title.height/2, title_, 0x000000, false);
	plot_up += 2*size_title.height;


	//Draw x-Axis label
	if(!x_axis.label().empty())
	{
		nana::size size_x_label(graph.textWidth(x_axis.label()), graph.textHeight());
		graph.text((graph.width()-size_x_label.width)/2, graph.height() - 3*size_x_label.height/2, x_axis.label(), 0x000000, false);

		plot_down -= 2*size_x_label.height;
	}

	//Draw y-Axis label
	if(!y_axis.label().empty())
	{
		nana::size size_y_label(graph.textWidth(y_axis.label()), graph.textHeight());
		graph.text(size_y_label.height/2, (graph.height()+size_y_label.width)/2, y_axis.label(), 0x000000, true);

		plot_left += 2*size_y_label.height;
		plot_right -= 2*size_y_label.height;
	}

	//Draw axes
	nana::size size_x_number = x_axis.numberFormat().maxSize(graph);
	nana::size size_y_number = y_axis.numberFormat().maxSize(graph);
	plot_left += size_y_number.width;
	plot_right -= size_y_number.width;
	plot_down -= 2*size_x_number.height;

	int plot_width = plot_right - plot_left + 1;
	int plot_height = plot_down - plot_up+1;

	if(plot_width > 0 && plot_height > 0)
	{
		//New plot area
		plot_area = nana::rectangle(plot_left, plot_up, plot_width, plot_height);

		//Optional scale to prevent distortion (doesn't work yet)
		//double delta_x = x_axis.max() - x_axis.min();
		//double delta_y = y_axis.max() - y_axis.min();
		//double disp_ratio = double(plot_area.width)/double(plot_area.height);

		//if(delta_x/delta_y > disp_ratio)
		//{
		//	x_axis.setView(x_axis.min(), x_axis.min() + delta_y*disp_ratio);
		//}
		//else
		//{
		//	y_axis.setView(y_axis.min(), y_axis.min() + delta_x/disp_ratio);
		//}
		
		//Draw background and axis
		graph.rectangle(plot_area.x, plot_area.y, plot_area.width, plot_area.height, 0x000000, false);
		x_axis.draw(graph, plot_area);
		y_axis.draw(graph, plot_area);

		//Data plots
		graph.clip(plot_area.x, plot_area.y, plot_area.width, plot_area.height);
		for (std::size_t i = 0; i < data.size(); ++i)
		{
			data[i].draw(graph, plot_area, x_axis, y_axis);
		}
		graph.reset_clip();

		//Zoombox
		if(zoom_box_active)
		{
			graph.rectangle(zoom_box.x, zoom_box.y, zoom_box.width, zoom_box.height, 0xC2C2C2, false);
		}
	}
}

void PlotBase::grid(bool gd)
{
	x_axis.grid(gd);
	y_axis.grid(gd);
}

void PlotBase::zoom(const nana::point& pos, double factor)
{
	if(plot_area.is_hit(pos))
	{
		point p = pixelToCoordinates(pos);
		x_axis.setView((x_axis.min() - p.x)*factor + p.x, (x_axis.max() - p.x)*factor + p.x);
		y_axis.setView((y_axis.min() - p.y)*factor + p.y, (y_axis.max() - p.y)*factor + p.y);
	}
}

void PlotBase::zoom(const nana::point& start, const nana::point& end)
{
	if(widget_area.is_hit(start.x, start.y) && widget_area.is_hit(end.x, end.y) && start.x != end.x && start.y != end.y)
	{
		point p0 = pixelToCoordinates(start);
		point p1 = pixelToCoordinates(end);
		x_axis.setView(p0.x, p1.x);
		y_axis.setView(p0.y, p1.y);
	}

	zoom_box_active = false;
}

void PlotBase::zoomBox(const nana::point& start, const nana::point& end)
{
	if(widget_area.is_hit(start.x, start.y) && widget_area.is_hit(end.x, end.y))
	{
		zoom_box.x = std::min(start.x, end.x);
		zoom_box.y = std::min(start.y, end.y);
		zoom_box.width = std::abs(end.x - start.x);
		zoom_box.height = std::abs(end.y - start.y);
	}
}

void PlotBase::zoomBox()
{
	zoom_box_active = true;
}

void PlotBase::pan(const nana::point& start, const nana::point& end)
{
	if(plot_area.is_hit(start.x, start.y) && plot_area.is_hit(end.x, end.y))
	{
		point p0 = pixelToCoordinates(start);
		point p1 = pixelToCoordinates(end);
		x_axis.shiftView(p0.x - p1.x);
		y_axis.shiftView(p0.y - p1.y);
	}
}

point PlotBase::pixelToCoordinates(const nana::point& pos)
{
	point p;
	p.x = x_axis.pixelToCoord(pos.x - plot_area.x, plot_area.width);
	p.y = y_axis.pixelToCoord(plot_area.y + plot_area.height - pos.y, plot_area.height);
	return p;
}