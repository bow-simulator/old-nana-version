// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "GdipDrawer.h"

GdipInit init;

GdipDrawer::GdipDrawer(nana::paint::graphics& graph)
	: gdip((HDC)graph.context()),
	  font(L"Arial", 9),
	  width_(graph.width()),
	  height_(graph.height())
{
	gdip.SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);	//Gdiplus::SmoothingModeHighQuality
}

unsigned int GdipDrawer::width() const
{
	//Gdiplus::Rect bounds;
	//gdip.GetClipBounds(&bounds);
	//return bounds.Width;
	return width_;
}

unsigned int GdipDrawer::height() const
{
	return height_;
}

unsigned int GdipDrawer::textWidth(const std::wstring& string) const
{
	Gdiplus::RectF layoutRect(0.f, 0.f, (float)width(), (float)height());
	//Gdiplus::StringFormat format;
	//format.SetAlignment(Gdiplus::StringAlignmentNear);

	Gdiplus::RectF size;
	gdip.MeasureString(string.c_str(), -1, &font, layoutRect, &size);

	return (unsigned int)size.Width;
}

unsigned int GdipDrawer::textHeight() const
{
	//Doesn't work with more than one plot for some reason
	//return (unsigned int)font.GetHeight(&gdip);

	return (unsigned int)font.GetHeight(gdip.GetDpiY());
}

void GdipDrawer::line(int x1, int y1, int x2, int y2, unsigned int color)
{
	Gdiplus::Pen pen(rgb(color));
	gdip.DrawLine(&pen, x1, y1, x2, y2);
}

void GdipDrawer::rectangle(int x, int y, unsigned int width, unsigned int height, unsigned int color, bool filled)
{
	width -= 1;
	height -= 1;

	if (filled)
	{
		Gdiplus::SolidBrush brush(rgb(color));
		gdip.FillRectangle(&brush, x, y, width, height);
	}
	else
	{
		Gdiplus::Pen pen(rgb(color));
		gdip.DrawRectangle(&pen, x, y, width, height);
	}
}

void GdipDrawer::text(int x, int y, const std::wstring& string, unsigned int color, bool vertical)
{
	Gdiplus::StringFormat format;
	format.SetAlignment(Gdiplus::StringAlignmentNear);
	Gdiplus::SolidBrush blackBrush(rgb(color));

	gdip.TranslateTransform(x, y);
	Gdiplus::RectF layoutRect(0, 0, (float)width(), (float)height());

	if(vertical)
	{
		gdip.RotateTransform(270);
	}

	gdip.DrawString(string.c_str(), -1, &font, layoutRect, &format, &blackBrush);
	gdip.ResetTransform();
}

void GdipDrawer::clip(int x, int y, unsigned int width, unsigned int height)
{
	clip_plane.X = x;
	clip_plane.Y = y;
	clip_plane.Width = width;
	clip_plane.Height = height;

	gdip.SetClip(clip_plane);
}

void GdipDrawer::reset_clip()
{
	gdip.ResetClip();
}

Gdiplus::Color GdipDrawer::rgb(unsigned int color)
{
	//http://stackoverflow.com/questions/2262100/rgb-int-to-rgb-python
	unsigned int blue = color & 255;
	unsigned int green = (color >> 8) & 255;
	unsigned int red = (color >> 16) & 255;

	return Gdiplus::Color(red, green, blue);
}

//Old Nana implementations

/*
unsigned int GdipDrawer::textWidth(const std::wstring& string) const
{
	return graph.text_extent_size(string).width;
}
*/

/*
unsigned int GdipDrawer::textHeight() const
{
	return graph.typeface().height();
}
*/

/*
void GdipDrawer::line(int x1, int y1, int x2, int y2, unsigned int color)
{
	graph.line(x1, y1, x2, y2, color);
}
*/

/*
void GdipDrawer::rectangle(int x, int y, unsigned int width, unsigned int height, unsigned int color, bool filled)
{
	graph.rectangle(x, y, width, height, color, filled);
}
*/

/*
void GdipDrawer::text(int x, int y, const std::wstring& string, unsigned int color)
{
	graph.string(x, y, color, string);
}
*/