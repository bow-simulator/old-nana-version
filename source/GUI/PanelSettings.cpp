// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "PanelSettings.h"

PanelSettings::PanelSettings(nana::window wd)
  : panel(wd),
  	box_settings(*this, L"Settings"),
	lb_elements(box_settings.pnl(), L"Limb elements"),
	lb_timestep_factor(box_settings.pnl(), L"Timestep factor"),
	tb_elements(box_settings.pnl()),
	tb_timestep_factor(box_settings.pnl()),
	cb_contact(box_settings.pnl(), L"Contact handling"),
	plc(*this)
{
	tb_elements.multi_lines(false);
	tb_timestep_factor.multi_lines(false);
	
	box_settings.plc().div("<grid=[2,3] settings gap=2 collapse(0,1,2,1)>");
	box_settings.plc().field("settings") << lb_elements << tb_elements
										 << cb_contact
										 << lb_timestep_factor << tb_timestep_factor;
	box_settings.plc().collocate();
	

	plc.div("<vertical <horizontal <box_settings weight=400><> weight=156><>>");
	plc.field("box_settings") << box_settings;
	plc.collocate();
}

void PanelSettings::loadFromJson(const jsoncons::wjson& obj)
{
	tb_elements.caption(obj[L"settings"][L"elements"].as<std::wstring>());
	cb_contact.check(obj[L"settings"][L"contact_handling"].as<bool>());
	tb_timestep_factor.caption(obj[L"settings"][L"timestep_factor"].as<std::wstring>());

	edited_reset();
}

void PanelSettings::saveToJson(jsoncons::wjson& obj)
{
	jsoncons::wjson settings;
	settings[L"elements"] = tb_elements.to_int();
	settings[L"contact_handling"] = cb_contact.checked();
	settings[L"timestep_factor"] = tb_timestep_factor.to_double();

	obj[L"settings"] = settings;

	edited_reset();
}

bool PanelSettings::edited()
{
	return tb_elements.edited() || tb_timestep_factor.edited();	//cb_contact is missing
}

void PanelSettings::edited_reset()
{
	tb_elements.edited_reset();
	tb_timestep_factor.edited_reset();
}