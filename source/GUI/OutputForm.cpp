// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "OutputForm.h"

StaticValues::StaticValues(nana::window wd, const StaticData& static_data)
	: panel(wd),
	  plc(*this),
	  box(*this, L"Specific values"),
	  lb_draw_force(box.pnl(), L"Final draw force " + units::force.label()),
      lb_string_length(box.pnl(), L"String length " + units::length.label()),
      lb_energy_braced(box.pnl(), L"Energy braced " + units::energy.label()),
      lb_energy_drawn(box.pnl(), L"Energy drawn " + units::energy.label()),
      lb_drawing_work(box.pnl(), L"Drawing work " + units::energy.label()),
      lb_storage_ratio(box.pnl(), L"Storage ratio " + units::percent.label()),
	  tb_val_draw_force(box.pnl()),
	  tb_val_string_length(box.pnl()),
      tb_val_energy_braced(box.pnl()),
      tb_val_energy_drawn(box.pnl()),
      tb_val_drawing_work(box.pnl()),
      tb_val_storage_ratio(box.pnl())
{
	tb_val_draw_force.editable(false);
	tb_val_string_length.editable(false);
	tb_val_energy_braced.editable(false);
    tb_val_energy_drawn.editable(false);
    tb_val_drawing_work.editable(false);
    tb_val_storage_ratio.editable(false);

	tb_val_draw_force.from(units::force.from_base(static_data.finalDrawForce()));
	tb_val_string_length.from(units::length.from_base(static_data.stringLength()));
	tb_val_energy_braced.from(units::energy.from_base(static_data.energyBraced()));
    tb_val_energy_drawn.from(units::energy.from_base(static_data.energyDrawn()));
    tb_val_drawing_work.from(units::energy.from_base(static_data.drawingWork()));
    tb_val_storage_ratio.from(units::percent.from_base(static_data.storageRatio()));
	
	box.plc().div("<grid=[2,6] labels gap=2>");
	box.plc().field("labels") << lb_draw_force << tb_val_draw_force
							  << lb_string_length << tb_val_string_length
							  << lb_energy_braced << tb_val_energy_braced
							  << lb_energy_drawn << tb_val_energy_drawn
							  << lb_drawing_work << tb_val_drawing_work
							  << lb_storage_ratio << tb_val_storage_ratio;
	box.plc().collocate();

	plc.div("<box>");
	plc.field("box") << box;
	plc.collocate();
}

TabpageStatic::TabpageStatic(nana::window wd, const StaticData& static_data, const LimbGeometry& limb)
	: panel(wd),
	  plot_draw_states(*this),
	  plot_draw_curve(*this),
	  //plot_draw_curve_deriv(*this),
	  plot_draw_work(*this),
	  plot_string_force(*this),
	  plot_stress(*this),
	  //plot_strain(*this),
	  //plot_curvature(*this),
	  specific_values(*this, static_data),
	  tabs(*this),
	  plc(*this)
{
	//Create draw states plot
	plot_draw_states.title(L"Equilibrium states during draw");
	plot_draw_states.getXAxis().label(L"x-position " + units::length.label());
	plot_draw_states.getYAxis().label(L"y-position " + units::length.label());
	plot_draw_states.getXAxis().scale(units::length.from_base());
	plot_draw_states.getYAxis().scale(units::length.from_base());
	plot_draw_states.grid(true);
	plot_draw_states.addPlotData(PlotData(limb.x(),
		                                  limb.y(),
										  LineFormat(LineStyle::SOLID, 0x00008B),
										  MarkerFormat(MarkerStyle::PLUS, 0x00008B)));

	int m = 2;	//Intermediate steps
	for(int i = 0; i <= m+1; ++i)
	{
		int j = i*(static_data.limbShapeX().size()-1)/(m+1);
		plot_draw_states.addPlotData(PlotData(static_data.limbShapeX()[j],
											  static_data.limbShapeY()[j],
											  LineFormat(LineStyle::SOLID, 0x00008B),
											  MarkerFormat(MarkerStyle::PLUS, 0x00008B)));

		plot_draw_states.addPlotData(PlotData(static_data.stringShapeX()[j],
											  static_data.stringShapeY()[j],
											  LineFormat(LineStyle::SOLID, 0x2E9AFE),
											  MarkerFormat(MarkerStyle::PLUS, 0x2E9AFE)));
	}

	plot_draw_states.fit125();


	//Create draw curve plot
	plot_draw_curve.title(L"Draw curve");
	plot_draw_curve.getXAxis().label(L"Draw length " + units::length.label());
	plot_draw_curve.getYAxis().label(L"Draw force " + units::force.label());
	plot_draw_curve.getXAxis().scale(units::length.from_base());
	plot_draw_curve.getYAxis().scale(units::force.from_base());
	plot_draw_curve.grid(true);
	plot_draw_curve.addPlotData(PlotData(static_data.drawLength(),
										 static_data.drawForce(),
										 LineFormat(LineStyle::SOLID, 0x00008B)));
	plot_draw_curve.fit125(false, false);


	//Create draw work plot
	plot_draw_work.title(L"Total potential energy w.r.t. the unbraced state");
	plot_draw_work.getXAxis().label(L"Draw length " + units::length.label());
	plot_draw_work.getYAxis().label(L"Potential energy " + units::energy.label());
	plot_draw_work.getXAxis().scale(units::length.from_base());
	plot_draw_work.getYAxis().scale(units::energy.from_base());
	plot_draw_work.grid(true);
	plot_draw_work.addPlotData(PlotData(static_data.drawLength(),
										static_data.potentialEnergy(),
										LineFormat(LineStyle::SOLID, 0x00008B)));
	plot_draw_work.fit125(false);


	//Create string force plot
	plot_string_force.title(L"Axial string force");
	plot_string_force.getXAxis().label(L"Draw length " + units::length.label());
	plot_string_force.getYAxis().label(L"String force " + units::force.label());
	plot_string_force.getXAxis().scale(units::length.from_base());
	plot_string_force.getYAxis().scale(units::force.from_base());
	plot_string_force.grid(true);
	plot_string_force.addPlotData(PlotData(static_data.drawLength(),
										   static_data.stringForce(),
										   LineFormat(LineStyle::SOLID, 0x00008B)));
	plot_string_force.fit125(false);


	//Create stress plot
	plot_stress.title(L"Stresses on back (blue) and belly (red) at full draw");
	plot_stress.getXAxis().label(L"Centerline arc length " + units::length.label());
	plot_stress.getYAxis().label(L"Stress " + units::stress.label());
	plot_stress.getXAxis().scale(units::length.from_base());
	plot_stress.getYAxis().scale(units::stress.from_base());
	plot_stress.grid(true);
	plot_stress.addPlotData(PlotData(limb.arcLength(),
									 static_data.stressBack().back(),
								     LineFormat(LineStyle::SOLID, 0x00008B)));

	plot_stress.addPlotData(PlotData(limb.arcLength(),
								     static_data.stressBelly().back(),
								     LineFormat(LineStyle::SOLID, 0xFF0000)));
	plot_stress.fit125();


	//Create limb strain plot
	/*
	plot_strain.caption(L"centerline strain " + units::strain.label() + L" over arc length " + units::length.label());
	plot_strain.grid(true);

	for(int i = 0; i <= 0; ++i)
	{
		int j = i*(data.limbShapeU().size()-1)/(m+1);
		plot_strain.addPlotData(PlotData(units::length.from_base(limb.s()),
		    							 units::strain.from_base(data.limbStrain()[j]),
			    						 LineFormat(LineStyle::SOLID, 0x00008B)));

	}
	plot_strain.fit(false);
	*/

	//Create limb curvature plot
	/*
	plot_curvature.caption(L"centerline curvature change " + units::curvature.label() + L" over arc length " + units::length.label());
	plot_curvature.grid(true);

	for(int i = 0; i <= 0; ++i)
	{
		int j = i*(data.limbShapeU().size()-1)/(m+1);
	    plot_curvature.addPlotData(PlotData(units::length.from_base(limb.s()),
		    								units::curvature.from_base(data.limbCurvature()[j]),
			    							LineFormat(LineStyle::SOLID, 0x00008B)));
	}

	plot_curvature.fit(false);
	*/

	//Tabs and layout
	tabs.push_back(L"Equilibrium states");
	tabs.push_back(L"Draw curve");
	tabs.push_back(L"Potential energy");
	tabs.push_back(L"Axial string force");
	tabs.push_back(L"Limb stress");

	tabs.relate(0, plot_draw_states);
	tabs.relate(1, plot_draw_curve);
	tabs.relate(2, plot_draw_work);
	tabs.relate(3, plot_string_force);
	tabs.relate(4, plot_stress);
	tabs.activate(0);

	plc.div("<vertical <tabs weight=25><<plots margin=10><vertical weight=360<specific_values weight=230><>>>>");
	plc.field("tabs") << tabs;
	plc.field("plots").fasten(plot_draw_states);
	plc.field("plots").fasten(plot_draw_curve);
	plc.field("plots").fasten(plot_draw_work);
	plc.field("plots").fasten(plot_string_force);
	plc.field("plots").fasten(plot_stress);
	plc.field("plots").fasten(specific_values);
	plc.field("specific_values") << specific_values;
	plc.collocate();
}

DynamicValues::DynamicValues(nana::window wd, const StaticData& static_data, const DynamicData& dynamic_data)
	: panel(wd),
	  plc(*this),
	  box(*this, L"Specific values"),
	  lb_final_velocity(box.pnl(), L"Arrow velocity " + units::velocity.label()),
	  lb_final_energy(box.pnl(), L"Arrow energy " + units::energy.label()),
	  lb_efficiency(box.pnl(), L"Efficiency " + units::percent.label()),
	  tb_val_final_velocity(box.pnl()),
	  tb_val_final_energy(box.pnl()),
	  tb_val_efficiency(box.pnl())
{
    tb_val_final_velocity.editable(false);
	tb_val_final_energy.editable(false);
	tb_val_efficiency.editable(false);

	tb_val_final_velocity.from(units::velocity.from_base(dynamic_data.finalArrowVelocity()));
	tb_val_final_energy.from(units::energy.from_base(dynamic_data.finalArrowEnergy()));
	tb_val_efficiency.from(units::percent.from_base(dynamic_data.efficiency(static_data)));

	box.plc().div("<grid=[2,3] labels gap=2>");
	box.plc().field("labels") << lb_final_velocity << tb_val_final_velocity
							  << lb_final_energy << tb_val_final_energy
							  << lb_efficiency << tb_val_efficiency;
	box.plc().collocate();

	plc.div("<box>");
	plc.field("box") << box;
	plc.collocate();
}

TabpageDynamic::TabpageDynamic(nana::window wd, const StaticData& static_data, const DynamicData& dynamic_data, const LimbGeometry& limb)
	: panel(wd),
	  //plot_states(*this),
	  plot_position(*this),
	  plot_velocity(*this),
	  plot_acceleration(*this),
	  plot_string_force(*this),
	  plot_support_force(*this),
	  plot_energy(*this),
	  specific_values(*this, static_data, dynamic_data),
	  tabs(*this),
	  plc(*this)
{
	/*
	//Create draw states plot
	plot_states.caption(L"y-position " + units::length.label() + L" over x-position " + units::length.label());
	plot_states.grid(true);
	plot_states.addPlotData(PlotData(units::length.from_base(limb.u()),
		                                  units::length.from_base(limb.v()),
										  LineFormat(LineStyle::SOLID, 0x00008B),
										  MarkerFormat(MarkerStyle::PLUS, 0x00008B)));
	
	int m = 2;	//Intermediate steps
	for(int i = 0; i <= m+1; ++i)
	{
		int j = i*(data.limbShapeU().size()-1)/(m+1);
		plot_states.addPlotData(PlotData(units::length.from_base(data.limbShapeU()[j]),
											  units::length.from_base(data.limbShapeV()[j]),
											  LineFormat(LineStyle::SOLID, 0x00008B),
											  MarkerFormat(MarkerStyle::PLUS, 0x00008B)));

		plot_states.addPlotData(PlotData(units::length.from_base(data.stringShapeU()[j]),
											  units::length.from_base(data.stringShapeV()[j]),
											  LineFormat(LineStyle::SOLID, 0x2E9AFE),
											  MarkerFormat(MarkerStyle::PLUS, 0x2E9AFE)));
	}
	
	plot_states.fit();
	*/
	
	//Create position plot
	plot_position.title(L"Position of string center (blue) and arrow (red)");
	plot_position.getXAxis().label(L"Time " + units::time.label());
	plot_position.getYAxis().label(L"Position " + units::length.label());
	plot_position.getXAxis().scale(units::time.from_base());
	plot_position.getYAxis().scale(units::length.from_base());
	plot_position.grid(true);
	plot_position.addPlotData(PlotData(dynamic_data.time(),
									   dynamic_data.stringPosition(),
									   LineFormat(LineStyle::SOLID, 0x00008B)));

	plot_position.addPlotData(PlotData(dynamic_data.time(),
									   dynamic_data.arrowPosition(),
									   LineFormat(LineStyle::SOLID, 0xFF0000)));
	plot_position.fit125();


	//Create velocity plot
	plot_velocity.title(L"Velocity of string center (blue) and arrow (red)");
	plot_velocity.getXAxis().label(L"Time " + units::time.label());
	plot_velocity.getYAxis().label(L"Velocity " + units::velocity.label());
	plot_velocity.getXAxis().scale(units::time.from_base());
	plot_velocity.getYAxis().scale(units::velocity.from_base());
	plot_velocity.grid(true);
	plot_velocity.addPlotData(PlotData(dynamic_data.time(),
									   dynamic_data.stringVelocity(),
									   LineFormat(LineStyle::SOLID, 0x00008B)));

	plot_velocity.addPlotData(PlotData(dynamic_data.time(),
									   dynamic_data.arrowVelocity(),
									   LineFormat(LineStyle::SOLID, 0xFF0000)));

	plot_velocity.getXAxis().scale(units::time.from_base());
	plot_velocity.getYAxis().scale(units::velocity.from_base());
	plot_velocity.fit125();

	
	//Create acceleration plot
	plot_acceleration.title(L"Acceleration of string center (blue) and arrow (red)");
	plot_acceleration.getXAxis().label(L"Time " + units::time.label());
	plot_acceleration.getYAxis().label(L"Acceleration " + units::acceleration.label());
	plot_acceleration.getXAxis().scale(units::time.from_base());
	plot_acceleration.getYAxis().scale(units::acceleration.from_base());
	plot_acceleration.grid(true);
	plot_acceleration.addPlotData(PlotData(dynamic_data.time(),
										   dynamic_data.stringAcceleration(),
										   LineFormat(LineStyle::SOLID, 0x00008B)));

	plot_acceleration.addPlotData(PlotData(dynamic_data.time(),
										   dynamic_data.arrowAcceleration(),
										   LineFormat(LineStyle::SOLID, 0xFF0000)));
	plot_acceleration.fit125();


	//Create string force
	plot_string_force.title(L"Axial force at string center");
	plot_string_force.getXAxis().label(L"Time " + units::time.label());
	plot_string_force.getYAxis().label(L"String force " + units::force.label());
	plot_string_force.getXAxis().scale(units::time.from_base());
	plot_string_force.getYAxis().scale(units::force.from_base());
	plot_string_force.grid(true);
	plot_string_force.addPlotData(PlotData(dynamic_data.time(),
										   dynamic_data.stringForce(),
										   LineFormat(LineStyle::SOLID, 0x00008B)));
	plot_string_force.fit125();


	//Create grip force
	plot_support_force.title(L"Support reaction force");
	plot_support_force.getXAxis().label(L"Time " + units::time.label());
	plot_support_force.getYAxis().label(L"Support force " + units::force.label());
	plot_support_force.getXAxis().scale(units::time.from_base());
	plot_support_force.getYAxis().scale(units::force.from_base());
	plot_support_force.grid(true);
	plot_support_force.addPlotData(PlotData(dynamic_data.time(),
										   dynamic_data.gripForce(),
										   LineFormat(LineStyle::SOLID, 0x00008B)));
	plot_support_force.fit125();


	//Create energy plot
	plot_energy.title(L"Potential (blue), kinetic (red) and total energy (green)");
	plot_energy.getXAxis().label(L"Time " + units::time.label());
	plot_energy.getYAxis().label(L"Energy " + units::energy.label());
	plot_energy.getXAxis().scale(units::time.from_base());
	plot_energy.getYAxis().scale(units::energy.from_base());
	plot_energy.grid(true);
	plot_energy.addPlotData(PlotData(dynamic_data.time(),
									 dynamic_data.potentialEnergy(),
									 LineFormat(LineStyle::SOLID, 0x00008B)));
	
	plot_energy.addPlotData(PlotData(dynamic_data.time(),
									 dynamic_data.kineticEnergy(),
									 LineFormat(LineStyle::SOLID, 0xFF0000)));

	plot_energy.addPlotData(PlotData(dynamic_data.time(),
								     dynamic_data.totalEnergy(),
									 LineFormat(LineStyle::SOLID, 0x0B610B)));
	plot_energy.fit125();
	plot_energy.getYAxis().max(1.2*plot_energy.getYAxis().max());


	//Tabs and layout
	//tabs.push_back(L"States");
	tabs.push_back(L"Position");
	tabs.push_back(L"Velocity");
	tabs.push_back(L"Acceleration");
	tabs.push_back(L"Axial string force");
	tabs.push_back(L"Support force");
	tabs.push_back(L"Energy");

	//tabs.relate(0, plot_states);
	tabs.relate(0, plot_position);
	tabs.relate(1, plot_velocity);
	tabs.relate(2, plot_acceleration);
	tabs.relate(3, plot_string_force);
	tabs.relate(4, plot_support_force);
	tabs.relate(5, plot_energy);
	tabs.activate(0);

	plc.div("<vertical <tabs weight=25><<plots margin=10><vertical weight=360<specific_values weight=156><>>>>");
	plc.field("tabs") << tabs;
	//plc.field("plots").fasten(plot_states);
	plc.field("plots").fasten(plot_position);
	plc.field("plots").fasten(plot_velocity);
	plc.field("plots").fasten(plot_acceleration);
	plc.field("plots").fasten(plot_string_force);
	plc.field("plots").fasten(plot_support_force);
	plc.field("plots").fasten(plot_energy);
	plc.field("specific_values") << specific_values;
	plc.collocate();
}

OutputForm::OutputForm(nana::window wd, const nana::string& filename, const StaticData& static_data, const DynamicData& dynamic_data,  const LimbGeometry& limb)
	: nana::form(wd, nana::rectangle(0, 0, 1000, 600)),
	  tabs(*this),
	  plc(*this)
{
	if (!static_data.drawLength().empty())
	{
		caption(L"Results for " + filename + L" - Bow Simulation Tool");
		
		plc.div("<vertical <tabs weight=25><panels>>");
		plc.field("tabs") << tabs;
		
		tabpage_static = std::unique_ptr<TabpageStatic>(new TabpageStatic(*this, static_data, limb));
		tabs.push_back(L"Static results");
		tabs.relate(0, *tabpage_static);
		tabs.activate(0);
		plc.field("panels").fasten(*tabpage_static);
		
		if (!dynamic_data.time().empty())
		{
			tabpage_dynamic = std::unique_ptr<TabpageDynamic>(new TabpageDynamic(*this, static_data, dynamic_data, limb));
			tabs.push_back(L"Dynamic results");
			tabs.relate(1, *tabpage_dynamic);
			tabs.activate(1);
			plc.field("panels").fasten(*tabpage_dynamic);
		}

		plc.collocate();
	}
	else
	{
		close();
	}
}