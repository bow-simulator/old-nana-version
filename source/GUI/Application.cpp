// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "Application.h"

const std::string Application::version = "2015.2";

Application::Application(const nana::string& path)
	: nana::form(nana::API::make_center(1000, 660)),
	  menubar(*this),
	  tabbar(*this),
	  pnl_parameters(*this),
	  pnl_settings(*this),
	  pnl_comments(*this),
	  plc(*this)
{
	nana::API::window_icon(*this, nana::paint::image(L"BowSimulationTool.exe"));	//nana::paint::image(L"icon.ico")
	nana::API::window_icon_default(nana::paint::image(L"BowSimulationTool.exe"));

	//Window closing event
	events().unload([&](const nana::arg_unload& arg)
	{
		if(!askToSave())
		{
			arg.cancel = true;
		}
	});

	/*
	//Key events for shortcuts
	nana::API::register_shortkey(*this, L'n');
	nana::API::register_shortkey(*this, L'o');
	nana::API::register_shortkey(*this, L's');
	events().shortkey([&](const nana::arg_keyboard& arg)
	{
		switch(arg.key)
		{
		case L'n':
			menu_file_new();
			break;
		case L'o':
			menu_file_open();
			break;
		case L's':
			menu_file_save();
			break;
		}
	
	});
	*/

	//Menus
	menubar.bgcolor(nana::color(246, 246, 246));
	nana::menu& menu_file = menubar.push_back(L"&File");
	menu_file.append(L"&New", [&](nana::menu::item_proxy&){ fileNew(); });
	menu_file.append(L"&Open", [&](nana::menu::item_proxy&){ fileOpen(); });
	menu_file.append(L"&Save", [&](nana::menu::item_proxy&){ fileSave(); });
	menu_file.append(L"Save &as", [&](nana::menu::item_proxy&){ fileSaveAs(); });
	menu_file.append_splitter();
	menu_file.append(L"&Exit", [&](nana::menu::item_proxy&){ fileExit(); });

	nana::menu& menu_view = menubar.push_back(L"&View");
	menu_view.append(L"&Limb preview", [&](nana::menu::item_proxy&){ viewPreview(); });
	
	nana::menu& menu_sim = menubar.push_back(L"&Simulation");
	menu_sim.append(L"&Static analysis", [&](nana::menu::item_proxy&){ simulationRun(false); });
	menu_sim.append(L"&Dynamic analysis", [&](nana::menu::item_proxy&){ simulationRun(true); });

	nana::menu& menu_help = menubar.push_back(L"&Help");
	menu_help.append(L"&About", [&](nana::menu::item_proxy&){ helpAbout(); });
	
	//Tabs
	tabbar.push_back(L"Bow parameters");
	tabbar.push_back(L"Numerical settings");
	tabbar.push_back(L"Comments");
	tabbar.relate(0, pnl_parameters);
	tabbar.relate(1, pnl_settings);
	tabbar.relate(2, pnl_comments);
	tabbar.activate(0);

	plc.div("<vertical <menubar weight=25><tabbar weight=25><vertical tabs>>");
	plc.field("menubar") << menubar;
	plc.field("tabbar") << tabbar;
	plc.field("tabs").fasten(pnl_parameters);
	plc.field("tabs").fasten(pnl_settings);
	plc.field("tabs").fasten(pnl_comments);
	plc.collocate();

	show();

	if(path.empty() || !load(path))
	{
		load();
	}
}

bool Application::load()
{
	try
	{
		jsoncons::wjson obj = jsoncons::wjson::parse_string(parameters::default_json_string);
		obj[L"version"] = std::wstring(version.begin(), version.end());
		loadFromJson(obj);
		
		filepath = L"";
		filename = L"Untitled";
		caption(filename + L" - Bow Simulation Tool");
		edited_reset();

		return true;
	}
	catch(std::exception& e)
	{
		error(e);
		return false;
	}
}

bool Application::load(const nana::string& path)
{
	//Check for invalid path
	if(nana::filesystem::path(path).what() == nana::filesystem::path::type::not_exist)
	{
		nana::msgbox mb(*this, L"Bow Simulation Tool", nana::msgbox::ok);
		mb.icon(nana::msgbox::icon_warning);
		mb << L"The file " + path + L" could not be found.";
		mb();

		return false;
	}

	try
	{
		std::wifstream file(path);
		loadFromJson(jsoncons::wjson::parse(file));
		
		//Loading worked, update file path, name and window caption
		filepath = path;
		filename = nana::filesystem::path(filepath).name();
		caption(filename + L" - Bow Simulation Tool");
		edited_reset();

		return true;
	}
	catch(std::exception& e)
	{
		error(e);
		return false;
	}
}

bool Application::save(const nana::string& path)
{
	try
	{
		jsoncons::wjson obj;
		saveToJson(obj);

		jsoncons::woutput_format fmt;
		fmt.precision(10);

		std::wofstream file;
		file.open(path);
		file << jsoncons::pretty_print(obj, fmt);
		file.close();

		//Saving worked, update file path, name and window caption
		filepath = path;
		filename = nana::filesystem::path(filepath).name();
		caption(filename + L" - Bow Simulation Tool");
		edited_reset();

		return true;
	}
	catch(std::exception& e)
	{
		error(e);
		return false;
	}
}

void Application::loadFromJson(const jsoncons::wjson& obj)
{
	if(obj[L"version"].as<std::wstring>() != std::wstring(version.begin(), version.end()))
	{
		throw std::runtime_error("Application::load() : File not compatible with this version of Bow Simulation Tool");
	}
	
	pnl_parameters.loadFromJson(obj);
	pnl_settings.loadFromJson(obj);
	pnl_comments.loadFromJson(obj);
}

void Application::saveToJson(jsoncons::wjson& obj)
{
	obj[L"version"] = std::wstring(version.begin(), version.end());
	pnl_parameters.saveToJson(obj);
	pnl_settings.saveToJson(obj);
	pnl_comments.saveToJson(obj);
}

bool Application::askToSave()	// true: continue, false: abort
{
	if(edited())
	{
		nana::msgbox mb(*this, L"Bow Simulation Tool", nana::msgbox::yes_no_cancel);
		mb.icon(nana::msgbox::icon_question);
		mb << L"Save changes to " + filename + L"?";

		switch(mb())
		{
		case nana::msgbox::pick_yes:
			fileSave();
			return !edited();

		case nana::msgbox::pick_no:
			return true;

		case nana::msgbox::pick_cancel:
			return false;
		}
	}

	return true;
}

bool Application::edited()
{
	return pnl_parameters.edited() || pnl_settings.edited() || pnl_comments.edited();
}

void Application::edited_reset()
{
	pnl_parameters.edited_reset();
	pnl_settings.edited_reset();
	pnl_comments.edited_reset();
}

void Application::error(std::exception& e)
{
	nana::msgbox mb(*this, L"Error");
	mb.icon(nana::msgbox::icon_error);
	mb << e.what();
	mb();
}

void Application::fileNew()
{
	if(askToSave())
	{
		load();
	}
}

void Application::fileOpen()
{
	if(askToSave())
	{
		nana::filebox fb(*this, true);
		fb.add_filter(L"Bow files (*.bow)", L"*.bow");

		if(fb())
		{
			load(fb.file());
		}
	}
}

void Application::fileSave()
{
	if(filepath.empty())
	{
		fileSaveAs();
	}
	else
	{
		save(filepath);
	}
}

void Application::fileSaveAs()
{
	nana::filebox fb(*this, false);
	fb.add_filter(L"Bow files", L"*.bow");

	if(filepath.empty())
	{
		fb.init_file(L"Untitled.bow");
	}
	else
	{
		fb.init_file(filepath);
	}

	if(fb())
	{
		save(fb.file());
	}
}

void Application::fileExit()
{
	if(askToSave())
	{
		nana::API::exit();
	}
}

void Application::simulationRun(bool dynamic)
{
	try
	{
		jsoncons::wjson obj;
		saveToJson(obj);

		BowParameters parameters(obj);
		BowSettings settings(obj);

		LimbGeometry geometry(parameters, settings.elements);
		StaticData static_data;
		DynamicData dynamic_data;

		ProgressForm prg(*this, dynamic);
		nana::threads::pool threadpool;
		nana::threads::pool_push(threadpool, [&]()
		{
			try
			{
				Bow bow(geometry, parameters, settings);

				bow.simulateStatics(static_data, std::bind(&ProgressForm::progress1, &prg, std::placeholders::_1));

				if(dynamic)
				{
					bow.simulateDynamics(dynamic_data, std::bind(&ProgressForm::progress2, &prg, std::placeholders::_1));
				}
			}
			catch(std::exception& e)
			{
				error(e);
			}

			prg.close();
		})();

		nana::API::modal_window(prg);

		OutputForm output(*this, filename, static_data, dynamic_data, geometry);
		nana::API::modal_window(output);
	}
	catch(std::exception& e)
	{
		error(e);
	}

}

void Application::viewPreview()
{
	try
	{
		jsoncons::wjson obj;
		saveToJson(obj);
		BowParameters parameters(obj);
		BowSettings settings(obj);

		PreviewForm preview(*this, filename, parameters, settings);
		nana::API::modal_window(preview);
	}
	catch(std::exception& e)
	{
		error(e);
	}
}

void Application::helpAbout()
{
	nana::msgbox mb(*this, L"About");
	mb.icon(nana::msgbox::icon_information);
	mb << L"Bow Simulation Tool version " << version << L"\n";
	mb << L"https://sourceforge.net/projects/bowsimulationtool/\n\n";
	mb << L"Copyright (C) 2014 Stefan Pfeifer\n";
	mb << L"Distributed under the GNU General Public License v3.0";
	mb();
}

// Either path_out_s or path_out_d have to be non-empty
int Application::cmdSimulationRun(const std::wstring& path_in, const std::wstring& path_out_s, const std::wstring& path_out_d)
{
	if(nana::filesystem::path(path_in).what() == nana::filesystem::path::type::file)
	{
		try
		{
			std::wifstream file(path_in);
			jsoncons::wjson obj = jsoncons::wjson::parse(file);

			BowParameters parameters(obj);
			BowSettings settings(obj);

			LimbGeometry geometry(parameters, settings.elements);
			Bow bow(geometry, parameters, settings);

			StaticData static_data;
			std::cout << "Simulating statics...\n";
			CmdProgress prg_statics(5);
			bow.simulateStatics(static_data, [&](unsigned int progress)
			{
				prg_statics.set(progress);
				return true;
			});
			std::cout << "\n";

			if(!path_out_s.empty())
			{
				static_data.save(path_out_s);
			}

			if(!path_out_d.empty())
			{
				DynamicData dynamic_data;
				std::cout << "\nSimulating dynamics...\n";
				CmdProgress prg_dynamics(5);
				bow.simulateDynamics(dynamic_data, [&](unsigned int progress)
				{
					prg_dynamics.set(progress);
					return true;
				});
				std::cout << "\n";
				dynamic_data.save(path_out_d);
			}
		}
		catch(std::exception& e)
		{
			std::cerr << "Error: " << e.what();
			return 1;
		}
	}
	else
	{
		std::wcerr << L"The input file " + path_in + L" could not be found.";
		return 1;
	}

	return 0;
}