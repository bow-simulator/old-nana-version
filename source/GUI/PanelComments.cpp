// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "PanelComments.h"

PanelComments::PanelComments(nana::window wd)
  : panel(wd),
	tb_comments(*this),
	plc(*this)
{
	tb_comments.line_wrapped(true);

	plc.div("<comments margin=25>");
	plc.field("comments") << tb_comments;
	plc.collocate();
}

void PanelComments::loadFromJson(const jsoncons::wjson& obj)
{
	tb_comments.caption(obj[L"comments"].as<std::wstring>());
	edited_reset();
}

void PanelComments::saveToJson(jsoncons::wjson& obj)
{
	obj[L"comments"] = tb_comments.caption();
	edited_reset();
}

bool PanelComments::edited()
{
	return tb_comments.edited();	//cb_contact is missing
}

void PanelComments::edited_reset()
{
	tb_comments.edited_reset();
}