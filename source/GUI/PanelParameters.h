// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef PANEL_PARAMETERS_H
#define PANEL_PARAMETERS_H

#include "Table.h"
#include "GroupBox.h"
#include "../Bow/Bow.h"
#include "../Bow/Units.h"

#include <nana/gui.hpp>
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/textbox.hpp>
#include <nana/gui/widgets/panel.hpp>

#include <jsoncons/json.hpp>

#include <stdexcept>

class PanelParameters: public nana::panel<false>
{
public:
	PanelParameters(nana::window wd);
	
	void loadFromJson(const jsoncons::wjson& obj);
	void saveToJson(jsoncons::wjson& obj);
	bool edited() const;
	void edited_reset();

private:
	nana::place plc;
	GroupBox box_centerline;
	GroupBox box_sections;
	GroupBox box_material;
	GroupBox box_string;
	GroupBox box_masses;

	Table tbl_centerline;
	Table tbl_sections;

	nana::label lb_material_E;
	nana::label lb_material_rho;
	nana::textbox tb_material_E;
	nana::textbox tb_material_rho;

	nana::label lb_string_rhoA;
	nana::label lb_string_EA;
	nana::label lb_string_diameter;
	nana::label lb_string_length;
	nana::label lb_string_draw;
	nana::textbox tb_string_rhoA;
	nana::textbox tb_string_EA;
	nana::textbox tb_string_diameter;
	nana::textbox tb_string_brace;
	nana::textbox tb_string_draw;

	nana::label lb_mass_arrow;
	nana::label lb_mass_tip;
	nana::label lb_mass_serving;
	nana::textbox tb_mass_arrow;
	nana::textbox tb_mass_tip;
	nana::textbox tb_mass_serving;
};

#endif // PANEL_PARAMETERS_H