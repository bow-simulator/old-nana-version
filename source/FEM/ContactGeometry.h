// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef CONTACT_GEOMETRY_H
#define CONTACT_GEOMETRY_H

#include "Element.h"
#include "Node.h"

#include <Eigen/Core>

class AABB
{
public:
	double x_min;
	double x_max;
	double y_min;
	double y_max;

	AABB(double x_min, double x_max, double y_min, double y_max) : x_min(x_min), x_max(x_max), y_min(y_min), y_max(y_max)
	{

	}

	bool intersects(const AABB& bbox)
	{
		return (x_max > bbox.x_min && x_min < bbox.x_max) || (y_max > bbox.y_min && y_min < bbox.y_max);
	}
};

class ContactPoint
{
public:
	ContactPoint(Node node, double margin, double stiffness)
		: m_node(node), m_margin(margin), m_stiffness(stiffness)
	{

	}

	const Node& node() const
	{
		return m_node;
	}

	double margin() const
	{
		return m_margin;
	}

	double stiffness() const
	{
		return m_stiffness;
	}

	AABB aabb() const
	{
		double x = m_node.x();
		double y = m_node.y();
		return AABB(x - m_margin, x + m_margin, y - m_margin, y + m_margin);
	}

private:
	Node m_node;
	double m_margin;
	double m_stiffness;
};

class ContactSurface
{
public:
	ContactSurface(Node node1, Node node2)
		: m_node1(node1), m_node2(node2)
	{

	}

	const Node& node1() const
	{
		return m_node1;
	}

	const Node& node2() const
	{
		return m_node2;
	}

	AABB aabb() const
	{
		double x1 = m_node1.x();
		double y1 = m_node1.y();
		double x2 = m_node2.x();
		double y2 = m_node2.y();

		double x_min = std::min(x1, x2);
		double x_max = std::max(x1, x2);
		double y_min = std::min(y1, y2);
		double y_max = std::max(y1, y2);

		return AABB(x_min, x_max, y_min, y_max);
	}

private:
	Node m_node1;
	Node m_node2;
};

class ContactPair
{
public:
	ContactPair(const ContactSurface& line, const ContactPoint& point)
		: m_line(line), m_point(point)
	{

	}

	const ContactSurface& line() const
	{
		return m_line;
	}

	const ContactPoint& point() const
	{
		return m_point;
	}

private:
	const ContactSurface& m_line;
	const ContactPoint& m_point;
};

#endif // CONTACT_GEOMETRY_H