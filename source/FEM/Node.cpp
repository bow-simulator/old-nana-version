// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "Node.h"
#include "Model.h"

Node::Node(Model& model, double x_val, double y_val, double phi_val, bool free_x, bool free_y, bool free_phi)
	: model(model),
	  index_x(-1),
	  index_y(-1),
	  index_phi(-1),
	  fixed_value_x(x_val),
	  fixed_value_y(y_val),
	  fixed_value_phi(phi_val)
{
	//Determine global dofs the node refers to and initialise corresponding data
	if(free_x)
	{
		index_x = model.createDof();
		model.coordinates()(index_x) = x_val;
		model.velocities()(index_x) = 0;
		model.externalForces()(index_x) = 0;
	}

	if(free_y)
	{
		index_y = model.createDof();
		model.coordinates()(index_y) = y_val;
		model.velocities()(index_y) = 0;
		model.externalForces()(index_y) = 0;
	}

	if(free_phi)
	{
		index_phi = model.createDof();
		model.coordinates()(index_phi) = phi_val;
		model.velocities()(index_phi) = 0;
		model.externalForces()(index_phi) = 0;
	}
}

Node::Node(Model& model, const Node& node, double phi_val)
	: model(model),
	  index_x(node.dof_x()),
	  index_y(node.dof_y()),
	  fixed_value_x(node.x()),
	  fixed_value_y(node.y()),
	  fixed_value_phi(phi_val)
{
	index_phi = model.createDof();
	model.coordinates()(index_phi) = phi_val;
	model.velocities()(index_phi) = 0;
	model.externalForces()(index_phi) = 0;
}

double Node::x() const
{
	if(index_x >= 0)
		return model.coordinates()(index_x);
	else
		return fixed_value_x;
}

double Node::y() const
{
	if(index_y >= 0)
		return model.coordinates()(index_y);
	else
		return fixed_value_y;
}

double Node::phi() const
{
	if(index_phi >= 0)
		return model.coordinates()(index_phi);
	else
		return fixed_value_phi;
}

double Node::dot_x() const
{
	if(index_x >= 0)
		return model.velocities()(index_x);
	else
		return 0.0;
}

double Node::dot_y() const
{
	if(index_y >= 0)
		return model.velocities()(index_y);
	else
		return 0.0;
}

double Node::dot_phi() const
{
	if(index_phi >= 0)
		return model.velocities()(index_phi);
	else
		return 0.0;
}

double Node::ddot_x() const
{
	if(index_x >= 0)
		return model.accelerations()(index_x);
	else
		return 0.0;
}

double Node::ddot_y() const
{
	if(index_y >= 0)
		return model.accelerations()(index_y);
	else
		return 0.0;
}

double Node::ddot_phi() const
{
	if(index_phi >= 0)
		return model.accelerations()(index_phi);
	else
		return 0.0;
}

double Node::force_x() const
{
	if(index_x >= 0)
		return model.externalForces()(index_x);
	else
		return 0.0;
}

double Node::force_y() const
{
	if(index_y >= 0)
		return model.externalForces()(index_y);
	else
		return 0.0;
}

double Node::torque() const
{
	if(index_phi >= 0)
		return model.externalForces()(index_phi);
	else
		return 0.0;
}

//No test on index because these methods should fail on fixed dofs
void Node::force_x(double value)
{
	model.externalForces()(index_x) = value;
}

void Node::force_y(double value)
{
	model.externalForces()(index_y) = value;
}

void Node::torque(double value)
{
	model.externalForces()(index_phi) = value;
}

double Node::stiffness_x() const
{
	return model.stiffness(index_x, index_x);
}

double Node::stiffness_y() const
{
	return model.stiffness(index_y, index_y);
}

double Node::stiffness_phi() const
{
	return model.stiffness(index_phi, index_phi);
}

int Node::dof_x() const
{
	return index_x;
}

int Node::dof_y() const
{
	return index_y;
}

int Node::dof_phi() const
{
	return index_phi;
}

double Node::distance(const Node& node1, const Node& node2)
{
	double delta_x = node2.x() - node1.x();
	double delta_y = node2.y() - node1.y();

	return std::sqrt(delta_x*delta_x + delta_y*delta_y);
}

double Node::angle(const Node& node1, const Node& node2)
{
	return std::atan2(node1.y() - node2.y(), node1.x() - node2.x());
}