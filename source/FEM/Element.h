// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef ELEMENT_H
#define ELEMENT_H

#include "Transform.h"
#include "Model.h"

#include <Eigen/Core>

#include <iostream>

/**
Element with the time-invariant equation of motion M*ddot_u + F_int(u) = F_ext\n
-M: Constant mass matrix\n
-F_int(u): Generalised internal forces of the element (Elastic forces, damping, etc.)\n
-F_ext: External forces applied to the element\n
*/
template<std::size_t n>
class Element: public ElementInterface
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	Element(const Model& model, DofMap<n> dof_map);
	virtual ~Element();

	//Functions from ElementInterface
	virtual void addMassMatrix(std::vector<Eigen::Triplet<double>>& entries) const override;
	virtual void addInternalForces(Eigen::VectorXd& f) const override;
	virtual void addTangentStiffness(std::vector<Eigen::Triplet<double>>& entries) const override;
	virtual void addPotentialEnergy(double& V) const override;
	virtual void update() override;

	//Functions to be overriden by derived element classes
    virtual Matrix<n> massMatrix() const = 0;
    virtual Vector<n> internalForces() const = 0;
    virtual Matrix<n> tangentStiffness() const;
	double kineticEnergy() const;
	virtual double potentialEnergy() const = 0;

	std::size_t dof();

protected:
	DofMap<n> dof_map;
	mutable Vector<n> u;
	mutable Vector<n> dot_u;
};

template<std::size_t n>
Element<n>::Element(const Model& model, DofMap<n> dof_map): ElementInterface(model), dof_map(dof_map)
{
	update();
}

template<std::size_t n>
Element<n>::~Element()
{

}

// Functions inherited from element interface
//////////////////////////////////////////////////////////////////////////////////////////////////////

template<std::size_t n>
void Element<n>::addMassMatrix(std::vector<Eigen::Triplet<double>>& entries) const
{
	tf::addLocalToGlobal<n>(entries, massMatrix(), dof_map);
}

template<std::size_t n>
void Element<n>::addInternalForces(Eigen::VectorXd& f) const
{
	tf::addLocalToGlobal<n>(f, internalForces(), dof_map);
}

template<std::size_t n>
void Element<n>::addTangentStiffness(std::vector<Eigen::Triplet<double>>& entries) const
{
	tf::addLocalToGlobal<n>(entries, tangentStiffness(), dof_map);
}

template<std::size_t n>
void Element<n>::addPotentialEnergy(double& V) const
{
	V += potentialEnergy();
}

template<std::size_t n>
void Element<n>::update()
{
	u = tf::globalToLocal<n>(model.coordinates(), dof_map);
	dot_u = tf::globalToLocal<n>(model.velocities(), dof_map);
}

/**
Jacobian matrix of the internal generalised forces F(u) with respect to u\n
Default implementation with numeric differentiation. In derived elements this can be replaced by analytical expressions 
for more efficiency and accuracy.
*/
template<std::size_t n>
Matrix<n> Element<n>::tangentStiffness() const
{
	double h = 1e-9;
	
	Matrix<n> K;
	Vector<n> f = internalForces();

	for(std::size_t i = 0; i < n; ++i)
	{
		//Forward difference
		//u(i) += h;
		//K.col(i) = (internalForces() - f)/h;
		//u(i) -= h;

		//Central difference
		double u_i = u(i);
		u(i) = u_i + h;
		Vector<n> fph = internalForces();
		u(i) = u_i - h;
		Vector<n> fmh = internalForces();
		u(i) = u_i;

		K.col(i) = (fph - fmh)/(2.0*h); 
	}

	//std::cout << K << "\n\n";

	return K;
}

template<std::size_t n>
double Element<n>::kineticEnergy() const
{
	return 0.5*dot_u.transpose()*massMatrix()*dot_u;
}

template<std::size_t n>
unsigned int Element<n>::dof()
{
	return n;
}

#endif // ELEMENT_H