// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "Model.h"

ElementInterface::ElementInterface(const Model& model): model(model)
{

}

Model::Model()
  : n(0),
	t(0.0)
{

}

void Model::add(ElementInterface& ei)
{
	elements.push_back(&ei);
}

void Model::updateMassMatrix()
{
	M = massMatrix();
	mass_dec.compute(M);
}

int Model::createDof()
{
	++n;

	u.conservativeResize(n);
	dot_u.conservativeResize(n);
	ddot_u.conservativeResize(n);
	f_ext.conservativeResize(n);

	return n-1;
}

std::size_t Model::dof() const
{
	return n;
}

const Eigen::VectorXd& Model::coordinates() const
{
	return u;
}

Eigen::VectorXd& Model::coordinates()
{
	return u;
}

const Eigen::VectorXd& Model::velocities() const
{
	return dot_u;
}

Eigen::VectorXd& Model::velocities()
{
	return dot_u;
}

const Eigen::VectorXd& Model::accelerations() const
{
	return ddot_u;
}

Eigen::VectorXd& Model::accelerations()
{
	return ddot_u;
}

const Eigen::VectorXd& Model::externalForces() const
{
	return f_ext;
}

Eigen::VectorXd& Model::externalForces()
{
	return f_ext;
}

double Model::time() const
{
	return t;
}

double Model::stiffness(int i1, int i2) const
{
	Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> stiffness_dec(tangentStiffness());

	if(stiffness_dec.info() != Eigen::Success)
	{
		throw std::runtime_error("Model::stiffness() : Stiffness matrix decomposition failed");
	}

	Eigen::VectorXd delta_f = Eigen::VectorXd::Zero(n);
	delta_f(i1) = 1.0;
	Eigen::VectorXd delta_u = stiffness_dec.solve(delta_f);
	return 1.0/delta_u(i2);
}

double Model::potentialEnergy() const
{
	double V = 0.0;

	for(std::size_t i = 0; i < elements.size(); ++i)
	{
		elements[i]->addPotentialEnergy(V);
	}

	return V;
}

double Model::kineticEnergy() const
{
	return 0.5*dot_u.transpose()*M*dot_u;
}

//Full newton raphson with line search
bool Model::staticEquilibrium(double epsilon, unsigned int i_max)
{
	dot_u.setZero();

	for(unsigned int i = 0; i < i_max; ++i)
	{
		Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> stiffness_dec(tangentStiffness());

		if(stiffness_dec.info() != Eigen::Success)
		{
			throw std::runtime_error("Model::staticEquilibrium() : Stiffness matrix decomposition failed");
		}

		Eigen::VectorXd ui = u;
		Eigen::VectorXd fi = f_ext - internalForces();
		Eigen::VectorXd delta_u = stiffness_dec.solve(fi);

		//Line search
		///////////////////////////////////////////////////////////
		
		auto s = [&](double alpha)
		{
			u = ui + alpha*delta_u;
			updateElements();
			Eigen::VectorXd fi = f_ext - internalForces();

			return delta_u.transpose()*fi;
		};

		double alpha0 = 0.0;
		double alpha1 = numerics::findSign<numerics::NEG>(s, 1.0, 1.5);
		numerics::root(s, alpha0, alpha1, 0.0, 0.05);
		
		///////////////////////////////////////////////////////////

		++i;

		if(delta_u.isZero(epsilon))
		{
			return true;
		}
	}

	throw std::runtime_error("Model::staticEquilibrium() : No convergence to equilibrium state");
	//return false;
}

//Dynamic relaxation with adaptive damping, http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3107576/#FD21
bool Model::staticEquilibrium2(double epsilon)
{
	//Set velocity and acceleration to 0
	dot_u.setZero();
	ddot_u.setZero();

	updateMassMatrix();

	//Timestep
	const double factor = 0.5;
	double h = factor*2.0/maxFrequency();

	Eigen::VectorXd u_prev = u - h*h/2.0*mass_dec.solve(f_ext - internalForces());
	Eigen::VectorXd f_prev;
	Eigen::VectorXd f = f_ext - internalForces();

	double c = 0.0;

	do
	{
		double alpha = 2*h*h/(2 + c*h);
		double beta = (2 - c*h)/(2 + c*h);

		Eigen::VectorXd u_temp = u;
		f_prev = f;

		f = f_ext - internalForces();
		u = u + beta*(u - u_prev) + alpha*mass_dec.solve(f);
		updateElements();
		u_prev = u_temp;

		double A0 = double((u-u_prev).transpose()*(f-f_prev))/double((u-u_prev).transpose()*M*(u-u_prev));
		//std::cout << 2.0*std::sqrt(std::abs(A0)) << ", " << f.norm() << "\n";
		
		c = 2.0*std::sqrt(std::abs(A0));
		
		std::cout << f.norm() << "\n";
	}while(f.norm() > epsilon);

	return true;
}

//Dynamic relaxation with "Kinetic damping", specified number of peaks
bool Model::staticEquilibrium3(int peaks)
{
	updateMassMatrix();

	//Timestep
	const double factor = 0.5;
	double dt = factor*2.0/maxFrequency();

	//Initialise acceleration and previous displacements
	ddot_u = mass_dec.solve(f_ext - internalForces());
	Eigen::VectorXd u_prev = u - dt*dot_u + dt*dt/2.0*ddot_u;

	double T_prev = 0.0;
	double T = 0.0;

	int i = 0;
	while(i < peaks)
	{
		Eigen::VectorXd u_temp = u;

		u = 2.0*u - u_prev + dt*dt*ddot_u;
		updateElements();
		dot_u = (u - u_prev)/(2.0*dt);		//(u - u_temp)/(dt) + dt/2.0*ddot_u; Consistent with time stepping but backwards difference and more computation
		ddot_u = mass_dec.solve(f_ext - internalForces());

		u_prev = u_temp;

		T_prev = T;
		T = 0.5*dot_u.transpose()*M*dot_u;

		if(T <= T_prev)
		{
			u = u_prev;
			++i;
		}
		//std::cout << ddot_u.norm() << ", " << T << ", " << T - T_prev << "\n";
	}

	//Set velocity and acceleration to 0
	dot_u.setZero();
	ddot_u.setZero();

	return true;
}

//http://ocw.mit.edu/resources/res-2-002-finite-element-procedures-for-solids-and-structures-spring-2010/nonlinear/lecture-13/MITRES2_002S10_lec13.pdf
void Model::integrate(double timestep_factor, std::function<bool()> callback)
{
	updateMassMatrix();

	//Timestep estimation
	double dt = timestep_factor*2.0/maxFrequency();

	//Initialise acceleration and previous displacements
	ddot_u = mass_dec.solve(f_ext - internalForces());
	Eigen::VectorXd u_prev = u - dt*dot_u + dt*dt/2.0*ddot_u;

	while(callback())
	{
		Eigen::VectorXd u_temp = u;

		u = 2.0*u - u_prev + dt*dt*ddot_u;
		updateElements();
		dot_u = (u - u_prev)/(2.0*dt);		//(u - u_temp)/(dt) + dt/2.0*ddot_u; Consistent with time stepping but backwards difference and more computation
		ddot_u = mass_dec.solve(f_ext - internalForces());

		u_prev = u_temp;
		t += dt;
	}
}

double Model::maxFrequency() const
{
	double omega_max = 0.0;

	for(std::size_t i = 0; i < elements.size(); ++i)
	{
		omega_max = std::max(omega_max, elements[i]->maxFrequency());
	}

	return omega_max;
}

Eigen::SparseMatrix<double> Model::massMatrix() const
{
	std::vector<Eigen::Triplet<double>> entries;

	for(std::size_t i = 0; i < elements.size(); ++i)
	{
		elements[i]->addMassMatrix(entries);
	}

	Eigen::SparseMatrix<double> M(n, n);
	M.setFromTriplets(entries.begin(), entries.end());

	return M;
}

Eigen::VectorXd Model::internalForces() const
{
	Eigen::VectorXd f = Eigen::VectorXd::Zero(n);

	for(std::size_t i = 0; i < elements.size(); ++i)
	{
		elements[i]->addInternalForces(f);
	}

	return f;
}

Eigen::SparseMatrix<double> Model::tangentStiffness() const
{
	std::vector<Eigen::Triplet<double>> entries;

	for(std::size_t i = 0; i < elements.size(); ++i)
	{
		elements[i]->addTangentStiffness(entries);
	}
	
	Eigen::SparseMatrix<double> K(n, n);
	K.setFromTriplets(entries.begin(), entries.end());
	
	return K;
}

void Model::updateElements()
{
	for(std::size_t i = 0; i < elements.size(); ++i)
	{
		elements[i]->update();
	}
}