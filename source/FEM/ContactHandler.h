// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef CONTACT_HANDLER_H
#define CONTACT_HANDLER_H

#include "Element.h"
#include "Node.h"
#include "ContactGeometry.h"
#include "Elements/ContactElement2D.h"

#include <Eigen/Core>

class BroadPhase
{
public:
	BroadPhase(const std::vector<ContactSurface>& lines, const std::vector<ContactPoint>& points)
		: lines(lines), points(points)
	{

	}

	std::vector<ContactPair> pairs()
	{
		std::vector<ContactPair> collision_pairs;

		for(std::size_t i = 0; i < lines.size(); ++i)
		{
			for(std::size_t j = 0; j < points.size(); ++j)
			{
				if(lines[i].aabb().intersects(points[j].aabb()))
				{
					collision_pairs.push_back({lines[i], points[j]});
				}
			}
		}

		return collision_pairs;
	}

private:
	const std::vector<ContactSurface>& lines;
	const std::vector<ContactPoint>& points;

	//std::vector<pair> collision_pairs;
};

class ContactHandler : public ElementInterface
{
public:
	///Master surface: Points connected by lines, slave surface: points
	ContactHandler(const Model& model, const std::vector<Node>& master_sufrace, const std::vector<Node>& slave_surface, const std::vector<double>& point_margins, double contact_stiffness, bool orientation)
		: ElementInterface(model), bph(contact_lines, contact_points)
	{
		for(std::size_t i = 1; i < master_sufrace.size(); ++i)
		{
			if(orientation)
			{
				contact_lines.push_back(ContactSurface(master_sufrace[i-1], master_sufrace[i]));
			}
			else
			{
				contact_lines.push_back(ContactSurface(master_sufrace[i], master_sufrace[i-1]));
			}
		}

		for(std::size_t i = 0; i < slave_surface.size(); ++i)
		{
			contact_points.push_back(ContactPoint(slave_surface[i], point_margins[i], contact_stiffness));
		}
	}

	virtual void addMassMatrix(std::vector<Eigen::Triplet<double>>& entries) const override
	{

	}

	virtual void addInternalForces(Eigen::VectorXd& f) const override
	{
		for(std::size_t i = 0; i < collision_pairs.size(); ++i)
		{
			const ContactSurface& line = collision_pairs[i].line();
			const ContactPoint& point = collision_pairs[i].point();

			ContactElement2D contact(model, line.node1(), line.node2(), point.node(), point.margin(), point.stiffness());
			contact.addInternalForces(f);
		}
	}

	virtual void addTangentStiffness(std::vector<Eigen::Triplet<double>>& entries) const override
	{
		for(std::size_t i = 0; i < collision_pairs.size(); ++i)
		{
			const ContactSurface& line = collision_pairs[i].line();
			const ContactPoint& point = collision_pairs[i].point();

			ContactElement2D contact(model, line.node1(), line.node2(), point.node(), point.margin(), point.stiffness());
			contact.addTangentStiffness(entries);
		}
	}

	virtual void addPotentialEnergy(double& V) const override
	{
		for(std::size_t i = 0; i < collision_pairs.size(); ++i)
		{
			const ContactSurface& line = collision_pairs[i].line();
			const ContactPoint& point = collision_pairs[i].point();

			ContactElement2D contact(model, line.node1(), line.node2(), point.node(), point.margin(), point.stiffness());
			contact.addPotentialEnergy(V);
		}
	}

	virtual double maxFrequency() const override
	{
		return 0.0;
	}

	virtual void update() override
	{
		collision_pairs = bph.pairs();
	}

private:
	std::vector<ContactSurface> contact_lines;
	std::vector<ContactPoint> contact_points;

	BroadPhase bph;
	std::vector<ContactPair> collision_pairs;
};

#endif // CONTACT_HANDLER_H