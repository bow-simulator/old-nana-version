// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef MODEL_H
#define MODEL_H

#include "Numerics.h"

#include <Eigen/Core>
#include <Eigen/Sparse>

#include <vector>
#include <iostream>
#include <functional>

class Model;

class ElementInterface
{
public:
	ElementInterface(const Model& model);
	virtual ~ElementInterface(){}
	virtual void addMassMatrix(std::vector<Eigen::Triplet<double>>& entries) const = 0;
	virtual void addInternalForces(Eigen::VectorXd& f) const = 0;
	virtual void addTangentStiffness(std::vector<Eigen::Triplet<double>>& entries) const = 0;
	virtual void addPotentialEnergy(double& V) const = 0;
	virtual double maxFrequency() const = 0;
	virtual void update() = 0;

protected:
	const Model& model;
};

class Model
{
public:
	Model();
	int createDof();
	std::size_t dof() const;

	void add(ElementInterface& ei);
	void updateMassMatrix();

	const Eigen::VectorXd& coordinates() const;
	Eigen::VectorXd& coordinates();
	const Eigen::VectorXd& velocities() const;	
	Eigen::VectorXd& velocities();
	const Eigen::VectorXd& accelerations() const;
	Eigen::VectorXd& accelerations();
	const Eigen::VectorXd& externalForces() const;
	Eigen::VectorXd& externalForces();
	double time() const;

	double stiffness(int i1, int i2) const;

	double potentialEnergy() const;
	double kineticEnergy() const;

	bool staticEquilibrium(double epsilon, unsigned int i_max);
	bool staticEquilibrium2(double epsilon);
	bool staticEquilibrium3(int peaks);
	void integrate(double timestep_factor, std::function<bool()> callback);

private:
	std::size_t n;									//Degrees of freedom
	std::vector<ElementInterface*> elements;

	Eigen::VectorXd u;
	Eigen::VectorXd dot_u;
	Eigen::VectorXd ddot_u;
	Eigen::VectorXd f_ext;
	double t;

	Eigen::SparseMatrix<double> M;
	Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> mass_dec;

	double maxFrequency() const;
	Eigen::SparseMatrix<double> massMatrix() const;
	Eigen::VectorXd internalForces() const;
	Eigen::SparseMatrix<double> tangentStiffness() const;

	void updateElements();
};

#endif // MODEL_H