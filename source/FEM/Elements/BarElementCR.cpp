// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "BarElementCR.h"

#include <cmath>

BarElementCR::BarElementCR(const Model& model, const Node& node1, const Node& node2, double rhoA, double EA, double L)
	: Element<4>::Element(model, DofMap<4>{{node1.dof_x(), node1.dof_y(), node2.dof_x(), node2.dof_y()}}),
	  rhoA(rhoA),
	  EA(EA),
	  L(L)
{

}

double BarElementCR::maxFrequency() const
{
	return 2.0/L*std::sqrt(EA/rhoA);
}

Matrix<4> BarElementCR::massMatrix() const
{
	Eigen::Matrix4d M;
	M << 2, 0, 1, 0,
		 0, 2, 0, 1,
		 1, 0, 2, 0,
		 0, 1, 0, 2;

	M *= rhoA*L/6.0;

	return M;
}

Vector<4> BarElementCR::internalForces() const
{
	Eigen::Matrix<double, 1, 4> Je = J();
	return Je.transpose()*EA/L*e();
}

///\todo Derivation of the tangent stiffness matrix is not quite right, the correct signs have only been found by fiddling around
Matrix<4> BarElementCR::tangentStiffness() const
{
	double a0 = u(0) - u(2);
    double a1 = u(1) - u(3);
    double b  = EA/L*(1.0 - L/sqrt(a0*a0+a1*a1));

    Matrix<4> K;
    K <<  a0*a0,  a0*a1, -a0*a0, -a0*a1,
          a0*a1,  a1*a1, -a0*a1, -a1*a1,
         -a0*a0, -a0*a1,  a0*a0,  a0*a1,
         -a0*a1, -a1*a1,  a0*a1,  a1*a1;

    K *= EA*pow(a0*a0 + a1*a1, -1.5);

    K(0, 0) += b; K(0, 2) -= b;
	K(1, 1) += b; K(1, 3) -= b;
    K(2, 0) -= b; K(2, 2) += b;
	K(3, 1) -= b; K(3, 3) += b;

    return K;
}

double BarElementCR::potentialEnergy() const
{
	//V(x) = 1/2*EA/L*(L(x) - L)^2
	double a0 = u(0) - u(2);
    double a1 = u(1) - u(3);

	return 0.5*EA/L*pow(sqrt(a0*a0+a1*a1) - L, 2.0);
}

double BarElementCR::longitudinalForce() const
{
	double a0 = u(0) - u(2);
    double a1 = u(1) - u(3);

	return EA/L*(sqrt(a0*a0 + a1*a1) - L);
}

double BarElementCR::e() const
{
	double d0 = u(2) -u (0);
	double d1 = u(3) - u(1);

	return std::sqrt(d0*d0 + d1*d1)-L;
}

Eigen::Matrix<double, 1, 4> BarElementCR::J() const
{
	double d0 = u(2) - u(0);
	double d1 = u(3) - u(1);
	double d = std::sqrt(d0*d0 + d1*d1);
	double c0 = d0/d;
	double c1 = d1/d;
	
	return (Eigen::Matrix<double, 1, 4>() << -c0, -c1, c0,  c1).finished();
}