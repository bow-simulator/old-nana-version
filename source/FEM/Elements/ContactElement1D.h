// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef CONTACT_ELEMENT_1D_H
#define CONTACT_ELEMENT_1D_H

#include "../Element.h"
#include "../Node.h"

#include <Eigen/Core>

/**
One-dimensional contact element.\n
It has two coordinates, x = [x0, x1]. Penetration for x0 > x1. There are two modes:\n
\n
- One-sided: Linear contact force c*(x1 - x0) if x0 > x1, else zero. This is used for dynamic simulation.
- Not one-sided: Linear contact force c*(x1 - x0) in any case. This essentially fixes the contact and is used for the static calculations, because the stiffness matrix would be irregular in one-sided mode.
\n
The element is also damped linearly with a damping force d*(dot_x1 - dot_x0) if x0 > x1
*/
class ContactElement1D: public Element<2>
{
public:
	ContactElement1D(const Model& model, const Node& node1, const Node& node2, double mass1, double mass2, double stiffness, bool one_sided);
	
	double maxFrequency() const override;

	virtual Matrix<2> massMatrix() const override;
    virtual Vector<2> internalForces() const override;
	virtual Matrix<2> tangentStiffness() const override;
	virtual double potentialEnergy() const override;	///< \todo Calculate potential energy of the element

	void oneSided(bool b);

private:
	double m1;
	double m2;
	double stiffness;
	bool one_sided;

	double contactForce(double h) const;	///< Contact force, h = x1 - x0. Contact for h < 0
	double contactForceDeriv(double h) const;
	double potentialEnergy(double h) const;
};

#endif // CONTACT_ELEMENT_1D_H