// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef CONTACT_ELEMENT_2D_H
#define CONTACT_ELEMENT_2D_H

#include "../Element.h"
#include "../Node.h"

#include <Eigen/Core>

/**
Contact between node3 and the line connecting node1 and node2.
*/
class ContactElement2D: public Element<6>
{

public:
	ContactElement2D(const Model& model, const Node& node1, const Node& node2, const Node& node3, double margin, double stiffness);
	
	double maxFrequency() const override;
	
	virtual Matrix<6> massMatrix() const override;
    virtual Vector<6> internalForces() const override;
	virtual double potentialEnergy() const override;

private:
    double margin;
	double stiffness;

	double penetration() const;
	double normalForce(double h) const;
	double potentialEnergy(double h) const;
	
	Eigen::Matrix<double, 1, 6> J() const;
};

#endif // CONTACT_ELEMENT_2D_H