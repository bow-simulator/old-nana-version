// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef MASS_ELEMENT_H
#define MASS_ELEMENT_H

#include "../Element.h"
#include "../Node.h"

#include <Eigen/Core>

class MassElement: public Element<2>
{
public:
	MassElement(const Model& model, const Node& node, double m);
	void mass(double val);
	double mass() const;

	double maxFrequency() const override;

	virtual Matrix<2> massMatrix() const override;
    virtual Vector<2> internalForces() const override;
	virtual Matrix<2> tangentStiffness() const override;
	virtual double potentialEnergy() const override;

private:
    double m;
};

#endif // MASS_ELEMENT_H