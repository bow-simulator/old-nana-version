// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef BEAM_ELEMENT_CR_H
#define BEAM_ELEMENT_CR_H

#include "../Element.h"
#include "../Node.h"

#include <Eigen/Core>

/**
Simple co-rotational Euler-Bernoulli beam element with a lumped mass matrix ([1], [2], [3])
\n
[1] http://www.colorado.edu/engineering/CAS/courses.d/MFEMD.d/MFEMD.Ch18.d/MFEMD.Ch18.pdf\n
[2] http://me-lrt.de/querelastischer-balken-euler-bernoulli\n
[3] 2D CRational Beam Formulation - by Louie L. Yaw - Walla Walla University - November 30, 2009
*/
class BeamElementCR: public Element<6>
{
public:
	BeamElementCR(const Model& model, const Node& node1, const Node& node2, double rhoA, double EI, double EA);
	BeamElementCR(const Model& model, const Node& node1, const Node& node2, double L, double rhoA, double EI, double EA);
	
	double length() const;
	void length(double val);

	double maxFrequency() const override;
	virtual Matrix<6> massMatrix() const override;
    virtual Vector<6> internalForces() const override;
	//virtual Eigen::MatrixXd getTangentStiffness(const Eigen::VectorXd& x) const override;
	virtual double potentialEnergy() const override;
	///Returns a vector containing the strain and curvature of the centerline at parameter 0 <= zeta <= 1. \todo Strange distribution of longitudinal strain along the element
	Vector<2> deformation(double zeta) const;
	double longitudinalForce() const;
	double shearForce() const;

private:
	double rhoA;
	double EI;
	double EA;
	double L;

	double phi;

	//Stiffness matrix defined in co-rotated reference frame
	Matrix<3> K() const;

	//Get the elastic coordinates defined in the co-rotated frame
	Vector<3> e() const;

	//Jacobian matrix of the elastic coordinates w.r.t. the global coordinates
	Eigen::Matrix<double, 3, 6> J() const;
};

#endif // BEAM_ELEMENT_CR_H