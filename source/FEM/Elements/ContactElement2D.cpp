// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "ContactElement2D.h"

ContactElement2D::ContactElement2D(const Model& model, const Node& node1, const Node& node2, const Node& node3, double margin, double stiffness)
	  : Element<6>::Element(model, DofMap<6>{{node1.dof_x(), node1.dof_y(), node2.dof_x(), node2.dof_y(), node3.dof_x(), node3.dof_y()}}),
	    margin(margin),
		stiffness(stiffness)
{

}

double ContactElement2D::maxFrequency() const
{
	return 0.0;
}

Matrix<6> ContactElement2D::massMatrix() const
{
	return Matrix<6>::Zero();
}

Vector<6> ContactElement2D::internalForces() const
{
	double s1 = (u(4)-u(0))*(u(2)-u(0)) + (u(5)-u(1))*(u(3)-u(1));
	double s2 = (u(2)-u(4))*(u(2)-u(0)) + (u(3)-u(5))*(u(3)-u(1));

	if(s1 >= 0 && s2 >= 0)//u(3) > u(5) && u(5) >= u(1))
	{
		//std::cout << "Contact, " << penetration() << "\n";
		return J().transpose()*normalForce(penetration());
	}
	else
	{
		return Vector<6>::Zero();
	}
}

double ContactElement2D::potentialEnergy() const
{
	return potentialEnergy(penetration());
}

double ContactElement2D::penetration() const
{
	return ((u(4)-u(0))*(u(3)-u(1)) - (u(5)-u(1))*(u(2)-u(0)))
			/std::sqrt((u(2)-u(0))*(u(2)-u(0)) + (u(3)-u(1))*(u(3)-u(1))) - margin;
}

double ContactElement2D::normalForce(double h) const
{
	if(h < 0.0)
	{
		return stiffness*h;
	}
	else
	{
		return 0.0;
	}
}

double ContactElement2D::potentialEnergy(double h) const
{
	if(h < 0.0 && (u(3) > u(5) && u(5) >= u(1)) )
	{
		return 0.5*stiffness*h*h;
	}
	else
	{
		return 0.0;
	}
}

Eigen::Matrix<double, 1, 6> ContactElement2D::J() const
{
	double c0 = (u(3)-u(1))*(u(4)-u(0))-(u(2)-u(0))*(u(5)-u(1));
	double c1 = std::sqrt((u(3)-u(1))*(u(3)-u(1)) + (u(2)-u(0))*(u(2)-u(0)));
	double c2 = std::pow((u(3)-u(1))*(u(3)-u(1)) + (u(2)-u(0))*(u(2)-u(0)), 1.5);

	return (Eigen::Matrix<double, 1, 6>() << (u(5)-u(3))/c1 + c0/c2*(u(2)-u(0)), (u(2)-u(4))/c1 + c0/c2*(u(3)-u(1)), (u(1)-u(5))/c1 - c0/c2*(u(2)-u(0)), (u(4)-u(0))/c1 - c0/c2*(u(3)-u(1)), (u(3)-u(1))/c1, (u(0)-u(2))/c1).finished();
}