// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "BeamElementCR.h"

BeamElementCR::BeamElementCR(const Model& model, const Node& node1, const Node& node2, double rhoA, double EI, double EA)
	: Element<6>::Element(model, DofMap<6>{{node1.dof_x(), node1.dof_y(), node1.dof_phi(), node2.dof_x(), node2.dof_y(), node2.dof_phi()}}),
	  rhoA(rhoA),
	  EI(EI),
	  EA(EA),
	  L(Node::distance(node1, node2)),
	  phi(Node::angle(node1, node2))
{

}

BeamElementCR::BeamElementCR(const Model& model, const Node& node1, const Node& node2, double L, double rhoA, double EI, double EA)
	: Element<6>::Element(model, DofMap<6>{{node1.dof_x(), node1.dof_y(), node1.dof_phi(), node2.dof_x(), node2.dof_y(), node2.dof_phi()}}),
	  rhoA(rhoA),
	  EI(EI),
	  EA(EA),
	  L(L),
	  phi(0.0)
{

}

double BeamElementCR::length() const
{
	return L;
}

void BeamElementCR::length(double val)
{
	L = val;
}

double BeamElementCR::maxFrequency() const
{
	return 2.0/L*std::sqrt(EA/rhoA);
}

Matrix<6> BeamElementCR::massMatrix() const
{
	const double alpha = 0.02;

	return rhoA*L*(Matrix<6>() << 0.5,     0,         0,   0,   0,         0,
					                0,   0.5,         0,   0,   0,         0,
					                0,     0, alpha*L*L,   0,   0,         0,
					                0,     0,         0, 0.5,   0,         0,
					                0,     0,         0,   0, 0.5,         0,
					                0,     0,         0,   0,   0, alpha*L*L).finished();
}

Vector<6> BeamElementCR::internalForces() const
{
	Eigen::Matrix<double, 3, 6> Je = J();
	return Je.transpose()*K()*e();
}

double BeamElementCR::potentialEnergy() const
{
	Vector<3> u_e = e();
	return 0.5*u_e.transpose()*K()*u_e;
}

Vector<2> BeamElementCR::deformation(double zeta) const
{
	Vector<3> u_e = e();
	double strain = u_e(0)/L;
	double curvature = (6.0*zeta - 4.0)/L*u_e(1) + (6.0*zeta - 2.0)/L*u_e(2);

	return Vector<2>{strain, curvature};
}

double BeamElementCR::longitudinalForce() const
{
	double a0 = u(0) - u(3);
    double a1 = u(1) - u(4);

	return EA/L*(sqrt(a0*a0 + a1*a1) - L);
}

double BeamElementCR::shearForce() const
{
	Vector<3> u_e = e();
	return 6.0*EI/(L*L)*(u_e(1) + u_e(2));
}

Matrix<3> BeamElementCR::K() const
{
	return (Matrix<3>() << EA/L,        0,        0,
						      0, 4.0*EI/L, 2.0*EI/L,
						      0, 2.0*EI/L, 4.0*EI/L).finished();
}

Vector<3> BeamElementCR::e() const
{
	double d0 = u(3)-u(0);
	double d1 = u(4)-u(1);
	double alpha = std::atan2(d1, d0);
	
	double e0 = std::sqrt(d0*d0 + d1*d1)-L;

	double e1 = std::atan((std::sin(u(2) + phi)*std::cos(alpha) - std::cos(u(2) + phi)*std::sin(alpha))/
		                  (std::cos(u(2) + phi)*std::cos(alpha) + std::sin(u(2) + phi)*std::sin(alpha)));

	double e2 = std::atan((std::sin(u(5) + phi)*std::cos(alpha) - std::cos(u(5) + phi)*std::sin(alpha))/
		                  (std::cos(u(5) + phi)*std::cos(alpha) + std::sin(u(5) + phi)*std::sin(alpha)));
	
	return Eigen::Vector3d{e0, e1, e2};
}

Eigen::Matrix<double, 3, 6> BeamElementCR::J() const
{
	double d0 = u(3) - u(0);
	double d1 = u(4) - u(1);
	double d = std::sqrt(d0*d0 + d1*d1);

	double c0 = d0/d;
	double c1 = d1/d;
	double c2 = c0/d;
	double c3 = c1/d;
	
	return (Eigen::Matrix<double, 3, 6>() << -c0, -c1, 0, c0,  c1, 0,
											 -c3,  c2, 1, c3, -c2, 0,
											 -c3,  c2, 0, c3, -c2, 1).finished();
}