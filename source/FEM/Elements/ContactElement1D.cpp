// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "ContactElement1D.h"

#include <cmath>

ContactElement1D::ContactElement1D(const Model& model, const Node& node1, const Node& node2, double mass1, double mass2, double stiffness, bool one_sided)
	: Element<2>::Element(model, DofMap<2>{{node1.dof_x(), node2.dof_x()}}),
	  m1(mass1),
	  m2(mass2),
	  stiffness(stiffness),
	  one_sided(one_sided)
{

}

///< \todo validate
double ContactElement1D::maxFrequency() const
{
	if(m1 > 0 && m2 > 0)
	{
		return std::sqrt(stiffness/m1 + stiffness/m2);
	}
	else
	{
		return 0.0;
	}
}

Matrix<2> ContactElement1D::massMatrix() const
{
	Matrix<2> M;
	M << m1,  0,
		  0, m2;

	return M;
}

Vector<2> ContactElement1D::internalForces() const
{
	double F = contactForce(u(1) - u(0));
	return Eigen::Vector2d{-F, F};
}

Matrix<2> ContactElement1D::tangentStiffness() const
{
	Matrix<2> K;
	K << 1, -1,
		-1,  1;

	return K*contactForceDeriv(u(1) - u(0));
}

double ContactElement1D::potentialEnergy() const
{
	return potentialEnergy(u(1) - u(0));
}

void ContactElement1D::oneSided(bool b)
{
	one_sided = b;
}

double ContactElement1D::contactForce(double h) const
{
	if(!one_sided || h < 0.0)
	{
		return stiffness*h;
	}
	else
	{
		return 0.0;
	}
}

double ContactElement1D::contactForceDeriv(double h) const
{
	if(!one_sided || h < 0.0)
	{
		return stiffness;
	}
	else
	{
		return 0.0;
	}
}

double ContactElement1D::potentialEnergy(double h) const
{
	if(!one_sided || h < 0.0)
	{
		return 0.5*stiffness*h*h;
	}
	else
	{
		return 0.0;
	}
}