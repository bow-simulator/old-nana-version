// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef BAR_ELEMENT_CR_H
#define BAR_ELEMENT_CR_H

#include "../Element.h"
#include "../Node.h"

#include <Eigen/Core>

/**
Simple co-rotational bar element.
*/
class BarElementCR: public Element<4>
{
public:
	BarElementCR(const Model& model, const Node& node1, const Node& node2, double rhoA, double EA, double L);
	
	double maxFrequency() const override;

	virtual Matrix<4> massMatrix() const override;
    virtual Vector<4> internalForces() const override;
	virtual Matrix<4> tangentStiffness() const override;
	virtual double potentialEnergy() const override;

	double longitudinalForce() const;

private:
    double rhoA;
    double EA;
	double L;

	//Get the elastic coordinate defined in the co-rotated frame
	double e() const;

	//Jacobian matrix of the elastic coordinates w.r.t. the global coordinates
	Eigen::Matrix<double, 1, 4> J() const;
};

#endif // BAR_ELEMENT_CR_H