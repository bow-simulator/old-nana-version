// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "MassElement.h"

MassElement::MassElement(const Model& model, const Node& node, double m)
	: Element<2>::Element(model, DofMap<2>{{node.dof_x(), node.dof_y()}}),
	  m(m)
{

}

void MassElement::mass(double val)
{
	m = val;
}

double MassElement::mass() const
{
	return m;
}

double MassElement::maxFrequency() const
{
	return 0.0;
}

Matrix<2> MassElement::massMatrix() const
{
	Matrix<2> M;
	M << m, 0,
		 0, m;

	return M;
}

Vector<2> MassElement::internalForces() const
{
	return Vector<2>::Zero();
}

Matrix<2> MassElement::tangentStiffness() const
{
	return Matrix<2>::Zero();
}

double MassElement::potentialEnergy() const
{
	return 0.0;
}