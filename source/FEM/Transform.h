// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <Eigen/Core>
#include <Eigen/Sparse>

#include <array>

template<std::size_t n>
using Matrix = Eigen::Matrix<double, n, n>;

template<std::size_t n>
using Vector = Eigen::Matrix<double, n, 1>;

template<std::size_t n>
using DofMap = std::array<int, n>;

namespace tf
{
	const static int FIXED = -1;

	template<std::size_t n>
	Vector<n> globalToLocal(const Eigen::VectorXd& y, const DofMap<n>& dof_map)
	{
		Vector<n> x;

		for(std::size_t i = 0; i < n; ++i)
		{
			if(dof_map[i] >= 0)
			{
				x(i) = y(dof_map[i]);
			}
			else
			{
				x(i) = 0;
			}
		}

		return x;
	}

	template<std::size_t n>
	void addLocalToGlobal(Eigen::VectorXd& global_vector, const Vector<n>& local_vector, const DofMap<n>& dof_map)
	{
		for(std::size_t i = 0; i < n; ++i)
		{
			if(dof_map[i] >= 0)
			{
				global_vector(dof_map[i]) += local_vector(i);
			}
		}
	}
	
	template<std::size_t n>
	void addLocalToGlobal(std::vector<Eigen::Triplet<double>>& global_matrix_entries, const Matrix<n>& local_matrix, const DofMap<n>& dof_map)
	{
		for(std::size_t i = 0; i < n; ++i)
		{
			if(dof_map[i] >= 0)
			{
				for(std::size_t j = 0; j < n; ++j)
				{
					if(dof_map[j] >= 0)
					{
						global_matrix_entries.push_back(Eigen::Triplet<double>(dof_map[i], dof_map[j], local_matrix(i, j)));
					}
				}
			}
		}
	}
}


#endif // TRANSFORM_H
