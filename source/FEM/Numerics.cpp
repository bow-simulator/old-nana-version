// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "Numerics.h"

numerics::Sign numerics::sgn(double val)
{
	return val > 0 ? numerics::POS : numerics::NEG;
}

void numerics::sort(double& xa, double& xb)
{
	if(xa > xb)
	{
		double tmp(xa);
		xa = xb;
		xb = tmp;
	}
}



double numerics::root(const std::function<double(double)>& f,
					  double xa,
					  double xb,
					  double ftol,
					  double xtol,
					  unsigned int iter)
{
	assert(xa <= xb);

	Sign sign = sgn(f(xa));

	for(unsigned int i = 0; i < iter; ++i)
	{
		double x_new = 0.5*(xa + xb);
		double f_new = f(x_new);

		if(std::abs(f_new) < ftol || xb - xa < xtol)
		{
			return x_new;
		}

		if(sign*f_new > 0)
		{
			xa = x_new;
		}
		else
		{
			xb = x_new;
		}
	}

	throw std::runtime_error("numerics::root(): Failed to find root\n");
}

double numerics::root(const std::function<double(double)>& f,
					  const std::function<double(double)>& df,
					  double x0,
					  double ftol,
					  double xtol,
					  unsigned int iter)
{
	double f0 = f(x0);

	for(unsigned int i = 0; i < iter; ++i)
	{
		double x1 = x0 - f0/df(x0);
		double f1 = f(x1);

		if(std::abs(f1) < ftol || std::abs(x1 - x0) < xtol)
		{
			return x1;
		}

		x0 = x1;
		f0 = f1;
	}

	throw std::runtime_error("numerics::root(): Failed to find root\n");
}

/*
//Brent's root finding algorithm, adapted from scipy function brentq, http://www.scipy.org/
//https://github.com/scipy/scipy/blob/033bfe49ba1b455bf6f26ff9d281b0a6d44b21c2/scipy/optimize/Zeros/brentq.c
//Should be faster then bisection, only problem: Evaluates function at start and end of interval, while bisection doesn't.
double root(const std::function<double(double)>& f, double xa, double xb, double ftol, double xtol, int iter = 20)
{
	double xblk = 0.0;
	double fblk = 0.0;
	double sa = 0.0;
	double sb = 0.0;
	
	double fa = f(xa);
	double fb = f(xb);

	assert(fa*fb <= 0);

	if(fa == 0)
	{
		return xa;
	}

	if(fb == 0)
	{
		return xb;
	}

	for(unsigned int i = 0; i < iter; ++i)
	{
		if(fa*fb < 0)
		{
			xblk = xa;
			fblk = fa;
			sb = xb - xa;
			sa = sb;
		}

		if(std::abs(fblk) < std::abs(fb))
		{
			xa = xb;
			xb = xblk;
			xblk = xa;
			fa = fb;
			fb = fblk;
			fblk = fa;
		}

		double rtol = 2*std::numeric_limits<double>::epsilon();
		double tol = xtol + rtol*std::abs(xb);

		double sbis = (xblk - xb)/2.0;

		if(std::abs(fb) < ftol || std::abs(sbis) < tol)
		{
			return xb;
		}

		if(std::abs(sa) > tol && std::abs(fb) < std::abs(fa))
		{
			double stry;
			if(xa == xblk)
			{
				// Interpolate
				stry = -fb*(xb - xa)/(fb - fa);
			}
			else
			{
				// Extrapolate
				double dpre = (fa - fb)/(xa - xb);
				double dblk = (fblk - fb)/(xblk - xb);
				stry = -fb*(fblk*dblk - fa*dpre)/(dblk*dpre*(fblk - fa));
			}
			
			if(2.0*std::abs(stry) < std::min(std::abs(sa), 3.0*std::abs(sbis) - tol))
			{
				// Good short step
				sa = sb;
				sb = stry;
			}
			else
			{
				// Bisection
				sa = sbis;
				sb = sbis;
			}
		}
		else
		{
			// Bisection
			sa = sbis;
			sb = sbis;
		}

		xa = xb;
		fa = fb;
		
		if(std::abs(sb) > tol)
		{
			xb += sb;
		}
		else
		{
			xb += (sbis > 0 ? tol : -tol);
		}

		fb = f(xb);
	}

	throw std::runtime_error("numerics::root(): Failed to find root\n");
}
*/