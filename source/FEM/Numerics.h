// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef NUMERICS_H
#define NUMERICS_H

#include <functional>
#include <assert.h>
#include <iostream>

namespace numerics
{

	enum Sign{ POS = 1, NEG = -1 };

	Sign sgn(double val);
	void sort(double& xa, double& xb);

	template<Sign target_sign>
	double findSign(const std::function<double(double)>& f, double x0, double factor, unsigned int iter = 20)
	{
		assert(x0 != 0 && factor != 0);

		for(unsigned int i = 0; i < iter; ++i)
		{
			if(sgn(f(x0)) == target_sign)
			{
				return x0;
			}

			x0 *= factor;
		}
	
		throw std::runtime_error("numerics::findSign(): Failed to find sign change\n");
	}

	template<Sign target_sign>
	double findSignChange(const std::function<double(double)>& f, double x0, double factor, unsigned int iter = 20)
	{
		assert(x0 != 0 && factor != 0);

		Sign start_sign = sgn(f(x0));
	
		double x1 = x0;
		for(unsigned int i = 0; i < iter; ++i)
		{

			//std::cout << x1 << ", " << f(x1) << "\n";

			if(start_sign == target_sign)
			{
				x1 /= factor;
			}
			else
			{
				x1 *= factor;
			}


			if(sgn(f(x1)) != start_sign)
			{
				return x1;
			}
		}
	
		throw std::runtime_error("numerics::findSignChange(): Failed to find sign change\n");
	}

	double root(const std::function<double(double)>& f,
				double xa,
				double xb,
				double ftol,
				double xtol,
				unsigned int iter = 20);
	
	double root(const std::function<double(double)>& f,
				const std::function<double(double)>& df,
				double x0,
				double ftol,
				double xtol,
				unsigned int iter = 20);
};

#endif // NUMERICS_H