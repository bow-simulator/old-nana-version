// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef NODE_H
#define NODE_H

#include <Eigen/Core>
#include <array>
#include <cmath>

class Model;

class Node
{
public:
	//Construct Node by values for position and rotation. Specify if positions and rotations are fixed.
	Node(Model& model, double x_val, double y_val, double phi_val, bool free_x, bool free_y, bool free_phi);

	//Construct Node from existing Node. New Node has identical position but different rotational DOF
	Node(Model& model, const Node& node, double phi_val);

	double x() const;
	double y() const;
	double phi() const;

	double dot_x() const;
	double dot_y() const;
	double dot_phi() const;

	double ddot_x() const;
	double ddot_y() const;
	double ddot_phi() const;

	double force_x() const;
	double force_y() const;
	double torque() const;

	void force_x(double value);
	void force_y(double value);
	void torque(double value);

	double stiffness_x() const;
	double stiffness_y() const;
	double stiffness_phi() const;
	
	int dof_x() const;
	int dof_y() const;
	int dof_phi() const;

	static double distance(const Node& node1, const Node& node2);
	static double angle(const Node& node1, const Node& node2);

private:
	Model& model;

	int index_x;
	int index_y;
	int index_phi;

	double fixed_value_x;
	double fixed_value_y;
	double fixed_value_phi;
};

#endif // NODE_H