// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "StaticData.h"
#include "Bow.h"
#include "LimbGeometry.h"

void StaticData::addBowState(const Bow& bow, const LimbGeometry& geometry)
{
	draw_length.push_back(bow.drawLength());
	draw_force.push_back(bow.getDrawForce());
	potential_energy.push_back(bow.potentialEnergy());
	
	limb_shape_x.push_back(std::vector<double>());
	limb_shape_v.push_back(std::vector<double>());
	limb_epsilon.push_back(std::vector<double>());
	limb_kappa.push_back(std::vector<double>());
	bow.getLimbShape(limb_shape_x.back(), limb_shape_v.back(), limb_epsilon.back(), limb_kappa.back());
	
	stress_back.push_back(geometry.stress(limb_epsilon.back(), limb_kappa.back(), 1.0));
	stress_belly.push_back(geometry.stress(limb_epsilon.back(), limb_kappa.back(), -1.0));

	string_shape_x.push_back(std::vector<double>());
	string_shape_y.push_back(std::vector<double>());
	bow.getStringShape(string_shape_x.back(), string_shape_y.back());
	string_force.push_back(bow.stringForceCenter());
}

const std::vector<double>& StaticData::drawLength() const
{
	return draw_length;
}

const std::vector<double>& StaticData::drawForce() const
{
	return draw_force;
}

const std::vector<double>& StaticData::potentialEnergy() const
{
	return potential_energy;
}

const std::vector<double>& StaticData::stringForce() const
{
	return string_force;
}

const std::vector<std::vector<double>>& StaticData::limbShapeX() const
{
	return limb_shape_x;
}

const std::vector<std::vector<double>>& StaticData::limbShapeY() const
{
	return limb_shape_v;
}

const std::vector<std::vector<double>>& StaticData::limbStrain() const
{
	return limb_epsilon;
}

const std::vector<std::vector<double>>& StaticData::limbCurvature() const
{
	return limb_kappa;
}

const std::vector<std::vector<double>>& StaticData::stressBack() const
{
	return stress_back;
}

const std::vector<std::vector<double>>& StaticData::stressBelly() const
{
	return stress_belly;
}

const std::vector<std::vector<double>>& StaticData::stringShapeX() const
{
	return string_shape_x;
}

const std::vector<std::vector<double>>& StaticData::stringShapeY() const
{
	return string_shape_y;
}

double StaticData::finalDrawForce() const
{
	return drawForce().back();
}

double StaticData::stringLength() const
{
	return 3.1415926535;
}

double StaticData::energyBraced() const
{
	return potentialEnergy().front();
}

double StaticData::energyDrawn() const
{
	return potentialEnergy().back();
}

double StaticData::drawingWork() const
{
	return energyDrawn() - energyBraced();
}

double StaticData::storageRatio() const
{
	return drawingWork()/(0.5*finalDrawForce()*(drawLength().back() - drawLength().front()));
}

double StaticData::maxStress() const
{
	double max_stress = 0.0;

	for(std::size_t i = 0; i < stress_back.back().size(); ++i)
	{
		double stress_back_abs = std::abs(stress_back.back()[i]);
		double stress_belly_abs = std::abs(stress_belly.back()[i]);
		
		if(stress_back_abs > max_stress)
		{
			max_stress = stress_back_abs;
		}
		if(stress_belly_abs > max_stress)
		{
			max_stress = stress_belly_abs;
		}
	}

	return max_stress;
}

void StaticData::save(const std::wstring& path)
{
	jsoncons::wjson obj;

	obj[L"final_draw_force"] = units::force.from_base(finalDrawForce());
	obj[L"string_length"] = units::length.from_base(stringLength());
	obj[L"energy_braced"] = units::energy.from_base(energyBraced());
	obj[L"energy_drawn"] = units::energy.from_base(energyDrawn());
	obj[L"drawing_work"] = units::energy.from_base(drawingWork());
	obj[L"storage_ratio"] = units::percent.from_base(storageRatio());
	obj[L"max_stress"] = units::stress.from_base(maxStress());

	std::wofstream file;
	file.open(path);
	file << jsoncons::pretty_print(obj);
	file.close();
}