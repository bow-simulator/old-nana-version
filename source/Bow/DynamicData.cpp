// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "DynamicData.h"
#include "Bow.h"

void DynamicData::addBowState(const Bow& bow)
{
	const double delta_t = 1.0e-4;		//Time resolution for output

	if(t.size() == 0 || bow.time() - t.back() > delta_t)
	{
		t.push_back(bow.time());

		string_position.push_back(-bow.stringPosition());
		string_velocity.push_back(-bow.stringVelocity());
		string_acceleration.push_back(-bow.stringAcceleration());
		arrow_position.push_back(-bow.arrowPosition());
		arrow_velocity.push_back(-bow.arrowVelocity());
		arrow_acceleration.push_back(-bow.arrowAcceleration());

		potential_energy.push_back(bow.potentialEnergy());
		kinetic_energy.push_back(bow.kineticEnergy());
		total_energy.push_back(potential_energy.back() + kinetic_energy.back());
		arrow_energy.push_back(bow.arrowEnergy());

		limb_shape_u.push_back(std::vector<double>());
		limb_shape_v.push_back(std::vector<double>());
		limb_epsilon.push_back(std::vector<double>());
		limb_kappa.push_back(std::vector<double>());
		bow.getLimbShape(limb_shape_u.back(), limb_shape_v.back(), limb_epsilon.back(), limb_kappa.back());

		string_shape_u.push_back(std::vector<double>());
		string_shape_v.push_back(std::vector<double>());
		bow.getStringShape(string_shape_u.back(), string_shape_v.back());
		string_force.push_back(bow.stringForceCenter());
		grip_force.push_back(bow.gripForce());
	}
}

const std::vector<double>& DynamicData::time() const
{
	return t;
}

const std::vector<double>& DynamicData::stringPosition() const
{
	return string_position;
}

const std::vector<double>& DynamicData::stringVelocity() const
{
	return string_velocity;
}

const std::vector<double>& DynamicData::stringAcceleration() const
{
	return string_acceleration;
}

const std::vector<double>& DynamicData::arrowPosition() const
{
	return arrow_position;
}

const std::vector<double>& DynamicData::arrowVelocity() const
{
	return arrow_velocity;
}

const std::vector<double>& DynamicData::arrowAcceleration() const
{
	return arrow_acceleration;
}

const std::vector<double>& DynamicData::potentialEnergy() const
{
	return potential_energy;
}

const std::vector<double>& DynamicData::kineticEnergy() const
{
	return kinetic_energy;
}

const std::vector<double>& DynamicData::totalEnergy() const
{
	return total_energy;
}

const std::vector<double>& DynamicData::arrowEnergy() const
{
	return arrow_energy;
}

const std::vector<double>& DynamicData::stringForce() const
{
	return string_force;
}

const std::vector<double>& DynamicData::gripForce() const
{
	return grip_force;
}

const std::vector<std::vector<double>>& DynamicData::limbShapeU() const
{
	return limb_shape_u;
}

const std::vector<std::vector<double>>& DynamicData::limbShapeV() const
{
	return limb_shape_v;
}

const std::vector<std::vector<double>>& DynamicData::limbStrain() const
{
	return limb_epsilon;
}

const std::vector<std::vector<double>>& DynamicData::limbCurvature() const
{
	return limb_kappa;
}

const std::vector<std::vector<double>>& DynamicData::stringShapeU() const
{
	return string_shape_u;
}

const std::vector<std::vector<double>>& DynamicData::stringShapeV() const
{
	return string_shape_v;
}

double DynamicData::finalArrowVelocity() const
{
	return std::abs(arrowVelocity().back());
}

double DynamicData::finalArrowEnergy() const
{
	return arrow_energy.back();
}

double DynamicData::efficiency(const StaticData& static_data) const
{
	return finalArrowEnergy()/static_data.drawingWork();
}

void DynamicData::save(const std::wstring& path)
{
	jsoncons::wjson obj;

	obj[L"final_arrow_velocity"] = units::velocity.from_base(finalArrowVelocity());
	obj[L"final_arrow_energy"] = units::energy.from_base(finalArrowEnergy());

	std::wofstream file;
	file.open(path);
	file << jsoncons::pretty_print(obj);
	file.close();
}