// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "Bow.h"
#include "LimbGeometry.h"

Bow::Bow(const LimbGeometry& limb, const BowParameters& parameters, const BowSettings& settings)
	: limb(limb),
	  parameters(parameters),
	  settings(settings)
{
	init_limb();

	//Brace bow
	try
	{
		init_string_brace_height();
	}
	catch(std::exception& e)
	{
		throw std::runtime_error(std::string("Bow::Bow(): Failed to find braced equilibrium of the bow\n") + e.what());
	}

	/*
	if(settings.contact_handling)
	{
		init_string_contact();
	}
	else
	{
		init_string_no_contact();
	}
	*/
	
	//Limb tip mass
	element_tip = new MassElement(model, nodes_limb.back(), parameters.mass_limb_tip);
	model.add(*element_tip);

	//String center mass
	element_string_center = new MassElement(model, nodes_string.front(), parameters.mass_serving);
	model.add(*element_string_center);

	//Arrow center mass
	element_arrow = new MassElement(model, nodes_string.front(), parameters.mass_arrow);
	model.add(*element_arrow);

	/*
	//String center mass
	node_arrow = new Node(model, nodes_string.front().x(), nodes_string.front().y(), 0.0, true, false, false);
	
	//Contact element between string and arrow, 0.5 because of symmetric model
	double contact_stiffness = 10.0*parameters.string_EA/parameters.string_length;
	contact_arrow = new ContactElement1D(model, *node_arrow, nodes_string.front(), 0.5*parameters.mass_arrow, 0.5*parameters.mass_serving, contact_stiffness, false);
	model.add(*contact_arrow);
	*/

}

void Bow::init_limb()
{
	nodes_limb.push_back(Node(model, limb.x()[0], limb.y()[0], 0.0, false, false, false));

	for(std::size_t i = 0; i < settings.elements; ++i)
	{
		nodes_limb.push_back(Node(model, limb.x()[i+1], limb.y()[i+1], 0.0, true, true, true));
		
		double rhoA = 0.5*(limb.rhoA()[i] + limb.rhoA()[i+1]);
		double EI = 0.5*(limb.EI()[i] + limb.EI()[i+1]);
		double EA = 0.5*(limb.EA()[i] + limb.EA()[i+1]);

		BeamElementCR* element = new BeamElementCR(model, nodes_limb[i], nodes_limb[i+1], rhoA, EI, EA);
		elements_limb.push_back(element);
		model.add(*element);
	}
}

void Bow::init_string_brace_height()
{
	//Prepare limb for bracing
	//////////////////////////////////////////////////////////////////////////////////

	//Returns the difference between the rightmost point on the limb and the brace height
	auto brace_pos = [&](double torque)
	{
		//Apply torque to the limb tip
		BeamElementCR* last = elements_limb.back();
		
		nodes_limb.back().torque(-torque);
		model.staticEquilibrium(settings.static_tolerance, settings.static_iterations);
		nodes_limb.back().torque(0);

		double pos = 0.0;
		for(auto node: nodes_limb)
		{
			pos = std::max(pos, node.x());
		}
		//std::cout << "torque = " << torque << ", pos = " << pos << "\n";
		return parameters.string_brace - pos;
	};

	double T0 = 0.0;
	double T1 = numerics::findSign<numerics::NEG>(brace_pos, 0.1, 1.5);	//Better initial estimate for the right bound would be good
	numerics::root(brace_pos, T0, T1, parameters.string_brace*1e-2, 0);

	//Set up string
	//////////////////////////////////////////////////////////////////////////////////

	//Measure string length, only non-recurve for now
	double string_length = 2.0*nodes_limb.back().y();

	double L = 0.5*string_length/settings.elements;
	double x_tip = nodes_limb.back().x();
	double y_tip = nodes_limb.back().y();
	double delta_y = y_tip/settings.elements;

	nodes_string.push_back(Node(model, x_tip, 0.0, -M_PI_2, true, false, true));

	for(std::size_t i = 0; i < settings.elements; ++i)
	{
		if(i == settings.elements - 1)
		{
			nodes_string.push_back(Node(model, nodes_limb.back(), -M_PI_2));
		}
		else
		{
			nodes_string.push_back(Node(model, x_tip, (i+1)*delta_y, -M_PI_2, true, true, true));
		}

		BeamElementCR* element = new BeamElementCR(model, nodes_string[i], nodes_string[i+1], L, parameters.string_rhoA, 1e-8, parameters.string_EA);

		elements_string.push_back(element);
		model.add(*element);
	}

	//Adjust string length to reach the desired brace height more accurately. Assumed to be increasing with element_length
	//////////////////////////////////////////////////////////////////////////////////

	auto brace_height = [&](double element_length)
	{
		for(const auto& element : elements_string)
		{
			element->length(element_length);
		}

		model.staticEquilibrium(settings.static_tolerance, settings.static_iterations);
		nodes_limb.back().torque(0);

		double pos = 0.0;
		for(auto node: nodes_limb)
		{
			pos = std::max(pos, node.x());
		}

		return parameters.string_brace - nodes_string.front().x();
	};

	double factor = 1 + 1e-1*parameters.string_brace/string_length;
	double La = L;
	double Lb = numerics::findSignChange<numerics::POS>(brace_height, La, factor);
	numerics::sort(La, Lb);
	

	//Set up string to limb contact
	//////////////////////////////////////////////////////////////////////////////////
	surface_contact = nullptr;
}

/*
void Bow::init_string_contact()
{
	//Set up string
	//////////////////////////////////////////////////////////////////////////////////

	double L = 0.5*parameters.string_length/settings.elements;

	double phi = Node::angle(nodes_limb[0], nodes_limb[1]);
	nodes_string.push_back(Node(model, limb.x()[0], limb.y()[0], phi, true, false, true));

	for(std::size_t i = 0; i < settings.elements; ++i)
	{
		phi = Node::angle(nodes_limb[i], nodes_limb[i+1]);
		//std::cout << phi << "\n";

		if(i == settings.elements - 1)
		{
			nodes_string.push_back(Node(model, nodes_limb.back(), phi));
		}
		else
		{
			nodes_string.push_back(Node(model, limb.x()[i+1], limb.y()[i+1], phi, true, true, true));
		}

		BeamElementCR* element = new BeamElementCR(model, nodes_string[i], nodes_string[i+1], L, parameters.string_rhoA, 1e-8, parameters.string_EA);

		elements_string.push_back(element);
		model.add(*element);
	}

	//Set up string to limb contact
	//////////////////////////////////////////////////////////////////////////////////

	double contact_stiffness = 100.0*parameters.string_EA/parameters.string_length;		//In terms of string stiffness
	//double contact_stiffness = 0.5*limb.EA()[0]/Node::distance(nodes_limb[0], nodes_limb[1]);	//Stiffness of first limb element

	std::vector<double> margin;
	for(std::size_t i = 0; i <= settings.elements; ++i)
	{
		//margin.push_back(0.5*parameters.string_diameter);
		//margin.push_back(0.25*(limb.height()[i] + limb.height()[i+1]) + 0.5*parameters.string_diameter);
		margin.push_back(0.5*(limb.height()[i] + parameters.string_diameter));
	}

	//surface_contact = new ContactHandler(model, nodes_limb, nodes_string, margin, contact_stiffness, true);
	surface_contact = new ContactHandler(model, nodes_string, nodes_limb, margin, contact_stiffness, false);
	model.add(*surface_contact);
}
*/

/*
void Bow::init_string_no_contact()
{
	//Prepare limb for bracing
	//////////////////////////////////////////////////////////////////////////////////

	//Returns difference between tip height and string_length/2 and its derivative w.r.t. the tip torque
	auto tipHeight = [&](double torque)
	{
		//Apply torque to the limb tip
		BeamElementCR* last = elements_limb.back();
		nodes_limb.back().torque(-torque);

		model.staticEquilibrium(1e-12, 150);
		double hgt = nodes_limb.back().y() - 0.5*parameters.string_length;
		double k = nodes_limb.back().stiffness_phi();
		
		//Undo torque
		nodes_limb.back().torque(0);
		
		return Eigen::Vector2d{hgt, -1/k};
	};

	//Find T such that the derivative tipHeight(T)(1) < 0 and the height itself tipHeight(T)(0) < 0.
	//This way the newton method in the next step converges to the right solution.
	double T = 0.1;	//Estimate based on the bow's parameters would be better
	Eigen::Vector2d hgt;
	while(true)
	{
		hgt = tipHeight(T);
		if(hgt(1) > 0 || hgt(0) > 0)
		{
			T *= 1.5;	//Better would be to reduce step size when the solver doesn't converge anymore
		}
		else
		{
			break;
		}
		//std::cout << "Initial estimate: T = " << T << "\n";
	}

	//Use Newton-method to find T such that tipHeight(T) == 0
	double epsilon = 0.01;
	do
	{
		T -= hgt(0)/hgt(1);
		hgt = tipHeight(T);
		//std::cout << "Newton iteration: T = " << T << "\n";
	}while(std::abs(hgt(0)) > epsilon);
	
	//Set up string
	//////////////////////////////////////////////////////////////////////////////////

	double L = 0.5*parameters.string_length/settings.elements;
	double x_tip = nodes_limb.back().x();
	double y_tip = nodes_limb.back().y();
	double delta_y = y_tip/settings.elements;

	nodes_string.push_back(Node(model, x_tip, 0.0, -M_PI_2, true, false, true));

	for(std::size_t i = 0; i < settings.elements; ++i)
	{
		if(i == settings.elements - 1)
		{
			nodes_string.push_back(Node(model, nodes_limb.back(), -M_PI_2));
		}
		else
		{
			nodes_string.push_back(Node(model, x_tip, (i+1)*delta_y, -M_PI_2, true, true, true));
		}

		BeamElementCR* element = new BeamElementCR(model, nodes_string[i], nodes_string[i+1], L, parameters.string_rhoA, 1e-8, parameters.string_EA);

		elements_string.push_back(element);
		model.add(*element);
	}

	//Set up string to limb contact
	//////////////////////////////////////////////////////////////////////////////////
	surface_contact = nullptr;
}
*/

Bow::~Bow()
{
	for(std::size_t i = 0; i < elements_limb.size(); ++i)
	{
		delete elements_limb[i];
	}

	for(std::size_t i = 0; i < elements_string.size(); ++i)
	{
		delete elements_string[i];
	}

	delete surface_contact;
	delete element_tip;
	delete element_string_center;
	delete element_arrow;
}

double Bow::getDrawForce() const
{
	return 2.0*nodes_string.front().force_x();	//*2 because of symmetric model
}

double Bow::getDrawForceDeriv() const
{
	return 2.0*nodes_string.front().stiffness_x();	//*2 because of symmetric model
}

void Bow::setDrawForce(double force, bool equilibrium)
{
	nodes_string.front().force_x(0.5*force);	//*0.5 because of symmetric model

	if(equilibrium)
	{
		model.staticEquilibrium(settings.static_tolerance, settings.static_iterations);
	}
}

void Bow::simulateStatics(StaticData& data, std::function<bool(unsigned int)> progress_callback)
{
	//contact_arrow->oneSided(false);
	separated = false;

	double force = 0.0;
	double draw = drawLength();
	double draw_step = parameters.string_draw/50.0;
	double epsilon = parameters.string_draw/1000.0;	//Tolerance for the final draw length

	if(parameters.string_draw <= draw)
	{
		throw std::runtime_error("Bow::simulateStatics() : Brace height is larger then the maximum draw length");
	}

	//Add initial state
	data.addBowState(*this, limb);

	unsigned int progress;
	double draw_initial = draw;

	do
	{
		force += draw_step*getDrawForceDeriv();
		setDrawForce(force, true);
		draw = drawLength();
		progress = static_cast<unsigned int>((draw - draw_initial)/(parameters.string_draw - draw_initial)*100.0);

		if(draw <= parameters.string_draw)
		{
			data.addBowState(*this, limb);
		}
		else
		{
			//Drawn too far, use newton iteration to hit the last draw length more exactly
			do
			{
				draw_step = parameters.string_draw - draw;
				force += draw_step*getDrawForceDeriv();
				setDrawForce(force, true);
				draw = drawLength();
				//std::cout << "Draw - finetuning: " << draw << "\n";

				if(!progress_callback(progress))
				{
					return;
				}
			}while(std::abs(draw_step) > epsilon);

			data.addBowState(*this, limb);
			progress_callback(100);
			return;
		}
	}while(progress_callback(progress));
}

void Bow::simulateDynamics(DynamicData& data, std::function<bool(unsigned int)> progress_callback)
{
	//contact_arrow->oneSided(true);
	separated = false;
	setDrawForce(0.0, false);

	const double final_pos = -1.0*parameters.string_draw;
	unsigned int progress = 0;

	double acc_prev = 0;
	double acc_pres = 0;

	model.integrate(settings.timestep_factor, [&]()
	{
		data.addBowState(*this);

		acc_prev = acc_pres;
		acc_pres = stringAcceleration();

		if(acc_prev < 0 && acc_pres > 0 && !separated)
		{
			separated = true;
			t_separation = time();
			s_separation = stringPosition();
			v_separation = stringVelocity();

			element_arrow->mass(0);
			model.updateMassMatrix();
		}

		progress = static_cast<unsigned int>(100.0*std::sqrt((parameters.string_draw - arrowPosition())/(parameters.string_draw - final_pos)));

		return stringForceCenter() > 0 && arrowPosition() > final_pos && progress_callback(progress);
	});

	progress_callback(100);
}

///Draw length of the bow
double Bow::drawLength() const
{
	return nodes_string.front().x();
}

void Bow::getLimbShape(std::vector<double>& u, std::vector<double>& v, std::vector<double>& epsilon, std::vector<double>& kappa) const
{
	for(std::size_t i = 0; i < elements_limb.size(); ++i)
	{
		//Data for element i
		Eigen::Vector2d def0 = elements_limb[i]->deformation(0.0);
		Eigen::Vector2d def1 = elements_limb[i]->deformation(1.0);

		//Starting point of element i
		u.push_back(nodes_limb[i].x());
		v.push_back(nodes_limb[i].y());

		if(i == 0)
		{
			epsilon.push_back(def0(0));
			kappa.push_back(def0(1));

			epsilon.push_back(def1(0));
			kappa.push_back(def1(1));
		}
		else
		{
			//Average starting point data of current element with end point data of previous one
			epsilon.back() = 0.5*(def0(0) + epsilon.back());
			kappa.back() = 0.5*(def0(1) + kappa.back());

			epsilon.push_back(def1(0));
			kappa.push_back(def1(1));
		}
	}

	//End point of last element
	u.push_back(nodes_limb.back().x());
	v.push_back(nodes_limb.back().y());
}

void Bow::getStringShape(std::vector<double>& u, std::vector<double>& v) const
{
	for(std::size_t i = 0; i < nodes_string.size(); ++i)
	{
		u.push_back(nodes_string[i].x());
		v.push_back(nodes_string[i].y());
	}
}

double Bow::stringForceCenter() const
{
	return elements_string.front()->longitudinalForce();
}

double Bow::kineticEnergy() const
{
	return 2*model.kineticEnergy();		//*2 because of symmetric model
}

double Bow::potentialEnergy() const
{
	return 2*model.potentialEnergy();	//*2 because of symmetric model
}

double Bow::arrowEnergy() const
{
	double v_arrow = arrowVelocity();
	return 0.5*parameters.mass_arrow*v_arrow*v_arrow;
}

double Bow::stringPosition() const
{
	return nodes_string.front().x();
}

double Bow::stringVelocity() const
{
	return nodes_string.front().dot_x();
}

double Bow::stringAcceleration() const
{
	return nodes_string.front().ddot_x();
}

double Bow::arrowPosition() const
{
	if(separated)
	{
		return s_separation + (time()-t_separation)*v_separation;
	}
	else
	{
		return nodes_string.front().x();
	}
}

double Bow::arrowVelocity() const
{
	if(separated)
	{
		return v_separation;
	}
	else
	{
		return nodes_string.front().dot_x();
	}
}

double Bow::arrowAcceleration() const
{
	if(separated)
	{
		return 0.0;
	}
	else
	{
		return nodes_string.front().ddot_x();
	}
}

double Bow::gripForce() const
{
	//Eigen::VectorXd u = limb_elements.front()->localCoordinates(model);
	//return 2.0*limb_elements.front()->shearForce(u);		//times 2 because of symmetric model
	return 2.0*elements_limb.front()->internalForces()(0);	//times 2 because of symmetric model
}

double Bow::time() const
{
	return model.time();
}