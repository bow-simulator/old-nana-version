// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef DYNAMIC_DATA_H
#define DYNAMIC_DATA_H

#include "Units.h"
#include "StaticData.h"

#include <vector>
#include <functional>

class Bow;
class Parameters;

class DynamicData
{
public:
	//void simulate(Bow& bow, double string_draw, std::function<bool(unsigned int)> progressCallback);
	void addBowState(const Bow& bow);

	const std::vector<double>& time() const;
	const std::vector<double>& stringPosition() const;
	const std::vector<double>& stringVelocity() const;
	const std::vector<double>& stringAcceleration() const;
	const std::vector<double>& arrowPosition() const;
	const std::vector<double>& arrowVelocity() const;
	const std::vector<double>& arrowAcceleration() const;
	const std::vector<double>& potentialEnergy() const;
	const std::vector<double>& kineticEnergy() const;
	const std::vector<double>& totalEnergy() const;
	const std::vector<double>& arrowEnergy() const;

	const std::vector<double>& stringForce() const;
	const std::vector<double>& gripForce() const;
	const std::vector<std::vector<double>>& limbShapeU() const;
	const std::vector<std::vector<double>>& limbShapeV() const;
	const std::vector<std::vector<double>>& limbStrain() const;
	const std::vector<std::vector<double>>& limbCurvature() const;
	const std::vector<std::vector<double>>& stringShapeU() const;
	const std::vector<std::vector<double>>& stringShapeV() const;

	//Specific values
	double finalArrowVelocity() const;
	double finalArrowEnergy() const;
	double efficiency(const StaticData& static_data) const;

	void save(const std::wstring& path);

private:
	std::vector<double> t;
	std::vector<double> string_position;
	std::vector<double> string_velocity;
	std::vector<double> string_acceleration;
	std::vector<double> arrow_position;
	std::vector<double> arrow_velocity;
	std::vector<double> arrow_acceleration;
	std::vector<double> potential_energy;
	std::vector<double> kinetic_energy;
	std::vector<double> total_energy;
	std::vector<double> arrow_energy;

	std::vector<double> string_force;
	std::vector<double> grip_force;
	std::vector<std::vector<double>> limb_shape_u;
	std::vector<std::vector<double>> limb_shape_v;
	std::vector<std::vector<double>> limb_epsilon;
	std::vector<std::vector<double>> limb_kappa;
	std::vector<std::vector<double>> string_shape_u;
	std::vector<std::vector<double>> string_shape_v;
};

#endif // DYNAMIC_DATA_H