// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef UNITS_H
#define UNITS_H

#include <string>
#include <vector>
#include <string>

class Unit
{
public:
	Unit(std::wstring label, double factor)
		: label_(label), factor_(factor)
	{

	}

	std::wstring label() const
	{
		return label_;
	}

	double to_base(double value) const
	{
		return value*factor_;
	}

	std::vector<double>& to_base(std::vector<double>& vec) const
	{
		for(std::size_t i = 0; i < vec.size(); ++i)
		{
			vec[i] *= factor_;
		}

		return vec;
	}

	double to_base() const
	{
		return factor_;
	}

	double from_base(double value) const
	{
		return value/factor_;
	}

	std::vector<double>& from_base(std::vector<double>& vec) const
	{
		for(std::size_t i = 0; i < vec.size(); ++i)
		{
			vec[i] /= factor_;
		}

		return vec;
	}

	double from_base() const
	{
		return 1.0/factor_;
	}

private:
	std::wstring label_;
	double factor_;

};

///base_value = input_value*unit
namespace units
{
	const Unit one(L"[1]", 1.0);
	const Unit percent(L"[%]", 1.0e-2);
	const Unit length(L"[mm]", 1.0e-3);
	const Unit angle(L"[�]", M_PI/180.0);
	const Unit mass(L"[g]", 1.0e-3);
	const Unit time(L"[s]", 1.0);
	const Unit velocity(L"[m/s]", 1.0);
	const Unit acceleration(L"[m/s�]", 1.0);
	const Unit force(L"[N]", 1.0);
	const Unit stress(L"[N/mm�]", 1.0e6);
	const Unit e_modulus(L"[N/mm�]", 1.0e6);
	const Unit strain(L"[%]", 1.0e-2);
	const Unit curvature(L"[1/mm]", 1.0e3);
	const Unit energy(L"[J]", 1.0);
	const Unit vol_density(L"[kg/m�]", 1.0);
	const Unit lin_density(L"[kg/m]", 1.0);
	const Unit stiffness(L"[N/mm]", 1.0e3);
	const Unit ltd_stiffness(L"[N]", 1.0);
	const Unit bd_stiffness(L"[Nmm�]", 1.0e-6);
};

#endif // UNITS_H

/*
template<double factor_>
class Unit
{
public:
	Unit(std::wstring label): label_(label)
	{

	}


	std::wstring label() const
	{
		return label_;
	}


	double to_base(double value) const
	{
		return value*factor_;
	}


	double to_base() const
	{
		return factor_;
	}


	double from_base(double value) const
	{
		return value/factor_;
	}


	double from_base() const
	{
		return 1.0/factor_;
	}
	
	std::wstring label_;

};

///base_value = input_value*unit
namespace units
{

	const Unit<1.0> one(L"[1]");
	const Unit<1.0e-2> percent(L"[%]");
	const Unit<1.0e-3> length(L"[mm]");
	const Unit<1.0e-3> mass(L"[g]");
	const Unit<1.0> time(L"[s]");
	const Unit<1.0> velocity(L"[m/s]");
	const Unit<1.0> acceleration(L"[m/s�]");
	const Unit<1.0> force(L"[N]");
	const Unit<1.0e6> stress(L"[N/mm�]");
	const Unit<1.0e6> e_modulus(L"[N/mm�]");
	const Unit<1.0e-2> strain(L"[%]");
	const Unit<1.0e3> curvature(L"[1/mm]");
	const Unit<1.0> energy(L"[J]");
	const Unit<1.0> vol_density(L"[kg/m�]");
	const Unit<1.0> lin_density(L"[kg/m]");
	const Unit<1.0e3> stiffness(L"[N/mm]");
	const Unit<1.0> ltd_stiffness(L"[N]");
	const Unit<1.0e-6> bd_stiffness(L"[Nmm�]");

};

#endif // UNITS_H
*/