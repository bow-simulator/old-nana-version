// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "LimbGeometry.h"

///\todo Remove code duplication after end of loop
LimbGeometry::LimbGeometry(const BowParameters& parameters, std::size_t elements_limb)
	: parameters(parameters)
{
	//Assure that the first control point is at the origin
	if(parameters.x.at(0) != 0 || parameters.y.at(0) != 0)
	{
		throw std::runtime_error("LimbGeometry::LimbGeometry() : Limb centerline must start at the origin (x = 0, y = 0)");
	}

	SplineSegment centerline(parameters.x, parameters.y);
	Rectification rect(centerline, 0.001);	//Get rekt!

	//Total centerline arc length
	double L = rect.length(0.0, parameters.pos.back());

	std::vector<double> arc_length_sections;
	for(std::size_t i = 0; i < parameters.pos.size(); ++i)
	{
		arc_length_sections.push_back(parameters.pos[i]*L);
	}

	Spline spline_width(arc_length_sections, parameters.width);
	Spline spline_height(arc_length_sections, parameters.height);

	//Divide the total arc length of the centerline into elements_limb equal parts of length delta_l
	double delta_L = L/elements_limb;
	
	double start = 0.0;
	double end;

	Eigen::Vector2d r_start = centerline.value(start);
	Eigen::Vector2d r_end;

	for(int i = 0; i < elements_limb; ++i)
	{
		end = rect.endParameter(start, delta_L);
		r_end = centerline.value(end);

		//std::cout << r_end << "length: " << centerline.getLength(start, end);

		_x.push_back(r_start(0));
		_y.push_back(r_start(1));
		_s.push_back(i*delta_L);
		_width.push_back(spline_width.value(i*delta_L));
		_height.push_back(spline_height.value(i*delta_L));
		
		//Advance section
		start = end;
		r_start = r_end;
	}

	_x.push_back(r_start(0));
	_y.push_back(r_start(1));
	_s.push_back(elements_limb*delta_L);
	_width.push_back(spline_width.value(elements_limb*delta_L));
	_height.push_back(spline_height.value(elements_limb*delta_L));


	//Calculate rhoA, EI, EA (Would be more elegant in loop above)
	for(std::size_t i = 0; i < _s.size(); ++i)
	{
		_rhoA.push_back(parameters.material_rho*_width[i]*_height[i]);
		_EI.push_back(parameters.material_E*_width[i]*_height[i]*_height[i]*_height[i]/12.0);
		_EA.push_back(parameters.material_E*_width[i]*_height[i]);
	}
}

std::vector<double> LimbGeometry::stress(const std::vector<double>& strain, const std::vector<double>& curvature, double eta) const
{
	std::vector<double> sigma;

	for(std::size_t i = 0; i < _s.size(); ++i)
	{
		double epsilon = strain[i] - eta*_height[i]/2.0*curvature[i];
		sigma.push_back(parameters.material_E*epsilon);
	}

	return sigma;
}

const std::vector<double>& LimbGeometry::x() const
{
	return _x;
}

const std::vector<double>& LimbGeometry::y() const
{
	return _y;
}

const std::vector<double>& LimbGeometry::arcLength() const
{
	return _s;
}

const std::vector<double>& LimbGeometry::width() const
{
	return _width;
}

const std::vector<double>& LimbGeometry::height() const
{
	return _height;
}

const std::vector<double>& LimbGeometry::rhoA() const
{
	return _rhoA;
}

const std::vector<double>& LimbGeometry::EI() const
{
	return _EI;
}

const std::vector<double>& LimbGeometry::EA() const
{
	return _EA;
}