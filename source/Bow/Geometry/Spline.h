// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef SPLINE_H
#define SPLINE_H

#include "tk_spline.h"

#include <vector>
#include <iostream>
#include <cmath>

#include <Eigen/Core>

/**
Cubic natural spline according to mathworld.wolfram.com/CubicSpline.html
\todo Overall Implementation not very pretty.
\todo When less than 4 control points are used, there are additional points being inserted.
A better way would be to adapt the calculation of the coefficients to lower numbers of points.
\todo look at java code here: http://www.serc.iisc.ernet.in/~amohanty/SE288/lagrange/spline.html
*/
class Spline
{

public:
	Spline(std::vector<double> values);
	Spline(std::vector<double> args, std::vector<double> values);

	double value(double x) const;
	double derivative(double x) const;

private:
	tk::spline s;

};

#endif // SPLINE_H
