// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "Rectification.h"

void LookupTable::add(double arg_value, double fcn_value)
{
	arg_vector.push_back(arg_value);
	fcn_vector.push_back(fcn_value);
}

double LookupTable::fcn_value(double arg_value) const
{
	return interpolate(arg_vector, fcn_vector, arg_value);
}

double LookupTable::arg_value(double fcn_value) const
{
	return interpolate(fcn_vector, arg_vector, fcn_value);
}

//Bisection to find interval, then linear interpolation
double LookupTable::interpolate(const std::vector<double>& x, const std::vector<double>& y, double x_value) const
{
	std::size_t lower = 0;
	std::size_t upper = x.size() - 1;

	while(upper - lower > 1)
	{
		std::size_t middle = (lower + upper)/2;
		if(x[middle] < x_value)
		{
			lower = middle;
		}
		else if(x[middle] >= x_value)
		{
			upper = middle;
		}
	}

	return y[lower] + (y[upper] - y[lower])/(x[upper] - x[lower])*(x_value - x[lower]);
}

Rectification::Rectification(const Curve& curve, double step)
{
	//Calculate relationship between parameter and curve length at discrete points and put it into a lookup table.
	//Integration with Simpson-method.
	double t = 0.0;
	double L = 0.0;

	while(t < 1.0)
	{
		lookup.add(t, L);
		L += step/6.0*(curve.derivative(t).norm() + 4.0*(curve.derivative((2*t + step)/2.0).norm()) + curve.derivative(t + step).norm());
		t += step;
	}

	lookup.add(t, L);
}

double Rectification::length(double t_start, double t_end) const
{
	return lookup.fcn_value(t_end) - lookup.fcn_value(t_start);
}

double Rectification::endParameter(double t_start, double L) const
{
	return lookup.arg_value(L + lookup.fcn_value(t_start));
}