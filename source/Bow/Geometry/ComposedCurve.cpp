// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "ComposedCurve.h"

TransformedCurve::TransformedCurve(const Curve& curve)
	: curve(curve), T(Eigen::Matrix2d::Identity()), r(Eigen::Vector2d::Zero())
{

}

void TransformedCurve::rotation(double value)
{
	double cp = std::cos(value);
	double sp = std::sin(value);
	T << cp, -sp, sp, cp;
}

void TransformedCurve::offset(const Eigen::Vector2d& value)
{
	r = value;
}

Eigen::Vector2d TransformedCurve::value(double t) const
{
	return T*curve.value(t) + r;
}

Eigen::Vector2d TransformedCurve::derivative(double t) const
{
	return T*curve.derivative(t);
}

void ComposedCurve::addSegment(const Curve& seg)
{
	TransformedCurve* curve = new TransformedCurve(seg);

	if(segments.empty())
	{
		curve->offset(-curve->value(0));
	}
	else
	{
		curve->offset(segments.back()->value(1) - curve->value(0));
	}

	segments.push_back(curve);
}

void ComposedCurve::addSegment(const Curve& seg, double angle)
{
	TransformedCurve* curve = new TransformedCurve(seg);

	if(segments.empty())
	{
		curve->rotation(angle);
		curve->offset(-curve->value(0));
	}
	else
	{
		curve->rotation(segments.back()->orientation(1) - seg.orientation(0) + angle);
		curve->offset(segments.back()->value(1) - curve->value(0));
	}

	segments.push_back(curve);
}

void ComposedCurve::getNodes(std::vector<double>& x, std::vector<double>& y)
{
	for(std::size_t i = 0; i < segments.size(); ++i)
	{
		Eigen::Vector2d r = segments[i]->value(0);
		x.push_back(r(0));
		y.push_back(r(1));
	}

	Eigen::Vector2d r = segments.back()->value(1);
	x.push_back(r(0));
	y.push_back(r(1));
}

Eigen::Vector2d ComposedCurve::value(double t) const
{
	t *= segments.size();
	std::size_t i = std::min(static_cast<std::size_t>(t), segments.size() - 1);
	return segments[i]->value(t - i);

}

Eigen::Vector2d ComposedCurve::derivative(double t) const
{
	t *= segments.size();
	std::size_t i = std::min(static_cast<std::size_t>(t), segments.size() - 1);
	return segments[i]->derivative(t - i);
}