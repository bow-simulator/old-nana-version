// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef CURVE_H
#define CURVE_H

#include "Spline.h"

#include <Eigen/Core>

#include <vector>
#include <cmath>
#include <algorithm>

class Curve
{
public:
	virtual Eigen::Vector2d value(double t) const = 0;
	virtual Eigen::Vector2d derivative(double t) const = 0;
	double orientation(double t) const;
};

class ArcSegment: public Curve
{
public:
	ArcSegment(double radius, double angle);
	virtual Eigen::Vector2d value(double t) const override;
	virtual Eigen::Vector2d derivative(double t) const override;
	
private:
	double radius;
	double angle;
};

class LineSegment: public Curve
{
public:
	LineSegment(double length);
	virtual Eigen::Vector2d value(double t) const override;
	virtual Eigen::Vector2d derivative(double t) const override;

private:
	double length;
};

class SplineSegment: public Curve
{
public:
	SplineSegment(const std::vector<double>& x, const std::vector<double>& y);
	virtual Eigen::Vector2d value(double t) const override;
	virtual Eigen::Vector2d derivative(double t) const override;

private:
	Spline spline_x;
	Spline spline_y;
};

#endif // CURVE_H