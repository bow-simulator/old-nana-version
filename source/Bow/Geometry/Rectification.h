// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef RECTIFICATION_H
#define RECTIFICATION_H

#include "Curve.h"

class LookupTable
{
public:
	void add(double arg_value, double fcn_value);
	double fcn_value(double arg_value) const;
	double arg_value(double fcn_value) const;

private:
	std::vector<double> arg_vector;
	std::vector<double> fcn_vector;

	double interpolate(const std::vector<double>& x, const std::vector<double>& y, double x_value) const;
};

class Rectification
{
public:
	Rectification(const Curve& curve, double step);

	double Rectification::length(double t_start, double t_end) const;
	double Rectification::endParameter(double t_start, double L) const;

private:
	LookupTable lookup;

};

#endif // RECTIFICATION_H
