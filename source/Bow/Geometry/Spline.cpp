// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "Spline.h"

Spline::Spline(std::vector<double> values)
{
	std::vector<double> args;
	std::size_t n = values.size();

	for(std::size_t i = 0; i < n; ++i)
	{
		args.push_back(double(i)*1.0/double(n-1));
	}

	s.set_points(args, values);
}

Spline::Spline(std::vector<double> args, std::vector<double> values)
{
	s.set_points(args, values);
}

double Spline::value(double x) const
{
	return s(x);
}

double Spline::derivative(double x) const
{
	return s.derivative(x);
}