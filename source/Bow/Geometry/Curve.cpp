// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "Curve.h"

double Curve::orientation(double t) const
{
	Eigen::Vector2d tangent = derivative(t);
	return std::atan2(tangent(1), tangent(0));
}

ArcSegment::ArcSegment(double radius, double angle)
	: radius(radius),
	  angle( (radius >= 0)? std::abs(angle) : -std::abs(angle))	//Ensure angle has the same sign as radius
{

}

Eigen::Vector2d ArcSegment::value(double t) const
{
	return Eigen::Vector2d{radius*std::sin(angle*t), radius*std::cos(angle*t) - radius};
}

Eigen::Vector2d ArcSegment::derivative(double t) const
{
	return Eigen::Vector2d{radius*angle*std::cos(angle*t), -radius*angle*std::sin(angle*t)};
}

LineSegment::LineSegment(double length): length(length)
{

}

Eigen::Vector2d LineSegment::value(double t) const
{
	return Eigen::Vector2d{length*t, 0};
}

Eigen::Vector2d LineSegment::derivative(double t) const
{
	return Eigen::Vector2d{length, 0};
}

SplineSegment::SplineSegment(const std::vector<double>& x, const std::vector<double>& y)
	: spline_x(x), spline_y(y)
{

}

Eigen::Vector2d SplineSegment::value(double t) const
{
	return Eigen::Vector2d{spline_x.value(t), spline_y.value(t)};
}

Eigen::Vector2d SplineSegment::derivative(double t) const
{
	return Eigen::Vector2d{spline_x.derivative(t), spline_y.derivative(t)};
}