// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef COMPOSED_CURVE_H
#define COMPOSED_CURVE_H

#include "Curve.h"

#include <Eigen/Core>

#include <vector>
#include <cmath>
#include <algorithm>

class TransformedCurve: public Curve
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	TransformedCurve(const Curve& curve);

	void rotation(double value);
	void offset(const Eigen::Vector2d& value);

	virtual Eigen::Vector2d value(double t) const override;
	virtual Eigen::Vector2d derivative(double t) const override;
	
private:
	const Curve& curve;
	Eigen::Matrix2d T;
	Eigen::Vector2d r;
};

class ComposedCurve: public Curve
{
public:
	void addSegment(const Curve& seg);
	void addSegment(const Curve& seg, double angle);

	void getNodes(std::vector<double>& x, std::vector<double>& y);
	
	virtual Eigen::Vector2d value(double t) const override;
	virtual Eigen::Vector2d derivative(double t) const override;

private:
	std::vector<Curve*> segments;
};

#endif // COMPOSED_CURVE_H