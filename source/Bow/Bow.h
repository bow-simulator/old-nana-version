// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef BOW_H
#define BOW_H

#include "Parameters.h"
#include "StaticData.h"
#include "DynamicData.h"
#include "../FEM/Model.h"
#include "../FEM/Elements/BeamElementCR.h"
#include "../FEM/Elements/MassElement.h"
#include "../FEM/Elements/ContactElement1D.h"
#include "../FEM/ContactHandler.h"
#include "../FEM/Numerics.h"
#include "Units.h"

#include <vector>
#include <iostream>
#include <cmath>
#include <functional>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <memory>

class Bow
{
public:
	Bow(const LimbGeometry& limb, const BowParameters& parameters, const BowSettings& settings);	
	~Bow();

	void simulateStatics(StaticData& data, std::function<bool(unsigned int)> progress_callback);
	void simulateDynamics(DynamicData& data, std::function<bool(unsigned int)> progress_callback);

	double getDrawForce() const;
	double getDrawForceDeriv() const;	//< Derivative of the draw force w.r.t. the draw length
	double drawLength() const;
	///Get the limb node coordinates u, v and the elements strains epsilon and curvatures kappa
	void getLimbShape(std::vector<double>& u, std::vector<double>& v, std::vector<double>& epsilon, std::vector<double>& kappa) const;
	void getStringShape(std::vector<double>& u, std::vector<double>& v) const;
	double stringForceCenter() const;
	double kineticEnergy() const;
	double potentialEnergy() const;
	double arrowEnergy() const;
	double stringPosition() const;
	double stringVelocity() const;
	double stringAcceleration() const;
	double arrowPosition() const;
	double arrowVelocity() const;
	double arrowAcceleration() const;
	double gripForce() const;
	double time() const;

private:
	const LimbGeometry& limb;
	const BowParameters& parameters;
	const BowSettings& settings;
	Model model;
	
	std::vector<Node> nodes_limb;
	std::vector<Node> nodes_string;

	std::vector<BeamElementCR*> elements_limb;
	std::vector<BeamElementCR*> elements_string;
	ContactHandler* surface_contact;
	MassElement* element_tip;
	MassElement* element_string_center;
	MassElement* element_arrow;

	bool separated;
	double t_separation;
	double s_separation;
	double v_separation;

	void init_limb();
	//void init_string_contact();
	//void init_string_no_contact();
	void init_string_brace_height();


	void setDrawForce(double force, bool equilibrium);
};

#endif // BOW_H