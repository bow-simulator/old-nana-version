// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef STATIC_DATA_H
#define STATIC_DATA_H

#include "Units.h"

#include <vector>
#include <functional>

#include <jsoncons/json.hpp>

class Bow;
class LimbGeometry;

class StaticData
{
public:
	//void simulate(Bow& bow, const LimbGeometry& geometry, double string_draw, std::function<bool(unsigned int)> progressCallback);
	void addBowState(const Bow& bow, const LimbGeometry& geometry);

	const std::vector<double>& drawLength() const;
	const std::vector<double>& drawForce() const;
	const std::vector<double>& potentialEnergy() const;
	const std::vector<double>& stringForce() const;
	const std::vector<std::vector<double>>& limbShapeX() const;
	const std::vector<std::vector<double>>& limbShapeY() const;
	const std::vector<std::vector<double>>& limbStrain() const;
	const std::vector<std::vector<double>>& limbCurvature() const;
	const std::vector<std::vector<double>>& stressBack() const;
	const std::vector<std::vector<double>>& stressBelly() const;
	const std::vector<std::vector<double>>& stringShapeX() const;
	const std::vector<std::vector<double>>& stringShapeY() const;

	//Specific values
	double finalDrawForce() const;
	double stringLength() const;
	double energyBraced() const;
	double energyDrawn() const;
	double drawingWork() const;
	double storageRatio() const;
	double maxStress() const;

	void save(const std::wstring& path);

private:
	std::vector<double> draw_length;
	std::vector<double> draw_force;
	std::vector<double> potential_energy;
	std::vector<double> string_force;
	std::vector<std::vector<double>> limb_shape_x;
	std::vector<std::vector<double>> limb_shape_v;
	std::vector<std::vector<double>> limb_epsilon;
	std::vector<std::vector<double>> limb_kappa;
	std::vector<std::vector<double>> stress_back;
	std::vector<std::vector<double>> stress_belly;
	std::vector<std::vector<double>> string_shape_x;
	std::vector<std::vector<double>> string_shape_y;
};

#endif // STATIC_DATA_H