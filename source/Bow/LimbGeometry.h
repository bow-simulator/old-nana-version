// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef LIMB_GEOMETRY_H
#define LIMB_GEOMETRY_H

#include <vector>

#include "Bow.h"
#include "Geometry/Spline.h"
#include "Geometry/Curve.h"
#include "Geometry/ComposedCurve.h"
#include "Geometry/Rectification.h"

class LimbGeometry
{
public:
	LimbGeometry(const BowParameters& parameters, std::size_t elements_limb);

	///eta: Cross section parameter element [-1, 1]. eta = 1: Back, eta = 0: Centerline, eta = -1: Belly
	std::vector<double> stress(const std::vector<double>& strain, const std::vector<double>& curvature, double eta) const;

	const std::vector<double>& x() const;
	const std::vector<double>& y() const;
	const std::vector<double>& arcLength() const;
	const std::vector<double>& width() const;
	const std::vector<double>& height() const;
	const std::vector<double>& rhoA() const;
	const std::vector<double>& EI() const;
	const std::vector<double>& EA() const;

private:
	std::vector<double> _x;
	std::vector<double> _y;
	std::vector<double> _s;
	std::vector<double> _width;	///< width
	std::vector<double> _height;	///< height
	
	std::vector<double> _rhoA;
	std::vector<double> _EI;
	std::vector<double> _EA;

	const BowParameters& parameters;
};

#endif // LIMB_GEOMETRY_H