// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#ifndef PARAMETERS_H
#define PARAMETERS_H

#include "Units.h"

#include <jsoncons/json.hpp>

#include <string>

class BowParameters
{
public:
	BowParameters(const jsoncons::wjson& obj)
	{
		jsoncons::wjson centerline = obj[L"parameters"][L"centerline"];
		x = units::length.to_base(centerline[L"x"].as<std::vector<double>>());
		y = units::length.to_base(centerline[L"y"].as<std::vector<double>>());

		jsoncons::wjson sections = obj[L"parameters"][L"sections"];
		pos = units::percent.to_base(sections[L"pos"].as<std::vector<double>>());
		width = units::length.to_base(sections[L"width"].as<std::vector<double>>());
		height = units::length.to_base(sections[L"height"].as<std::vector<double>>());
		
		jsoncons::wjson material = obj[L"parameters"][L"material"];
		material_E = units::e_modulus.to_base(material[L"E"].as<double>());
		material_rho = units::vol_density.to_base(material[L"rho"].as<double>());
	
		jsoncons::wjson string = obj[L"parameters"][L"string"];
		string_rhoA = units::lin_density.to_base(string[L"rhoA"].as<double>());
		string_EA = units::ltd_stiffness.to_base(string[L"EA"].as<double>());
		string_diameter = units::length.to_base(string[L"diameter"].as<double>());
		string_brace = units::length.to_base(string[L"brace_height"].as<double>());
		string_draw = units::length.to_base(string[L"draw_length"].as<double>());
	
		jsoncons::wjson masses = obj[L"parameters"][L"masses"];
		mass_arrow = units::mass.to_base(masses[L"arrow"].as<double>());
		mass_limb_tip = units::mass.to_base(masses[L"limb_tip"].as<double>());
		mass_serving = units::mass.to_base(masses[L"serving"].as<double>());
	}

	std::vector<double> x;
	std::vector<double> y;
	std::vector<double> pos;
	std::vector<double> width;
	std::vector<double> height;

	double material_E;
	double material_rho;
	
	double string_rhoA;
	double string_EA;
	double string_diameter;
	double string_brace;
	double string_draw;

	double mass_arrow;
	double mass_limb_tip;
	double mass_serving;
};

class BowSettings
{
public:
	BowSettings(const jsoncons::wjson& obj)
	{
		jsoncons::wjson settings = obj[L"settings"];
		elements = settings[L"elements"].as<std::size_t>();
		contact_handling = settings[L"contact_handling"].as<bool>();
		timestep_factor = settings[L"timestep_factor"].as<double>();

		static_tolerance = 1e-12;
		static_iterations = 150;
	}

	std::size_t elements;
	bool contact_handling;
	double timestep_factor;

	double static_tolerance;
	unsigned int static_iterations; 

};

namespace parameters
{
	const std::wstring default_json_string =
	L"{"
	L"\"comments\":\"\","
	L"\"parameters\":"
	L"{"
	L"\"centerline\":{\"x\":[],\"y\":[]},"
	L"\"masses\":"
	L"{"
	L"\"arrow\":0,"
	L"\"limb_tip\":0,"
	L"\"serving\":0"
	L"},"
	L"\"material\":"
	L"{"
	L"\"E\":0.81,"
	L"\"rho\":0"
	L"},"
	L"\"sections\":{\"pos\":[],\"width\":[],\"height\":[]},"
	L"\"string\":"
	L"{"
	L"\"EA\":0,"
	L"\"diameter\":0,"
	L"\"draw_length\":0,"
	L"\"brace_height\":0,"
	L"\"rhoA\":0"
	L"}"
	L"},"
	L"\"settings\":"
	L"{"
	L"\"contact_handling\":false,"
	L"\"elements\":25,"
	L"\"timestep_factor\":0.1"
	L"}"
	L"}";
}

#endif // PARAMETERS_H