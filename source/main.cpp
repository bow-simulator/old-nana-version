// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2014 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/Licenses/gpl-3.0.txt)

#include "GUI/Application.h"

#include <tclap/CmdLine.h>

int main(int argc, char* argv[])
{
	try
	{
		TCLAP::UnlabeledValueArg<std::string> input("input", "Input file", false, "", "string");
		TCLAP::ValueArg<std::string> output_s("s", "static", "Output file for statics", false, "", "string");
		TCLAP::ValueArg<std::string> output_d("d", "dynamic", "Output file for dynamics", false, "", "string");

		TCLAP::CmdLine cmd("Specifying output files will prevent the GUI from opening.", ' ', Application::version);
		cmd.add(input);
		cmd.add(output_s);
		cmd.add(output_d);
		cmd.parse(argc, argv);

		if(!output_s.isSet() && !output_d.isSet())
		{
			//Start GUI, load file if specified
			if(input.isSet())
			{
				const std::string& path = input.getValue();
				Application app(std::wstring(path.begin(), path.end()));
				nana::exec();
			}
			else
			{
				Application app;
				nana::exec();
			}
		}
		else
		{
			//Run simulation without GUI
			const std::string& path_in = input.getValue();
			const std::string& path_out_s = output_s.getValue();
			const std::string& path_out_d = output_d.getValue();
			Application::cmdSimulationRun(std::wstring(path_in.begin(), path_in.end()),
										  std::wstring(path_out_s.begin(), path_out_s.end()),
										  std::wstring(path_out_d.begin(), path_out_d.end()));
		}
	}
	catch(TCLAP::ArgException &e)
	{
		std::cerr << "Error: " << e.error() << " for arg " << e.argId() << std::endl;
	}

	return 0;
}



/**
\mainpage Documentation for %Bow Simulation Tool
%Bow Simulation Tool is an open source program for analysing the static and dynamic performance of bow designs.\n
The source code is organised into three main parts:

- FEM: A basic set of classes and functions for assembling and solving mechanical finite element systems
- %Bow: Uses the FEM part to provide a simulation model for bows
- GUI: A graphical interface to the bow model so that users can work with it, e.g. put in their parameters, load and save files, run simulations, etc.

Following third party libraries are used:\n
- Nana (graphical user interface, version 1.0.1), http://www.nanapro.org/
- Eigen (linear algebra, version 3.2.1), http://eigen.tuxfamily.org/
- Jsoncons (parsing json files, version 0.93), https://sourceforge.net/projects/jsoncons/
- Tclap (Parsing command line arguments, version 1.2.1), http://tclap.sourceforge.net/

\section G Notes on possible improvements

\subsection B GUI

- Preview plot: Show actual profiles in top and side view
- Ask user if he wants to override file when an already existing file is chosen in save_as
- Test validity of a file before beginning to fill the textboxes. This way clear() doesn't have to be called when aborting the loading process.
- Test numeric inputs for validity with respect to syntax and domain. For example see wxWidgets wxFloatingPointValidator. Change comma to point during input.
- InputPanel: Load and save: Omit empty textfields.
- What is the standard behaviour when the program is called from the command line with invalid file? Should the window open? Should it stay open? Should a message be printed on the console?
- Problem when Application::filepath is set with the relative command line argument: save as doesn't work anymore due to filebox::init_file(...)
- Preview plots: Show the control points and their arc length in all of them.


\subsection E FEM

- Test different sparse matrix solvers (SimplicialLDLT, Cholesky?, ...)
- Write tests for all FEM classes. Make an own project for the tests.
- Calculate the analytic jacobian matrixs for the co-rotational beam element and verify it via tests.
- Use a profiler to see where the bottleneck actually is.
- Use relative errors whenever possible, avoid magic numbers as error limits, expose important ones to the user.
- What prevents models with high number of elements from converging in static analysis?


\subsection F Bow

- Tracking the draw curve: Higher order interpolation? Or generally a more elaborate tracking algorithm? Displacement control?
- Bow static analysis: Don't abort when no equilibrium was found. Try to reduce the load step first.
- Write own spline class, not urgent, perhaps wait until Eigen provides band matrices


\subsection Ideas for New features

\subsubsection C Middle-term

- Improve reliability of the bracing method when using contact handling
- String bridges
- Laminated bows
- More analysis options
- Python interface for automated variant calculations and optimisation

\subsubsection D Long-term improvements/Ideas

- Arc elemnts would allow easy collision detection and better approximation of curved bows
- 'Mesh' generation in a way that sections of the bow with higher curvature are divided into more elements.
- Residual stress?
- Asymmetric bows?
- Think about storing simulation results. Maybe a new section in the json file if memory is not a problem.
- Possibility to compare different designs
- Compound bows, requires displacement control, https://sites.google.com/site/technicalarchery/technical-discussions-1/gear-head (A Gear Head's Analysis of Compound Bow Function)
- String stoppers?

\subsection F Short-term to do list

- Plot maximum limb stress over draw length
- Step size reduction when static calculation doesn't converge
- Find a way to treat numerical settings without magic numbers
- Enable shortkeys, see nanapro.org forum
- Plot dynamic data also over travel instead of time
- Bow: Alternative to own heap allocation of the elements
- Dynamic energy plot: Pot. bow, pot. bow + kin. bow, pot. bow + kin. bow + kin. arrow
- Program icon (for exe file and application windows, see http://stackoverflow.com/questions/13248465/setting-executable-icon-in-visual-c-2010-express)
- Implement update method in elements when there can be done some common calculations
- Implement output of the total simulation time
- Release force for the string-arrow-contact?
- OutputForm should be independent from LimbGeometry object
- Correct frequency for beam with lumped mass matrix
- Try energy convergence criterion for newton method
- Calculate total limb mass and virtual mass?
- Enable limb preview without cross-sections already defined
- Plot: Option to scale axis such that plots are not distorted
- ProgressForm is not in middle of the parent window
- Enable limb not starting at the origin (Inputs currently shifted to origin)
- Look at Nana filesystem utilities
- Is the string movement plausible? https://www.youtube.com/watch?v=GaXWK083TtA, https://www.youtube.com/watch?v=znqBSsukLXs
- PanelSettings: checkbox for contact handling is not included in edited()
- Rename height to thickness? (Maybe for laminations)
- Look at Noh-Bathe-algorithm (An explicit time integration scheme for the analysis of wave propagations)
- Create a "string tip element", a beam element that connests to the limb tip but has an offset
- Use curve editor for centerline input. Still to solve: Starting point != (0, 0)
- Runtime fail in debug when output form is getting closed
- Look at todo annotations
- Why does the icon on the console window look better than that on the other windows?
- Use more assertions

- Maybe rename timestep factor to timestep reduction
- Find soultion to the precision problem when loading doubles, remove default parameter 0.81 when finished
- Change unit of linear density to g/m
- Static progress > 100% ?
- String specification: Strand properties + number of strands?
- Think about replacing the parameters for the bow with json objects
- Specify brace height instead of string length and have the bow model figure it out itself
- Eigen Parallelisation, http://eigen.tuxfamily.org/dox/TopicMultiThreading.html
- Secant method, regula falsi, brent's method?
- Get rid of assertions in release mode
- Implement StaticData::stringLength()
*/

/*
Nana remarks:
- Textbox: When something at the end of a string that is too long to be displayed at once is deleted it doesn't fit in automatically.
- Sometimes wrong cursos when hovering over menubar
*/

/*
Keep the following visual studio options in mind:

//Console window:
Linker - System - Subsystem: Windows
Linker - All Options - Additional Options: /ENTRY:wmainCRTStartup

//Command line arguments when running from visual studio:
Configuration Properties - Debugging - Command Arguments

//Compatibility to Windows XP:
Configuration Properties - General - Platform Toolset - Visual Studio 2013 - Windows XP (v120_xp)

//Preprocessor definitions
_CRT_SECURE_NO_WARNINGS for sprintf in Table.cpp and other stuff
_USE_MATH_DEFINES for mathematical constants in cmath
*/



/*
Plot libraries:
Giza, https://sourceforge.net/projects/giza/?source=navbar
PLPlot
DYSLIN
Matplotpp
Portable Plot, https://sourceforge.net/projects/pplot/?source=directory
*/



/*
FEM libraries
> Getfem++ (http://download.gna.org/getfem/html/homepage/index.html#)
	+ Nice style
	+ Contact
	- Lack of structural elements

> Vega (http://run.usc.edu/vega/)
	+ Several nonlinear methods
	+ Different solvers included
	- No Contact handling
	- C-style

> OOFEM
	- Not a library but a commandline tool

> Feel++
	- Too abstract

> FETK (http://www.fetk.org/)


> TOCHNOG (http://tochnog.sourceforge.net/)
	- Apparently not a library

> Frame3dd
	- Not a library
	- C

> deal.II

Reading about FEM implementations:
	http://link.springer.com/chapter/10.1007%2F978-1-4020-6264-3_77#page-1
	http://www.engissol.com/analysis-libraries.html
	Implementation details for IMPACT: http://www.impact-fem.org/dev_programmer_en
	LS Dyna theory manual
*/