#include "../GUI/XYPlot/XYPlot.h"

#include <nana/gui.hpp>
#include <nana/gui/place.hpp>

int wmain()
{
    using namespace nana;

    form fm(nana::rectangle(100, 100, 1000, 600));

	std::vector<double> x = {0.1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9.72};
	std::vector<double> y1 = {0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100};
	std::vector<double> y2 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	XYPlot plot(fm);
	plot.title(L"Title string");
	plot.getXAxis().label(L"x-Axis");
	plot.getYAxis().label(L"y-Axis");
	plot.getXAxis().scale(1.0);
	plot.getYAxis().scale(1.0);
	plot.addPlotData(PlotData(x, y1, LineFormat(LineStyle::NONE), MarkerFormat(MarkerStyle::SQUARE, 0xB7102F)));
	plot.addPlotData(PlotData(x, y2, LineFormat(LineStyle::SOLID, 0x00008B), MarkerFormat(MarkerStyle::PLUS)));
	plot.grid(true);
	plot.fit125();

	place plc(fm);
	plc.div("<plot>");
	plc.field("plot") << plot;
	plc.collocate();

	fm.show();
	exec();
}

/*
#include "../GUI/XYPlot/XYPlot.h"

#include <nana/gui/wvl.hpp>
#include <nana/gui/place.hpp>


int wmain()
{

	//std::cout << RoundTo125(3.1415926535);

    using namespace nana::gui;

    form fm(nana::rectangle(100, 100, 1000, 600));

	std::vector<double> x = {0.1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 9.72};
	std::vector<double> y1 = {0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100};
	std::vector<double> y2 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	XYPlot plot1(fm);
	plot1.caption(L"XYPlot test");
	plot1.addPlotData(PlotData(x, y1, LineFormat(LineStyle::NONE), MarkerFormat(MarkerStyle::SQUARE, 0xB7102F)));
	plot1.addPlotData(PlotData(x, y2, LineFormat(LineStyle::SOLID, 0x00008B), MarkerFormat(MarkerStyle::PLUS)));
	plot1.grid(true);
	plot1.getXAxis().scale(1.0);
	plot1.getYAxis().scale(1.0);
	plot1.getXAxis().label(L"x-Axis");
	plot1.getYAxis().label(L"y-Axis");
	plot1.fit125();
	plot1.title(L"This is the title");

	XYPlot plot2(fm);
	plot2.caption(L"XYPlot test");
	plot2.addPlotData(PlotData(x, y1, LineFormat(LineStyle::NONE), MarkerFormat(MarkerStyle::SQUARE, 0xB7102F)));
	plot2.addPlotData(PlotData(x, y2, LineFormat(LineStyle::SOLID, 0x00008B), MarkerFormat(MarkerStyle::PLUS)));
	plot2.grid(true);
	plot2.getXAxis().scale(1.0);
	plot2.getYAxis().scale(1.0);
	plot2.fit125();


	place plc(fm);
	plc.div("<plot1><plot2>");
	plc.field("plot1") << plot1;
	plc.field("plot2") << plot2;
	plc.collocate();

	fm.show();
	exec();

}
*/