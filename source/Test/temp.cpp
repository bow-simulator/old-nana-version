#include "../FEM/Numerics.h"

#include <algorithm>
#include <limits>


int main(int argc, char* argv[])
{
	auto f = [](double x)
	{
		return -1 + x*x;
	};
	
	//std::cout << "x = " << numerics::root(f, 0, 5, 0.001, 0) << "\n";

	double xa = 1.5;
	
	try
	{
		double xb = numerics::findSignChange<numerics::POS>(f, xa, 1.1);
		std::cout << xb << "\n";
	}
	catch(std::exception& e)
	{
		std::cout << e.what();
	}
}

/*
//Working version, adapted from scipy function brentq, http://www.scipy.org/
//https://github.com/scipy/scipy/blob/033bfe49ba1b455bf6f26ff9d281b0a6d44b21c2/scipy/optimize/Zeros/brentq.c
double brent(const std::function<double(double)>& f, double xa, double xb, double xtol, double rtol, int iter = 20)
{
	double xblk = 0.0;
	double fblk = 0.0;
	double sa = 0.0;
	double sb = 0.0;
	
	double fa = f(xa);
	double fb = f(xb);

	assert(fa*fb <= 0);

	if(fa == 0)
	{
		return xa;
	}

	if(fb == 0)
	{
		return xb;
	}

	for(unsigned int i = 0; i < iter; ++i)
	{
		if(fa*fb < 0)
		{
			xblk = xa;
			fblk = fa;
			sb = xb - xa;
			sa = sb;
		}

		if(std::abs(fblk) < std::abs(fb))
		{
			xa = xb;
			xb = xblk;
			xblk = xa;
			fa = fb;
			fb = fblk;
			fblk = fa;
		}

		double tol = xtol + rtol*std::abs(xb);
		double sbis = (xblk - xb)/2.0;

		if(fb == 0 || std::abs(sbis) < tol)
		{
			return xb;
		}

		if(std::abs(sa) > tol && std::abs(fb) < std::abs(fa))
		{
			double stry;
			if(xa == xblk)
			{
				// interpolate
				stry = -fb*(xb - xa)/(fb - fa);
			}
			else
			{
				// extrapolate
				double dpre = (fa - fb)/(xa - xb);
				double dblk = (fblk - fb)/(xblk - xb);
				stry = -fb*(fblk*dblk - fa*dpre)/(dblk*dpre*(fblk - fa));
			}
			
			if(2.0*std::abs(stry) < std::min(std::abs(sa), 3.0*std::abs(sbis) - tol))
			{
				// good short step
				sa = sb;
				sb = stry;
			}
			else
			{
				// bisect
				sa = sbis;
				sb = sbis;
			}
		}
		else
		{
			// bisect
			sa = sbis;
			sb = sbis;
		}

		xa = xb;
		fa = fb;
		
		if(std::abs(sb) > tol)
		{
			xb += sb;
		}
		else
		{
			xb += (sbis > 0 ? tol : -tol);
		}

		fb = f(xb);
	}

	return xb;
}
*/






/*
#include <nana/gui/wvl.hpp>
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/textbox.hpp>
#include <nana/gui/widgets/panel.hpp>

class NumberInput: public nana::panel<false>
{
public:
NumberInput(nana::window wd)
: panel(wd),
plc(*this),
input(*this)
{
plc.div("vertical <><<><input weight=120><> weight=24><>");
plc.field("input") << input;
plc.collocate();
}

private:
nana::place plc;
nana::textbox input;

};




int wmain()
{
using namespace nana;

form fm;
NumberInput input(fm);

place plc(fm);
//plc.div("<><a weight=200><>");
plc.div("<a>");
plc.field("a") << input;
plc.collocate();

fm.show();
exec();
}
*/



/*
#include "../GUI/CurveEditor/PanelCenterline.h"

int wmain()
{

nana::form fm(nana::API::make_center(1000, 300));
PanelCenterline panel(fm);

std::wifstream file_in(L"Test.json");
jsoncons::wjson obj_in = jsoncons::wjson::parse(file_in);
file_in.close();

panel.load(obj_in);

nana::place plc(fm);
plc.div("<panel>");
plc.field("panel") << panel;
plc.collocate();

fm.show();
nana::exec();

jsoncons::wjson obj_out = panel.save();
std::wofstream file_out;
file_out.open(L"Test.json");
file_out << jsoncons::pretty_print(obj_out);
file_out.close();

return 0;

}
*/




/*
#include "CurveEditor.h"
#include "../GUI/Plot/Plot.h"
#include "../Bow/Geometry/ComposedCurve.h"

#include <jsoncons/json.hpp>

int wmain()
{

using namespace jsoncons;

//Line
wjson line_seg_length(wjson::an_array);
line_seg_length.add(L"length " + units::length.label());
line_seg_length.add(100);

wjson line_seg(wjson::an_array);
line_seg.add(line_seg_length);

wjson line;
line[L"segment"] = line_seg;
line[L"type"] = L"Line";
line[L"name"] = L"Unnamed";
line[L"angle"] = 0;


//Arc (radius, angle)
wjson arc1_seg_radius(wjson::an_array);
arc1_seg_radius.add(L"radius " + units::length.label());
arc1_seg_radius.add(100);

wjson arc1_seg_length(wjson::an_array);
arc1_seg_length.add(L"angle " + units::angle.label());
arc1_seg_length.add(45);

wjson arc1_seg(wjson::an_array);
arc1_seg.add(arc1_seg_radius);
arc1_seg.add(arc1_seg_length);

wjson arc1;
arc1[L"segment"] = arc1_seg;
arc1[L"type"] = L"Arc (r, \u03B1)";
arc1[L"name"] = L"Unnamed";
arc1[L"angle"] = 0;


//Arc (radius, length)
wjson arc2_seg_radius(wjson::an_array);
arc2_seg_radius.add(L"radius " + units::length.label());
arc2_seg_radius.add(100);

wjson arc2_seg_length(wjson::an_array);
arc2_seg_length.add(L"length " + units::length.label());
arc2_seg_length.add(100);

wjson arc2_seg(wjson::an_array);
arc2_seg.add(arc2_seg_radius);
arc2_seg.add(arc2_seg_length);

wjson arc2;
arc2[L"segment"] = arc2_seg;
arc2[L"type"] = L"Arc (r, L)";
arc2[L"name"] = L"Unnamed";
arc2[L"angle"] = 0;


std::vector<wjson> segment_types;
segment_types.push_back(line);
segment_types.push_back(arc1);
segment_types.push_back(arc2);

//std::wcout << pretty_print(arc1);

nana::form fm;
CurveEditor editor(fm, L"Limb centerline", segment_types, [](){});

nana::place plc(fm);
plc.div("<listbox>");
plc.field("listbox") << editor;
plc.collocate();

fm.show();
nana::exec();

return 0;

}
*/



/*

#include "../GUI/Plot/Plot.h"
#include "../Bow/Geometry/ComposedCurve.h"


int wmain()
{

ArcSegment arc1(1, 0.1);
ArcSegment arc2(2, 0.1);
ArcSegment arc3(-3, 0.1);
LineSegment line1(1.0);
SplineSegment spl1({0, 0.1, 0.2}, {0, 0.1, 0.4});

spl1.align(true);
spl1.alignmentAngle(-0.1);

ComposedCurve curve;
curve.addSegment(arc1);
curve.addSegment(arc2);
curve.addSegment(arc3);
curve.addSegment(line1);
curve.addSegment(spl1);

std::vector<double> x;
std::vector<double> y;


//curve.getNodes(x, y);


for(double p = 0.0; p <= 1.001; p += 0.001)
{
Eigen::Vector2d R = curve.value(p);
x.push_back(R(0));
y.push_back(R(1));
}


Plot::plot(x, y, L"Arc segment test", L"x", L"y");

return 0;

}

*/


/*
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

class AABB
{
public:
double x_min;
double x_max;
double y_min;
double y_max;

AABB(std::string name, double x_min, double x_max, double y_min, double y_max)
: name(name), x_min(x_min), x_max(x_max), y_min(y_min), y_max(y_max)
{

}

std::string name;

};


void sweepAndPruneX(std::vector<AABB> axis_list)
{

std::sort(axis_list.begin(), axis_list.end(), [](const AABB& box1, const AABB& box2){ return box1.x_min < box2.x_min; });

std::vector<AABB> active_list;
for(int i = 0; i < axis_list.size(); ++i)
{

for(int j = 0; j < active_list.size(); ++j)
{

if(active_list[j].x_max > axis_list[i].x_min)
{
std::cout << "Pair (x): " << axis_list[i].name << ", " << active_list[j].name << "\n";
}
else
{
active_list.erase(active_list.begin() + j);
--j;
}

}

active_list.push_back(axis_list[i]);

}

}


void sweepAndPruneXY(std::vector<AABB> list)
{

std::vector<AABB> axis_list_x = list;


std::sort(axis_list_x.begin(), axis_list_x.end(), [](const AABB& box1, const AABB& box2){ return box1.x_min < box2.x_min; });

std::vector<AABB> active_list;
for(int i = 0; i < axis_list_x.size(); ++i)
{

for(int j = 0; j < axis_list_x.size(); ++j)
{

if(active_list[j].x_max > axis_list_x[i].x_min)
{
std::cout << "Pair (x): " << axis_list_x[i].name << ", " << axis_list_x[j].name << "\n";
}
else
{
axis_list_x.erase(active_list.begin() + j);
--j;
}

}

active_list.push_back(axis_list_x[i]);

}

}



int wmain()
{

std::vector<AABB> axis_list{{"A", 1, 5, 1, 3}, {"B", 4, 7, 4, 7}, {"C", 6, 10, 3, 5}, {"D", 9.5, 10.5, 2, 9}};

//sweepAndPruneXY(axis_list);

}
*/


/*
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

class AABB
{
public:
double x_min;
double x_max;

AABB(std::string name, double x_min, double x_max): name(name), x_min(x_min), x_max(x_max)
{

}

std::string name;

};


int wmain()
{

std::vector<AABB> axis_list{{"B", 1, 10}, {"A", 0, 4}, {"C", 3, 5}, {"D", 6, 8}};

std::sort(axis_list.begin(), axis_list.end(), [](const AABB& box1, const AABB& box2){ return box1.x_min < box2.x_min; });

std::vector<AABB> active_list;
for(int i = 0; i < axis_list.size(); ++i)
{

for(int j = 0; j < active_list.size(); ++j)
{

if(active_list[j].x_max > axis_list[i].x_min)
{
std::cout << "Pair: " << axis_list[i].name << ", " << active_list[j].name << "\n";
}
else
{
active_list.erase(active_list.begin() + j);
--j;
}

}

active_list.push_back(axis_list[i]);

}

}
*/