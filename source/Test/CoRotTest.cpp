// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2013 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/licenses/gpl-3.0.txt)

#include "../FEM/Model.h"
#include "../FEM/Elements/BeamElementNew.h"
#include "../FEM/Elements/BeamElementCoRot.h"
#include "../FEM/Elements/BarElement.h"
#include "../FEM/Elements/MassElement.h"
#include "../FEM/Elements/ContactElement1D.h"
#include "../Bow/Bow.h"
#include "../Bow/Spline.h"
#include "../Bow/SplineCurve.h"

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <ctime>

#include <Eigen/Core>
#include <Eigen/Sparse>

/**
Static test "Large deformation of a cantilever beam subjected to a vertical tip load" from [1]\n
\n
[1] On the correct representation of bending and axial deformation in the absolute nodal coordinate formulation with an elastic line approach\n
Johannes Gerstmayr, Hans Irschik\n
Journal of Sound and Vibration 318 (2008) 461�487\n
*/
void coRotTest1()
{
	const int n_elements = 25;
	const double solver_tolerance = 1e-12;
	const int solver_iterations = 50;

	double L = 2.0;
	double b = 0.1;
	double h = 0.1;

	double I = b*h*h*h/12.0;
	double A = b*h;
	double E =  2.07e11;
	
	double F0 = 3*E*I/(L*L);

	Model mdl;
	std::vector<BeamElementCoRot> elements;
	elements.reserve(n_elements);

	for(int i = 0; i < n_elements; ++i)
	{
		//std::cout << 3*i-3 << ", " << 3*i-2 << ", " << 3*i-1 << ", " << 3*i << ", " << 3*i+1 << ", " << 3*i+2 << "\n";

		Eigen::Vector2d r0 = (Eigen::Vector2d() << i*L/n_elements, 0.0).finished();
		Eigen::Vector2d r1 = (Eigen::Vector2d() << (i+1)*L/n_elements, 0.0).finished();

		elements.push_back(BeamElementCoRot(0.0, E*I, E*A, r0, r1));
		elements.back().dofMap() = {3*i-3, 3*i-2, 3*i-1, 3*i, 3*i+1, 3*i+2};
		elements.back().externalForces() = Eigen::VectorXd::Zero(6);
		mdl.add(&elements.back());
	}
	elements.back().externalForces()(4) = F0;
	
	mdl.finished();

	if(mdl.staticEquilibrium(solver_tolerance, solver_iterations))
	{
		Eigen::VectorXd x_end = mdl.localCoordinates(elements.back());
		double ux = L-x_end(3);
		double uy = x_end(4);
		std::cout << "staticBeamTest1: " << (ux - 0.5089228704) << ", " << (uy - 1.2083981311) << "\n";
	}
	else
	{
		std::cout << "staticBeamTest1: No convergence\n";
	}
}

/**
Static test "Bending of a pre-curved beam into a full circle" from [1]\n
Except M = EI/R instread of M = EI*R
\n
[1] On the correct representation of bending and axial deformation in the absolute nodal coordinate formulation with an elastic line approach\n
Johannes Gerstmayr, Hans Irschik\n
Journal of Sound and Vibration 318 (2008) 461�487\n
*/
void coRotTest2()
{
	const int n_elements = 40;
	const double solver_tolerance = 1e-14;
	const int solver_iterations = 50;
	
	double EI = 10.0e6;
	double EA = 5.0e8;
	double R = 1.5;
	
	double M = EI/R;

	Model mdl;
	std::vector<BeamElementCoRot> elements;
	elements.reserve(n_elements);

	for(int i = 0; i < n_elements; ++i)
	{
		double phi0 = double(i)*M_PI/double(n_elements);
		double phi1 = double(i+1)*M_PI/double(n_elements);

		Eigen::Vector2d r0;
		r0 << R*std::sin(phi0), R*(std::cos(phi0) - 1.0);

		Eigen::Vector2d r1;
		r1 << R*std::sin(phi1), R*(std::cos(phi1) - 1.0);

		//std::cout << r0.transpose() << ", " << r1.transpose() << "\n";

		elements.push_back(BeamElementCoRot(0.0, EI, EA, r0, r1));
		elements.back().dofMap() = {3*i-3, 3*i-2, 3*i-1, 3*i, 3*i+1, 3*i+2};
		elements.back().externalForces() = Eigen::VectorXd::Zero(6);
		mdl.add(&elements.back());
	}

	mdl.finished();

	for(double load = 0.0; load <= 1.0; load += 0.1)
	{
		elements.back().externalForces()(5) = -load*M;
		
		//std::cout << load << "\n";

		if(!mdl.staticEquilibrium(solver_tolerance, solver_iterations))
		{
			std::cout << "staticBeamTest2: No convergence\n";
			return;
		}
	}

	//Make sure
	elements.back().externalForces()(5) = -M;
	mdl.staticEquilibrium(solver_tolerance, solver_iterations);

	Eigen::VectorXd x_end = mdl.localCoordinates(elements.back());
	double ux = x_end(3);
	double uy = x_end(4);
	double phi = x_end(5);
	std::cout << "staticBeamTest2: " << ux << ", " << uy << ", phi = " << phi << "\n";
}

int wmain()
{
	//coRotTest1();
	coRotTest2();

	return 0;
}