// Bow Simulation Tool
// https://sourceforge.net/projects/bowsimulationtool/
//
// Copyright (C) 2013 Stefan Pfeifer
// Distributed under the GNU General Public License v3.0
// (See accompanying file License.txt or copy at http://www.gnu.org/licenses/gpl-3.0.txt)

#include "../FEM/Model.h"
#include "../FEM/Elements/BeamElementT1L0.h"
#include "../FEM/Elements/BeamElementT1L1.h"
#include "../FEM/Elements/BeamElementT1L2.h"
#include "../FEM/Elements/BeamElementT1L3.h"
#include "../FEM/Elements/BeamElementT2L2.h"
#include "../FEM/Elements/BeamElementNew.h"
#include "../FEM/Elements/BeamElementCoRot.h"
#include "../FEM/Elements/BarElement.h"
#include "../FEM/Elements/MassElement.h"
#include "../FEM/Elements/ContactElement1D.h"
#include "../Bow/Bow.h"
#include "../Bow/Spline.h"
#include "../Bow/SplineCurve.h"

#include <vector>
#include <string>
#include <iostream>
#include <cmath>
#include <ctime>

#include <Eigen/Core>
#include <Eigen/Sparse>

/**
Static test "Large deformation of a cantilever beam subjected to a vertical tip load" from [1]\n
\n
[1] On the correct representation of bending and axial deformation in the absolute nodal coordinate formulation with an elastic line approach\n
Johannes Gerstmayr, Hans Irschik\n
Journal of Sound and Vibration 318 (2008) 461�487\n
*/
template<typename BeamElement>
bool staticBeamTest1(std::string name)
{
	const int n_elements = 25;
	const double solver_tolerance = 1e-12;
	const int solver_iterations = 50;

	double L = 2.0;
	double b = 0.1;
	double h = 0.1;

	double I = b*h*h*h/12.0;
	double A = b*h;
	double E =  2.07e11;
	
	double F0 = 3*E*I/(L*L);

	Model mdl;
	std::vector<BeamElement> elements;
	elements.reserve(n_elements);

	for(int i = 0; i < n_elements; ++i)
	{
		elements.push_back(BeamElement(0.0, E*I, E*A, L/n_elements));
		elements.back().dofMap() = {4*i-4, 4*i-3, 4*i-2, 4*i-1, 4*i, 4*i+1, 4*i+2, 4*i+3};
		elements.back().referenceCoordinates() = (Eigen::VectorXd(8) << i*L/n_elements, 0.0, 1.0, 0.0, (i+1)*L/n_elements, 0.0, 1.0, 0.0).finished();
		elements.back().externalForces() = Eigen::VectorXd::Zero(8);
		mdl.add(&elements.back());
	}
	elements.back().externalForces()(5) = F0;
	
	mdl.finished();
	
	if(mdl.staticEquilibrium(solver_tolerance, solver_iterations))
	{
		Eigen::VectorXd x_end = mdl.localCoordinates(elements.back());
		double ux = L-x_end(4);
		double uy = x_end(5);
		std::cout << "staticBeamTest1 - " << name << ": " << (ux - 0.5089228704) << ", " << (uy - 1.2083981311) << "\n";
	}
	else
	{
		std::cout << "staticBeamTest1 - " << name << ": No convergence\n";
	}
}

/**
Static test "Bending of a pre-curved beam into a full circle" from [1]\n
\n
[1] On the correct representation of bending and axial deformation in the absolute nodal coordinate formulation with an elastic line approach\n
Johannes Gerstmayr, Hans Irschik\n
Journal of Sound and Vibration 318 (2008) 461�487\n
*/
template<typename BeamElement>
bool staticBeamTest2(std::string name)
{
	const int n_elements = 50;
	const double solver_tolerance = 1e-12;
	const int solver_iterations = 50;

	double EI = 10.0e5;
	double EA = 5.0e8;
	double R = 1.0;
	
	double M = 10.5*EI*R;

	Model mdl;
	std::vector<BeamElement> elements;
	elements.reserve(n_elements);

	for(int i = 0; i < n_elements; ++i)
	{
		double phi0 = i*M_PI/n_elements;
		double phi1 = (i+1)*M_PI/n_elements;

		Eigen::Vector2d r0;
		r0 << R*sin(phi0), R*(cos(phi0) - 1.0);

		Eigen::Vector2d r1;
		r1 << R*sin(phi1), R*(cos(phi1) - 1.0);

		//std::cout << "r0: " << r0.transpose() << ", r1: " << r1.transpose() << "\n";

		double L = (r1 - r0).norm();
		Eigen::VectorXd slope = 1/L*(r1 - r0);

		elements.push_back(BeamElement(0.0, EI, EA, L));
		elements.back().dofMap() = {4*i-4, 4*i-3, 4*i-2, 4*i-1, 4*i, 4*i+1, 4*i+2, 4*i+3};
		elements.back().referenceCoordinates() = (Eigen::VectorXd(8) << r0, slope, r1, slope).finished();
		elements.back().externalForces() = Eigen::VectorXd::Zero(8);
		mdl.add(&elements.back());
	}

	//Problem: How to apply torque to an ANCF-Element?
	elements.back().externalForces()(6) = M;
	//elements.back().externalForces()(7) = M;

	mdl.finished();
	
	if(mdl.staticEquilibrium(solver_tolerance, solver_iterations))
	{
		Eigen::VectorXd x_end = mdl.localCoordinates(elements.back());
		double ux = x_end(4);
		double uy = x_end(5);
		std::cout << "staticBeamTest2 - " << name << ": " << ux << ", " << uy << "\n";
	}
	else
	{
		std::cout << "staticBeamTest2 - " << name << ": No convergence\n";
	}
}

int wmain()
{
	/*
	staticBeamTest1<BeamElementNew>("BeamElementNew");
	staticBeamTest1<BeamElementT1L0>("BeamElementT1L0");
	staticBeamTest1<BeamElementT1L1>("BeamElementT1L1");
	staticBeamTest1<BeamElementT1L2>("BeamElementT1L2");
	staticBeamTest1<BeamElementT1L3>("BeamElementT1L3");
	staticBeamTest1<BeamElementT2L2>("BeamElementT2L2");
	*/

	//staticBeamTest2<BeamElementNew>("BeamElementNew");
	//staticBeamTest2<BeamElementT1L0>("BeamElementT1L0");
	//staticBeamTest2<BeamElementT1L1>("BeamElementT1L1");
	//staticBeamTest2<BeamElementT1L2>("BeamElementT1L2");
	//staticBeamTest2<BeamElementT1L3>("BeamElementT1L3");
	//staticBeamTest2<BeamElementT2L2>("BeamElementT2L2");

	//coRotTest1();
	coRotTest3();

	return 0;
}

/*
void example()
{

	double rho = 7850;
	double E = 210e9;
	double A = 7.068e-6;
	double I = 3.976e-12;

	double e_0 = 0.0015;
	double e_1 = 0.0015;
	
	double L = 0.5;

	double Q = 0.5;
	double N = 0;

	BeamElement element(CrossSection(rho, E, A, I, e_0, e_1), L);

	element.getDofMap() = {FIXED, FIXED, FIXED, FIXED, 0, 1, 2, 3};
	element.getReferenceCoordinates() = (VectorXd(8) << 0, 0, 1, 0, L, 0, 1, 0).finished();
	element.getExternalForces() = (VectorXd(8) << 0, 0, 0, 0, N, Q, 0, 0).finished();
	
	Model model(4);
	model.addElement(&element);

	Solver solver(model);

	std::cout << solver.findEquilibriumNR(0.01, 20) << "\n\n";

	std::cout << "Numerisches Ergebnis\n";
	std::cout << "u: " << solver.getLocalCoordinates(element).transpose()(4)-L << "\n";
	std::cout << "v: " << solver.getLocalCoordinates(element).transpose()(5) << "\n";
	std::cout << "Pot. Energie: " << element.getPotentialEnergy(solver.getLocalCoordinates(element).transpose()) << "\n\n";
	
	//VectorXd x(8);
	//x << 0, 0, 1, 0, L+0.1, 0, 1, 0;
	//std::cout << "Spannungen unten/oben: " << element.getTension(x, 0.25).transpose() << "\n";

	std::cout << "Analytisches Ergebnis\n";
	std::cout << "u: " << L/(E*A)*N << "\n";
	std::cout << "v: " << L*L*L/(3*E*I)*Q << "\n";
	std::cout << "Pot. Energie: " << 0.5*L/(E*A)*N*N + 0.5*L*L*L/(3*E*I)*Q*Q << "\n\n";

}
*/

/*
void example1()
{

	double rhoA = 0.0552;
	double EI = 0.835;
	double EA = 10;
	double L = 0.01;

	BeamElementT2L2 element(rhoA, EI, EA, L);

	Eigen::VectorXd x_ref = (Eigen::VectorXd(8) << 0, 0, 1, 0, L, 0, 1, 0).finished();
	Eigen::VectorXd x_rel = (Eigen::VectorXd(8) << 0, 0, 0, 0, 0, 0, 0, 0).finished();
	Eigen::VectorXd x_abs = x_ref + x_rel;
	
	Eigen::MatrixXd J1 = element.getTangentStiffness(x_abs);
	Eigen::MatrixXd J2 = element.Element::getTangentStiffness(x_abs);

	//std::cout << J2 << "\n\n\n" << J1 << "\n\n\n";

	//std::cout << J2-J1;
	//Eigen::MatrixXd dot_x_abs(Eigen::MatrixXd::Zero(8));
	//element.getInternalForces(x_abs, dot_x_abs);

}
*/

/*
void example2()
{

	double rhoA = 10;
	double EA = 100;
	double L = 1;

	BarElement element(rhoA, EA, L);
	element.getDofMap() = {FIXED, FIXED, 0, 1};
	element.getExternalForces() = Eigen::VectorXd::Zero(4);
	element.getReferenceCoordinates() = (Eigen::VectorXd(4) << 0, 0, L+0.1, 0).finished();

	Model mdl;
	mdl.add(&element);
	mdl.finished();

	mdl.staticEquilibrium(1e-5, 100);

	std::cout << mdl.getLocalCoordinates(element) << "\n";


}
*/

/*
void example3()
{

	std::vector<double> y = {0, 1, 3};

	Spline s(y);

	for(double t = 0.0; t < 1.0; t += 0.01)
	{
		std::cout << t << ";" << s.interpolate(t) << "\n";
	}

}
*/

/*
void example4()
{

	BarElement bar(10, 100, 1);	//rhoA, EA, L
	bar.getDofMap() = {FIXED, FIXED, 0, FIXED};
	bar.getExternalForces() = Eigen::VectorXd::Zero(4);
	bar.getReferenceCoordinates() = (Eigen::VectorXd(4) << 0, 0, 1, 0).finished();

	ContactElement1D contact(1e10, 10); //c, d
	contact.getDofMap() = {0, FIXED};
	contact.getExternalForces() = Eigen::VectorXd::Zero(2);
	contact.getReferenceCoordinates() = (Eigen::VectorXd(2) << 1, 2).finished();
	contact.setOneSided(true);
	
	Model mdl;
	mdl.add(&bar);
	mdl.add(&contact);
	mdl.finished();


	for(double F = 0.0; F < 200.0; F += 5)
	{
		bar.getExternalForces() = (Eigen::VectorXd(4) << 0, 0, F, 0).finished();
		mdl.staticEquilibrium(1e-5, 100);
		std::cout << mdl.getLocalCoordinates(bar)(2) << ";" << F << "\n";
	}

}
*/

/*
void example5()
{
	
	ContactElement1D contact(1e10, 10, true); //c, d
	contact.getDofMap() = {0, FIXED};
	contact.getExternalForces() = Eigen::VectorXd::Zero(2);
	contact.getReferenceCoordinates() = (Eigen::VectorXd(2) << 1, 2).finished();
	//contact.setOneSided(false);
	
	Model mdl;
	mdl.add(&contact);
	mdl.finished();

	mdl.staticEquilibrium(1e-5, 100);
	std::cout << mdl.getLocalCoordinates(contact);

}
*/

/*
void example6()
{
	
	BarElement bar(10, 100, 1);	//rhoA, EA, L
	bar.getDofMap() = {FIXED, FIXED, 0, FIXED};
	bar.getExternalForces() = Eigen::VectorXd::Zero(4);
	bar.getReferenceCoordinates() = (Eigen::VectorXd(4) << 0, 0, 1.5, 0).finished();

	Model mdl;
	mdl.add(&bar);
	mdl.finished();

	double dt = 0.01;
	for(double t = 0.0; t < 10.0; t += dt)
	{
		mdl.integrate(dt);
		std::cout << t << ";" << mdl.getLocalCoordinates(bar)(2) << "\n";
	}

}
*/