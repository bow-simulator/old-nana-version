#define WIN32
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Menu_Bar.H>

Fl_Window* win;

void Menu_CB_Open(Fl_Widget* w, void*) { win->label("Open selected"); }
void Menu_CB_Save(Fl_Widget* w, void*) { win->label("Save selected"); }
void Menu_CB_Quit(Fl_Widget* w, void*) { win->label("Quit selected"); }
void Menu_CB_Help(Fl_Widget* w, void*) { win->label("Help selected"); }

int wmain() {

	Fl::scheme("none");

	win = new Fl_Window(420, 280);
	win->begin();
	Fl_Menu_Bar menubar(0, 0, win->w(), 25);
	menubar.add("File/New", 0, Menu_CB_Open);
	menubar.add("File/Open", 0, Menu_CB_Open);
	menubar.add("File/Save", 0, Menu_CB_Open);
	menubar.add("File/Save as", 0, Menu_CB_Open);
	menubar.add("File/Exit", 0, Menu_CB_Open);
	menubar.add("View/Limb preview", 0, Menu_CB_Open);
	menubar.add("Simulation/Run", 0, Menu_CB_Open);
	menubar.add("Help/About", 0, Menu_CB_Save);
	win->end();
	win->show();
	return(Fl::run());
}