# In this example we will show the relationship between the brace height of a bow and
# the resulting draw force and stored energy. To achieve this we will perform a series of
# simulations with varying parameters and plot a graph with the results at the end.

# Import the Bow Simulation Tool interface
import bst

# We will also need numpy for numerics and matplotlib for plotting graphs. They are not part of
# the python standard library, so you might have to install them first.
import numpy
import matplotlib.pyplot as plt

# Define the bow model for which we variate the brace height
data = {
    "parameters":
    {
        "centerline":
        {
            "x": [0, 0],
            "y": [0, 200]
        },
        "sections":
        {
            "pos": [0, 100],
            "width": [10, 10],
            "height": [1, 1],
        },
        "material":
        {
            "E": 210000,
            "rho": 7850
        },
        "string":
        {
            "EA": 3000,
            "diameter": 0,
            "draw_length": 200,
            "brace_height": 50,
            "rhoA": 0.01
        },
        "masses":
        {
            "arrow": 5,
            "limb_tip": 0,
            "serving": 0
        },
    },
    "settings":
    {
        "contact_handling": False,
        "elements": 25,
        "timestep_factor": 0.1
    },
    "comments": "",
    "version": "2015.2"
}

# Create a Simulation instance
with bst.Simulation("../Release/BowSimulationTool.exe") as sim:

    # Prepare a list with 20 brace heights from 5 to 130 and empty lists to store draw force
    # and drawing work for each brace heigt
    brace_height = numpy.linspace(5, 130, 20, True)
    draw_force = []
    drawing_work = []

    # Set the brace height in data to those in brace_height one after another
    for data["parameters"]["string"]["brace_height"] in brace_height:
        # Perform a static simulation with each modified data set
        sim.run(data, False)

        # Append the results to the prepared lists
        draw_force.append(sim.statics["final_draw_force"])
        drawing_work.append(sim.statics["drawing_work"])

# We can now visualise the results by plotting the draw force and the stored energy over the
# brace height. The plot shows the well-known but sometimes counterintuitive effect that the
# stored energy decreases with increasing brace height, even though the draw force increases.
# The reason for this is that the energy gain due to the increasing draw force is
# overcompensated by the energy loss due to the decreasing string travel.
fig, ax1 = plt.subplots()
ax1.plot(brace_height, draw_force, 'b-')
ax1.set_xlabel('Brace height [mm]')
ax1.set_ylabel('Draw force [N]', color='b')
for tl in ax1.get_yticklabels():
    tl.set_color('b')
    
ax2 = ax1.twinx()
ax2.plot(brace_height, drawing_work, 'r-')
ax2.set_ylabel('Stored energy [J]', color='r')
for tl in ax2.get_yticklabels():
    tl.set_color('r')
plt.show()
