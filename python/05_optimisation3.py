# This is an extension of the previous example that optimises the width as well as the height
# profile of the bow for maximum velocity. Also, a constraint for the maximum static limb
# stress is enforced. This leads to a result with a much better tiller 

import bst
import numpy
from scipy.optimize import minimize
import time

# Prepare bow data. n: Number of limb control points
n = 5

# Values for the constraints
max_width = 50
min_width = 10
max_height = 6
min_height = 3
max_stress = 1500

data = {
    "parameters":
    {
        "centerline":
        {
            "x":[0.0,0.0],
            "y":[0.0,450.0]
        },
        "masses":
        {
            "arrow":20.0,
            "limb_tip":0.0,
            "serving":0.0
        },
        "material":
        {
            "E":210000.0,
            "rho":7850.0
        },
        "sections":
        {
            "pos":numpy.linspace(0, 100, n, True).tolist(),
            "width":numpy.linspace(50, 10, n, True).tolist(),
            "height":numpy.linspace(5, 5, n, True).tolist()
        },
        "string":
        {
            "EA":40000,
            "rhoA":0.04,
            "diameter":0.0,
            "draw_length":400.0,
            "brace_height":120.0
        }
    },
    "settings":
    {
        "contact_handling":False,
        "elements":3*n,
        "timestep_factor":0.9
    },
    "comments":"",
    "version":"2015.2"
}

def to_data(x):
    data["parameters"]["sections"]["width"] = x[0:n].tolist()
    data["parameters"]["sections"]["height"] = x[n:2*n+1].tolist()

def from_data():
    x = numpy.empty(2*n)
    x[0:n] = data["parameters"]["sections"]["width"]
    x[n:2*n+1] = data["parameters"]["sections"]["height"]
    return x;

with bst.Simulation("../Release/BowSimulationTool.exe") as sim:

    # Calculates arrow velocity from widths and heights
    def objective(x):
        to_data(x)

        # Geometrical constraints
        if max(data["parameters"]["sections"]["width"]) > max_width:
            return 0
        if min(data["parameters"]["sections"]["width"]) < min_width:
            return 0
        if max(data["parameters"]["sections"]["height"]) > max_height:
            return 0
        if min(data["parameters"]["sections"]["height"]) < min_height:
            return 0

        sim.run(data, True)

        # Constraint for maximum limb stress
        if sim.statics["max_stress"] > max_stress:
            return 0

        v = sim.dynamics["final_arrow_velocity"]
        print("v = ", v, "\n")

        return -v

    # Concatenate width and height
    x0 = from_data()

    try:
        t1 = time.time()
        res = minimize(objective, x0, method='Powell', options={'xtol': 1e-5, 'disp': True})
        to_data(res.x)
        print("Time: ", time.time() - t1)
    finally:
        bst.save(data, "opt3.bow")
        sim.show("opt3.bow")
