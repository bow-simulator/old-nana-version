import bst
import numpy
from scipy.optimize import fmin_cobyla
from scipy.optimize import approx_fprime
import time

min_width = 10
max_width = 50
max_stress = 1500

n = 6

data = {
    "parameters":
    {
        "centerline":
        {
            "x": [0, 0],
            "y": [0, 450]
        },
        "masses":
        {
            "arrow": 30,
            "limb_tip": 0,
            "serving": 0
        },
        "material":
        {
            "E": 210000,
            "rho": 7850
        },
        "sections":
        {
            "pos": numpy.linspace(0, 100, n, True).tolist(),
            "width": numpy.linspace(max_width, min_width, n, True).tolist(),
            "height": numpy.linspace(5, 5, n, True).tolist()
        },
        "string":
        {
            "EA":40000,
            "rhoA":0.04,
            "diameter":0.0,
            "draw_length":400.0,
            "brace_height":120.0
        }
    },
    "settings":
    {
        "contact_handling":False,
        "elements":3*n,
        "timestep_factor":0.9
    },
    "comments":"",
    "version":"2015.2"
}

def to_data(x):
    data["parameters"]["sections"]["width"] = x.tolist()

def from_data():
    x = numpy.array(data["parameters"]["sections"]["width"])
    return x;

with bst.Simulation("../Release/BowSimulationTool.exe") as sim:

    def objective(x):
        to_data(x)

        sim.run(data, True)

        v = sim.dynamics["final_arrow_velocity"]
        print("v = ", v, "\n")
        return -v

    constraints_ge = []

    # Constraints for minimal width
    for i in range(0, n):
        def f1(x, i = i):
            return x[i] - min_width
        constraints_ge.append(f1)

    # Constraints for maximal width
    for i in range(0, n):
        def f2(x, i = i):
            return max_width - x[i]
        constraints_ge.append(f2)

    # Constraint for maximum stress
    def f3(x):
        to_data(x)
        sim.run(data, False)
        return numpy.array(max_stress - sim.statics["max_stress"])
    constraints_ge.append(f3)

    # Constraint for draw force
    def f4(x):
        to_data(x)
        sim.run(data, False)
        return numpy.array(max_draw_force - sim.statics["final_draw_force"])
    constraints_ge.append(f4)

    try:
        x0 = from_data()

        t1 = time.time()
        res = fmin_cobyla(objective, x0, constraints_ge, disp=True)
        to_data(res)
    finally:
        print("Time passed: ", time.time() - t1, "s")
        bst.save(data, "cobyla.bow")
        sim.show("cobyla.bow")
