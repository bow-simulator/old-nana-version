import json
import subprocess
import tempfile
import os
import shutil

def load(path):
    with open(path) as file:
        data = json.load(file)
    return data

def save(data, path):
    with open(path, 'w') as file:
        json.dump(data, file)

class Simulation:
    statics = None
    dynamics = None
    exe_path = None
    temp_dir = None

    def __init__(self, exe_path = "BowSimulationTool.exe"):
        self.exe_path = exe_path
        self.temp_dir = tempfile.mkdtemp()

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        shutil.rmtree(self.temp_dir)
    
    def run(self, data, dynamic):
        file_in = self.temp_dir + "/temp.bow"
        file_out1 = self.temp_dir + "/temp.bow.statics"
        file_out2 = self.temp_dir + "/temp.bow.dynamics"

        save(data, file_in)
              
        if dynamic:
            subprocess.check_call([self.exe_path, file_in, "-s " + file_out1, "-d " + file_out2])
            self.dynamics = load(file_out2)
        else:
            subprocess.check_call([self.exe_path, file_in, "-s " + file_out1])

        self.statics = load(file_out1)
                
    def show(self, path):
        subprocess.check_call([self.exe_path, path])
