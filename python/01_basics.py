# This example will show you how to use Bow Simulation Tool from python.
# We will create a bow model, simulate it and display the results.

# Import the file bst.py, which contains an interface to Bow Simulation Tool
import bst

# First we define the data that describes the bow. We could load/save data from/to a .bow file with
#
# data = bst.load("filename.bow")
# bst.save(data, "filename.bow")
#
# but we can also just create it directly in code.
# The data layout is identical to that of the .bow files, but as a python dictionary instead:
data = {
    "parameters":
    {
        "centerline":
        {
            "x": [0, 0],
            "y": [0, 200]
        },
        "sections":
        {
            "pos": [0, 100],
            "width": [10, 10],
            "height": [1, 1],
        },
        "material":
        {
            "E": 210000,
            "rho": 7850
        },
        "string":
        {
            "EA": 3000,
            "diameter": 0,
            "draw_length": 200,
            "brace_height": 60,
            "rhoA": 0.01
        },
        "masses":
        {
            "arrow": 5,
            "limb_tip": 0,
            "serving": 0
        },
    },
    "settings":
    {
        "contact_handling": False,
        "elements": 25,
        "timestep_factor": 0.1
    },
    "comments": "",
    "version": "2015.2"
}

# Next we need an instance of the Simiulation class, which handles running simulations and retrieving the results.
# It should be created within a with-statement because that way it can clean up the temporary files it generated
# for communication with Bow Simulation Tool when they aren't needed anymore.
# The constructor optionally takes a path to the BowSimulationTool executable. Otherwise it will be assumed
# that the executable is located in the current directory.
with bst.Simulation("../Release/BowSimulationTool.exe") as sim:

    # Now it's time to start the simulation by calling the run method.
    # The parameters are:
    # - The data defining the bow
    # - A boolean for running the dynamic simulation. Here we only run statics.
    sim.run(data, False)

    # The results are stored in the 'statics' and 'dynamics' attributes of the Simulation object. In our case, only
    # the statics attribute is set, so let's print that:
    print("Results: ", sim.statics, "\n")
