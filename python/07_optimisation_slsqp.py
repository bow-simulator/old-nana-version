import bst
import numpy
from scipy.optimize import fmin_slsqp
from scipy.optimize import approx_fprime
import time

min_width = 15
max_width = 60
min_height = 6
max_height = 20

max_stress = 100

n = 5

data = {
    "parameters":
    {
        "centerline":
        {
            "x": [0, 0],
            "y": [0, 800]
        },
        "masses":
        {
            "arrow": 25,
            "limb_tip": 0,
            "serving": 0
        },
        "material":
        {
            "E": 11000,
            "rho": 705
        },
        "sections":
        {
            "pos": numpy.linspace(0, 100, n, True).tolist(),
            "width": numpy.linspace(max_width, min_width, n, True).tolist(),
            "height": numpy.linspace(6, 6, n, True).tolist()
        },
        "string":
        {
            "EA":30000,
            "rhoA":0.02,
            "diameter":0.0,
            "draw_length":700.0,
            "brace_height":180.0
        }
    },
    "settings":
    {
        "contact_handling":False,
        "elements":3*n,
        "timestep_factor":0.9
    },
    "comments":"",
    "version":"2015.2"
}

def to_data(x):
    data["parameters"]["sections"]["width"] = x[0:n].tolist()
    data["parameters"]["sections"]["height"] = x[n:2*n+1].tolist()

def from_data():
    x = numpy.array(data["parameters"]["sections"]["width"] + data["parameters"]["sections"]["height"])
    return x;

with bst.Simulation("../Release/BowSimulationTool.exe") as sim:

    def objective(x):
        to_data(x)

        sim.run(data, True)

        v = sim.dynamics["final_arrow_velocity"]
        print("v = ", v, "\n")
        return -v

    def derivative(x):
        #dx = 0.00001*x
        return approx_fprime(x, objective, 0.001)


    # C(x) = 0
    def constraints_eq(x):
        return numpy.array()

    # C(x) >= 0
    def constraints_ge(x):
        to_data(x)
        sim.run(data, False)

        return numpy.array(max_stress - sim.statics["max_stress"])

    # L <= x <= U
    constraints_bd = [(min_width, max_width)]*n + [(min_height, max_height)]*n
    
    try:
        x0 = from_data()

        t1 = time.time()
        res = fmin_slsqp(objective, x0, f_ieqcons=constraints_ge, fprime=derivative, bounds=constraints_bd, disp=True)
        to_data(res.out)
    finally:
        print("Time passed: ", time.time() - t1, "s")
        bst.save(data, "opt5.bow")
        sim.show("opt5.bow")
