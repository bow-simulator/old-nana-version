# This is a simple example for an unconstrained optimisation with one variable. Following problem: What is the
# optimal number of strands for the string of a given bow with given string material, if we're interested in arrow
# speed? A string with too many strands is very stiff but too heavy, a string with too few strands is very
# light but too elastic. So there has to be an optimum somewhere in this tradeoff between mass and stiffness.
# Let's find out...

# Import the Bow Simulation Tool interface, the math standard module,
# numpy and scipy (libraries for numeric types and scientific computing)
import bst
import math
import numpy
from scipy.optimize import minimize

# Bow data to optimise
data = {
    "parameters":
    {
        "centerline":
        {
            "x":
            [0.0,0.0],
            "y":
            [0.0,450.0]
        },
        "masses":
        {
            "arrow":20.0,
            "limb_tip":0.0,
            "serving":0.0
        },
        "material":
        {
            "E":210000.0,
            "rho":7850.0
        },
        "sections":
        {
            "height":
            [5.0,5.0],
            "pos":
            [0.0,100.0],
            "width":
            [50.0,10.0]
        },
        "string":
        {
            "EA":0,
            "rhoA":0,
            "diameter":0.0,
            "draw_length":400.0,
            "brace_height":150.0
        }
    },
    "settings":
    {
        "contact_handling":False,
        "elements":25,
        "timestep_factor":0.9
    },
    "comments":"",
    "version":"2015.2"
}

# Create the Simulation object
with bst.Simulation("../Release/BowSimulationTool.exe") as sim:

    # The objective function maps the design parameters x (here: number of strands) to an objective
    # (here: arrow velocity). The optimisation algorithm will then search for an x that minimizes the
    # objective function. For this example we treat the number of strands as a floating number, not an integer.
    def objective(x):

        # Properties for a single strand (Dacron, Source: Supertiller excel spreadsheet by Alan Case)
        EA_i = 3113
        rhoA_i = 3.33e-3

        # Set the data in the model according to the number of strands and simulate the dynamics
        data["parameters"]["string"]["EA"] = x[0]*EA_i
        data["parameters"]["string"]["rhoA"] = x[0]*rhoA_i
        sim.run(data, True)

        # Retrieve arrow velocity and print out a message to make the program progress visible
        v = sim.dynamics["final_arrow_velocity"]
        print("strands: ", x[0], ", v = ", v)

        # The optimisation algorithm will minimize the objective, but we want maximum velocity,
        # so we have to switch the sign here
        return -v

    # Starting value from which the optimisation proceeds
    x0 = numpy.array([16])

    # Minimise the objective function, start at x0, use nelder-mead algorithm, ...
    # For this example, as x is only a scalar, other optimisation methods would probably
    # be better, but this way the script is easily extensible to other problems
    res = minimize(objective, x0, method='nelder-mead', options={'xtol': 1e-3, 'disp': True})

    # Print the results
    print("Optimal number of strands: ", res.x[0])
    print("Resulting arrow velocity: ", -res.fun)
