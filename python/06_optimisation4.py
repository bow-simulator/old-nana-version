# This is an example for constrained optimisation with multiple variables.
# Consider this problem: What is the optimal width profile for an otherwise given bow if we want to maximise
# arrow speed? Constraint: Widths of the limb have to lie between an upper and a lower bound.
# Warning: Running this script can take a few minutes.

# This is an extension of the previous example that optimises the width as well as the height
# profile of the bow for maximum velocity. Also, a constraint for the maximum static limb
# stress is enforced. This leads to a result with a much better tiller 
# Warning: Running this script can take a few minutes

# Import the Bow Simulation Tool interface, the numpy and scipy libraries and time for measuring runtime
import bst
import numpy
from scipy.optimize import minimize
import time

# Values for the constraints
max_width = 50
min_width = 15
max_stress = 100

# Prepare bow data. n: Number of limb control points
n = 6

data = {
    "parameters":
    {
        "centerline":
        {
            "x": [0, 0],
            "y": [0, 800]
        },
        "masses":
        {
            "arrow": 25,
            "limb_tip": 0,
            "serving": 0
        },
        "material": # Elm wood
        {
            "E": 11000,
            "rho": 705
        },
        "sections":
        {
            "pos": numpy.linspace(0, 100, n, True).tolist(),
            "width": numpy.linspace(max_width, min_width, n, True).tolist(),    # Start with a linear width taper ...
            "height": numpy.linspace(6, 6, n, True).tolist()                    # ... and a constant height
        },
        "string":
        {
            "EA":30000,
            "rhoA":0.02,
            "diameter":0.0,
            "draw_length":700.0,
            "brace_height":180.0
        }
    },
    "settings":
    {
        "contact_handling":False,
        "elements":3*n,             # Make sure there are enough elements to represent the limb geometry properly
        "timestep_factor":0.9
    },
    "comments":"",
    "version":"2015.2"
}

# Here we define how an array of design variables maps to the data in our bow model.
# Two functions are defined for this: from_data() takes the widths and heights from the
# bow model and constructs an array of design variables by concatenating them.
# to_data(x) takes an array of design variables, splits it up and sets the widths and heights
# accordingly.
def to_data(x):
    data["parameters"]["sections"]["width"] = x[0:n].tolist()
    data["parameters"]["sections"]["height"] = x[n:2*n+1].tolist()

def from_data():
    x = numpy.array(data["parameters"]["sections"]["width"] + data["parameters"]["sections"]["height"])
    return x;

with bst.Simulation("../Release/BowSimulationTool.exe") as sim:

    def objective(x):

        # Set the variables in the bow model
        to_data(x)

        # The most simple way to deal with constraints is to add a penalty to the objective function if
        # the constraints are not fullfilled. Here we return a velocity of 0 (which is certainly suboptimal)
        # if the widths are not all between max_width and min_width.
        if max(data["parameters"]["sections"]["width"]) > max_width:
            return 0
        if min(data["parameters"]["sections"]["width"]) < min_width:
            return 0

        # Run a dynamic simulation with the data
        sim.run(data, True)

        # With the results, test if the constraint for maximum limb stress is fulfilled
        if sim.statics["max_stress"] > max_stress:
            return 0

        # Print a message and return the objective
        v = sim.dynamics["final_arrow_velocity"]
        print("v = ", v, "\n")
        return -v
   
    try:
        # Take the initial value for the design variables from the bow data
        x0 = from_data()

        # Minimize the objective function, also measure the runtime
        t1 = time.time()
        res = minimize(objective, x0, method='Powell', options={'xtol': 1e-5, 'disp': True})

        # Store the final result to the data
        to_data(res.x)
    finally:
        # Print the runtime, save the bow resulting from the optimisation and open it in Bow Simulation Tool
        print("Time passed: ", time.time() - t1, "s")
        bst.save(data, "opt4.bow")
        sim.show("opt4.bow")
