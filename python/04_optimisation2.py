# This is an example for constrained optimisation with multiple variables.
# Consider this problem: What is the optimal width profile for an otherwise given bow if we want to maximise
# arrow speed? Constraint: Widths of the limb have to lie between an upper and a lower bound.
# Warning: Running this script can take a few minutes.

# Import the Bow Simulation Tool interface, the numpy and scipy libraries and time for measuring runtime
import bst
import numpy
from scipy.optimize import minimize
import time

# Prepare the bow data. n: Number of limb control points
n = 10
data = {
    "parameters":
    {
        "centerline":
        {
            "x":
            [0.0,0.0],
            "y":
            [0.0,450.0]
        },
        "masses":
        {
            "arrow":20.0,
            "limb_tip":0.0,
            "serving":0.0
        },
        "material":
        {
            "E":210000.0,
            "rho":7850.0
        },
        "sections":
        {
            # Initialise the cross sections according to 
            "pos":numpy.linspace(0, 100, n, True).tolist(),
            "width":numpy.linspace(50, 10, n, True).tolist(),    # Start with a linear taper
            "height":numpy.linspace(5, 5, n, True).tolist()
        },
        "string":
        {
            "EA":40000,
            "rhoA":0.04,
            "diameter":0.0,
            "draw_length":400.0,
            "brace_height":150.0
        }
    },
    "settings":
    {
        "contact_handling":False,
        "elements":3*n,     # Make sure the limb geometry gets resolved properly
        "timestep_factor":0.9
    },
    "comments":"",
    "version":"2015.2"
}

def to_data(x):
    data["parameters"]["sections"]["width"][1:-1] = x.tolist()

def from_data():
    x = numpy.array(data["parameters"]["sections"]["width"])[1:-1]
    return x


# Create the Simulation object
with bst.Simulation("../Release/BowSimulationTool.exe") as sim:

    # The objective function takes the width profile of the limb and returns the corresponding arrow velocity
    def objective(x):

        # The most simple way to deal with constraints is to add a penalty to the objective function when
        # the constraints are not fullfilled. Here we return a velocity of 0 (which is certainly suboptimal)
        # if the widths are not all between 10 and 50
        if max(x) > 50 or min(x) < 10:
            return 0

        to_data(x)
        sim.run(data, True)

        # Retrieve arrow velocity and print out intermediate result
        v = sim.dynamics["final_arrow_velocity"]
        print("v = ", v, "\n")

        # Return the negative velocity, which will be minimised
        return -v

    # Get the starting width profile from the data
    x0 = from_data()

    try:
        # Minimise the objective function, measure time for comparing different algorithms
        t1 = time.time()
        res = minimize(objective, x0, method='Powell', options={'xtol': 1e-3, 'disp': True})
        to_data(res.x)
        print("Time: ", time.time() - t1)
    finally:
        # Save the bow resulting from the optimisation...
        bst.save(data, "opt2.bow")

        # ... and open it in Bow Simulation Tool so we can view the profile.
        # Does the resulting width profile look familiar to you?
        sim.show("opt2.bow")
